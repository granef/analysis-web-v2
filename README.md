<img  src="https://is.muni.cz/www/milan.cermak/granef/granef-logo.svg"  height="60px">

[**Graph-Based Network Forensics**](https://gitlab.ics.muni.cz/granef/granef)**: Analysis web**

---

The Analysis Web module connects to [Analysis API](https://gitlab.ics.muni.cz/granef/analysis-api) and provides web-based user interface to analyze and browse provided data.

### Requirements

- a running instance of Analysis API module
- Docker
- PostgreSQL
- NodeJS
- Python3
- Python3 packages in [requirements.txt](requirements.txt)

The installation can be performed using the following command:

```bash
$ git clone https://gitlab.ics.muni.cz/granef/analysis-web-v2.git
```

Use the following command to build the Docker container:

```bash
$ docker build --tag=granef/analysis-web .
```

Alternatively, you can use Docker compose to build and run the module as separate Docker images:

```bash
$ docker-compose up
```

### Usage

The Docker container can be either run separately with command line arguments or as part of the Granef toolkit with arguments set in the [granef.yml](https://gitlab.ics.muni.cz/granef/granef/-/blob/master/granef.yml) configuration file. 

The following arguments can be set:

| Short argument | Long argument | Description | Default | 
|-|-|-|-|
|`-i`|`--input`|Dummy argument (requested by the GRANEF toolkit)||
|`-ip`|`--ip`|IP address to bind the web visualization application|`0.0.0.0`|
|`-p`|`--port`|Port to bind the web visualization application|`80`|
|`-ai`|`--api_ip`|IP address of the Granef analysis API|`granef-analysis-api`|
|`-ap`|`--api_port`|Port of the Granef analysis API|`7000`|
|`-nc`|`--nginx_configuration`|Nginx configuration file path|`/etc/nginx/conf.d/default.conf`|
|`-lp`|`--logs_path`|Directory to store subprocess outputs|`/var/log/granef/`|
|`-l`|`--log`|Log level|`INFO`|

Use the following command to run the Analysis Web:

```bash
$ docker run --rm --network granef -p 127.0.0.1:80:80 granef/analysis-web -ip 0.0.0.0 -p 80 -ai granef-analysis-api -ap 7000
```

The Analysis Web is then available at [http://127.0.0.1:80](http://127.0.0.1:80).

### Adding support for new queries

In case the analytic API is extended with new endpoints, follow these steps to provide UI support:

1. Add a function for making a request to the new endpoint in `client/src/api/analysis-api.js`.
2. Extend query type in `client/src/components/visualization/graph/side-panel/query/fetch/query-types.ts` with an appropriate constant.
3. Create a form component in `client/src/components/visualization/graph/side-panel/query/fetch/form` if any input for the API is necessary.
4. Create an entry in `client/src/components/visualization/graph/side-panel/query/fetch/query-options.js` for the new query.
