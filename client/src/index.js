import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import cytoscape from "cytoscape";
import lasso from "cytoscape-lasso";
import contextMenus from "cytoscape-context-menus";
import avsdf from "cytoscape-avsdf";
import dagre from "cytoscape-dagre";
import cola from "cytoscape-cola-with-layout-locking";
import springy from "cytoscape-springy";
import fcose from "cytoscape-fcose";
import cise from "cytoscape-cise";
import coseBilkent from "cytoscape-cose-bilkent";
import spread from "cytoscape-spread";
import klay from "cytoscape-klay";
import euler from "cytoscape-euler";
import 'cytoscape-context-menus/cytoscape-context-menus.css';
import { graphManipulation } from "./cy-extensions/manipulation/graph-manipulation";
import { clustering } from "./cy-extensions/clustering/clustering";
import { chineseWhisperClustering } from "./cy-extensions/clustering/chinese-whisper/chinese-whisper-clustering";
import { timelineClustering } from "./cy-extensions/clustering/timeline/timeline-clustering";
import { timeline } from "./cy-extensions/timeline/timeline";
import { annotations } from "./cy-extensions/annotations/annotations";

// initialize Cytoscape
// use UI extensions
cytoscape.use(lasso);
cytoscape.use(contextMenus);

// use layouts
cytoscape.use(avsdf);
cytoscape.use(dagre);
cytoscape.use(cola);
cytoscape.use(springy);
cytoscape.use(fcose);
cytoscape.use(cise);
cytoscape.use(coseBilkent);
cytoscape.use(spread);
cytoscape.use(klay);
cytoscape.use(euler);

// register Granef specific functions
graphManipulation(cytoscape);
clustering(cytoscape);
annotations(cytoscape);
chineseWhisperClustering(cytoscape);
timelineClustering(cytoscape);
timeline(cytoscape);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
