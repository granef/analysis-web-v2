import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link, useParams } from "react-router-dom";
import { findVisualization } from "../api/visualizations-api";

const useStyles = makeStyles((theme) => ({
  root: {
    zIndex: theme.zIndex.appBar,
    boxShadow: "1px 1px 4px #00000036",
  },
  toolbar: {
    minHeight: "56px",
    backgroundColor: "#fff"
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    fontFamily: "Montserrat, Helvetica Neue, Arial, sans-serif",
    color: theme.palette.secondary.main,
    marginRight: theme.spacing(2),
    textDecoration: "none",
    fontSize: "26px",
  },
  visualizationTitle: {
    flexGrow: 1,
  },
  link: {
    marginRight: theme.spacing(2),
    textDecoration: "none",
    color: "#0000008C",
    fontSize: "15px",
    fontWeight: 400,
    '&:hover': {
      color: "#000000B2",
    },
  }
}));

export default function MyAppBar() {
  const classes = useStyles();
  const { id } = useParams();
  const [visualizationName, setVisualizationName] = useState();

  useEffect(() => {
    if (id) {
      findVisualization(id)
        .then(response => {
          setVisualizationName(response.data.name);
        });
    } else {
      setVisualizationName(undefined);
    }
  }, [id]);

  return (
    <div className={classes.root}>
      <AppBar position="static" elevation={0}>
        <Toolbar variant="dense" className={classes.toolbar}>
            <Link to="/visualizations" className={classes.title}>
              <Typography variant="h5" className={classes.title}>
              Granef
              </Typography>
            </Link>

          {visualizationName && (
            <Typography className={classes.visualizationTitle} variant="subtitle1">
              {visualizationName}
            </Typography>
          )}

          <Link to="/visualizations" className={classes.link}>
            <Typography variant="subtitle2" className={classes.link}>
              Visualizations
            </Typography>
          </Link>
          <Link to="/visualizations/new" className={classes.link}>
            <Typography variant="subtitle2" className={classes.link}>
              New Visualization
            </Typography>
          </Link>
          <Link to="/preferences" className={classes.link}>
            <Typography variant="subtitle2" className={classes.link}>
              Preferences
            </Typography>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
}
