import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import Box from "@material-ui/core/Box";
import CytoscapeComponent from "react-cytoscapejs";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import { CyCustomEvents } from "../../../cy-extensions/cy-custom-events";
import { VisualizationGrid } from "./tools/VisualizationGrid";
import { Alert } from "@material-ui/lab";
import Snackbar from '@material-ui/core/Snackbar';
import { BASE_NODES_SCRATCH, SCRATCH_CLUSTERING } from "../../../cy-extensions/clustering/clustering";
import { CyContextProvider } from "../../../contexts/cy-context";
import { getVisualizationPreferences, getVisualizationState } from "../../../api/visualizations-api";
import { getStylesheet } from "../../../options/stylesheet";
import { useHistory, useParams } from "react-router-dom";
import { useIsEmpty } from "../../../hooks/use-is-empty";
import { PreferencesContextProvider } from "../../../contexts/preferences-context";

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.loader,
    color: '#fff',
  },
  box: {
    width: "100vw",
    zIndex: theme.zIndex.cyBox,
    [theme.breakpoints.down('xs')]: {
      border: '1px red',
      top: 48,
      height: "calc(100vh - 48px)",
    },
    [theme.breakpoints.up('sm')]: {
      border: '1px red',
      top: 56,
      height: "calc(100vh - 56px)",
    },
    [theme.breakpoints.up('md')]: {
      border: '1px red',
      top: 64,
      height: "calc(100vh - 64px)",
    },
  }
}));

export function Visualization() {
  const classes = useStyles();

  const { id } = useParams();
  const history = useHistory();

  const [cy, setCy] = useState(/** @type {GranefCore} */null);

  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState(null);

  const isEmpty = useIsEmpty(cy)

  /**
   * Set stylesheet according to preferences upon component mount.
   */
  useEffect(() => {
    if (!cy || !id || !history) {
      return;
    }

    setLoading(true);
    Promise.all([getVisualizationPreferences(id), getVisualizationState(id)])
      .then(([preferencesResponse, stateResponse]) => {
        // handle preferences
        const preferences = preferencesResponse.data;
        const stylePreferences = preferences.style;
        const initStylesheet = getStylesheet(stylePreferences);

        cy.style(initStylesheet);

        // handle state
        const viewport = stateResponse.data.viewport;
        cy.zoom(Number(viewport.zoom));
        cy.pan({
          x: Number(viewport.pan.x),
          y: Number(viewport.pan.y),
        }); // could also be mapped on BE

        const elements = stateResponse.data.elements;
        cy.add(elements);

        const scratch = stateResponse.data.scratch;
        cy.scratch(SCRATCH_CLUSTERING, scratch.clustering);
        cy.scratch(BASE_NODES_SCRATCH, scratch.baseNodeIds);

        cy.emit(CyCustomEvents.FETCH, [['dummy_id'], false]);

      }).catch(error => {
      console.error(error);
      history.push('/not-found');
    });
    setLoading(false);
  }, [cy, id, history]);


  return (
    <PreferencesContextProvider>
      <CyContextProvider cy={cy} id={id} setStatus={setStatus} setLoading={setLoading}>
        <Box
          position="absolute"
          className={classes.box}
        >
          <CytoscapeComponent
            // define diff (see https://github.com/plotly/react-cytoscapejs#diffobjecta-objectb)
            style={{
              width: "100%",
              height: "100%",
            }}
            cy={setCy}
            hideEdgesOnViewport={true}
            wheelSensitivity={0.1}
            motionBlur={true}
            elements={[]} // two example nodes connected with an edge are added to the graph by default if not initialized
          />
        </Box>
        <VisualizationGrid processActionResult={setStatus}/>

        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          open={isEmpty}
          children={<Alert severity="info">The graph is empty.</Alert>}
          key="empty-graph-snackbar-alert"
        />
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
          autoHideDuration={50000}
          onClose={() => setStatus(null)}
          open={!!status}
          children={<Alert severity={status?.severity} onClose={() => setStatus(null)}>{status?.message}</Alert>}
          key="action-status-alert"
        />

        <Backdrop className={classes.backdrop} open={loading}>
          <CircularProgress color="inherit"/>
        </Backdrop>
      </CyContextProvider>
    </PreferencesContextProvider>
  )
}