import React, { useContext, useEffect, useState } from 'react';
import { flatten, isEmpty, uniq } from "lodash";
import { CyContext } from "../../../../../contexts/cy-context";
import FirstPageIcon from '@material-ui/icons/FirstPage';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import LastPageIcon from '@material-ui/icons/LastPage';
import { makeStyles } from "@material-ui/core/styles";
import { CardActions, CardContent, IconButton } from "@material-ui/core";
import { TimelineBar } from "./TimelineBar";

const useStyles = makeStyles(theme => ({
  content: {
    overflow: "auto",
  },
  centerItems: {
    margin: "auto"
  }
}));

export const SingleIntervalTimeline = ({ timelineData, options }) => {
  const classes = useStyles();
  const [cy] = useContext(CyContext);

  const [index, setIndex] = useState({
    prev: -1,
    curr: 0,
  });
  const isBackDisabled = index.curr <= 0;
  const isNextDisabled = index.curr >= timelineData?.length - 1;

  useEffect(() => {
    if (!cy) return;

    const idArrays = timelineData?.map(({ ids }) => ids);
    const ids = uniq(flatten(idArrays));
    cy.timelineHideNodes(ids);
    cy.timelineShowNodes(timelineData[0].ids);
    setIndex({
      prev: -1,
      curr: 0,
    });
  }, [cy, timelineData]);

  useEffect(() => {
    if (isEmpty(timelineData) || !cy) {
      return;
    }

    const { prev, curr } = index;
    if (prev >= 0 && prev < timelineData?.length) {
      cy.timelineHideNodes(timelineData[prev].ids);
    } else if (prev < 0) {
      // hide all
      const idArrays = timelineData?.map(({ ids }) => ids);
      const ids = uniq(flatten(idArrays));
      cy.timelineHideNodes(ids);
    }
    if (curr >= 0 && curr < timelineData?.length) {
      cy.timelineShowNodes(timelineData[curr].ids);
    } else {
      const idArrays = timelineData?.map(({ ids }) => ids);
      const ids = uniq(flatten(idArrays));
      cy.timelineShowNodes(ids);
    }
  }, [timelineData, cy, index]);

  const moveToFirstInterval = () => {
    moveIndex(0);
  }

  const moveBackToNonEmptyInterval = () => {
    let i = index.curr;
    while (--i > 0 && timelineData[i].connections === 0) {
    }
    if (i >= 0) {
      moveIndex(i)
    }
  };

  const moveBack = () => {
    moveIndex(index.curr - 1);
  }

  const moveNext = () => {
    moveIndex(index.curr + 1);
  }

  const moveToNextNonEmptyInterval = () => {
    let i = index.curr;
    while (++i < timelineData.length - 1 && timelineData[i].connections === 0) {
    }
    if (i < timelineData.length) {
      moveIndex(i)
    }
  };

  const moveToLastInterval = () => moveIndex(timelineData.length - 1);

  const moveIndex = (newIndex) => {
    if (newIndex >= 0 && newIndex < timelineData?.length) {
      setIndex(prev => ({
        prev: prev.curr,
        curr: newIndex,
      }));
    }
  }

  const actions = [
    {
      IconButtonProps: {
        disabled: isBackDisabled,
        onClick: moveToFirstInterval
      },
      Icon: FirstPageIcon
    }, {
      IconButtonProps: {
        disabled: isBackDisabled,
        onClick: moveBackToNonEmptyInterval
      },
      Icon: NavigateBeforeIcon
    },
    {
      IconButtonProps: {
        disabled: isBackDisabled,
        onClick: moveBack
      },
      Icon: NavigateBeforeIcon
    },
    {
      IconButtonProps: {
        disabled: isNextDisabled,
        onClick: moveNext
      },
      Icon: NavigateNextIcon
    },
    {
      IconButtonProps: {
        disabled: isNextDisabled,
        onClick: moveToNextNonEmptyInterval
      },
      Icon: NavigateNextIcon
    },
    {
      IconButtonProps: {
        disabled: isNextDisabled,
        onClick: moveToLastInterval
      },
      Icon: LastPageIcon
    }
  ];

  return (
    <>
      <CardContent className={classes.content}>
        <TimelineBar
          index={index.curr}
          data={timelineData}
          valueScale={options.scale}
          onClick={event => moveIndex(event.index)}
        />
      </CardContent>
      <CardActions disableSpacing>
        {actions.map(({ IconButtonProps, Icon }) => (
          <IconButton className={classes.centerItems}
                      color="secondary"
                      size="small"
                      {...IconButtonProps}>
            <Icon/>
          </IconButton>
        ))}
      </CardActions>
    </>
  )
}