import React from "react";
import { TimelineBar as TimelineBaseBar } from "../TimelineBar";

export function TimelineBar({ data, valueScale, index, onClick }) {
  return (
    <TimelineBaseBar
      data={data}
      onClick={onClick}
      valueScale={valueScale}
      selectedIndices={[index]}
      annotations={[
        {
          type: 'circle',
          match: { key: `connections.${data[index].id}` },
          noteX: 25,
          noteY: -25,
          offset: 30,
          noteTextOffset: -3,
          noteWidth: 50,
          note: 'Current time step',
          size: 10,
        },
      ]}
    />
  )
}