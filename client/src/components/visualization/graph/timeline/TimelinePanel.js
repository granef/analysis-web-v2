import React, { useContext, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import { isEmpty } from "lodash";
import { Alert } from "@material-ui/lab";
import { PanelCard } from "../../../common/PanelCard";
import { CyContext } from "../../../../contexts/cy-context";
import { TimelineOptionsForm } from "./TimelineOptionsForm";
import { useTimeline } from "../../../../hooks/use-timeline";
import { CyCustomEvents } from "../../../../cy-extensions/cy-custom-events";
import moment from "moment";
import { PanelTitle } from "../side-panel/PanelTitle";
import { Loader } from "../../../common/Loader";
import { BASE_NODES_SCRATCH } from "../../../../cy-extensions/clustering/clustering";

const useStyles = makeStyles(theme => ({
  root: {
    borderColor: theme.palette.secondary.light,
    borderWidth: 2,
    maxHeight: "100%",
    overflow: "auto",
  },
  content: {
    overflow: "auto",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  centerItems: {
    margin: "auto"
  },
  rightAlignItems: {
    marginLeft: "auto"
  },
}));

export function TimelinePanel({ Timeline, close }) {
  const classes = useStyles();

  const [cy] = useContext(CyContext);

  const [optionsOpen, setOptionsOpen] = useState(false);

  const {
    timelineData,
    options,
    setOptions,
    setUids,
    error,
    loading
  } = useTimeline({
      interval: moment.duration(5, 'minutes').asMilliseconds(),
      timeRange: {
        min: null,
        max: null,
      },
      scale: 'linear',
    }
  );

  /**
   * Update UIDs upon which timeline operates when cy registers a fetch or deletion operation
   */
  useEffect(() => {
    if (cy) {
      const refreshTimelineIds = () => {
        setUids(cy.scratch(BASE_NODES_SCRATCH));
      }
      const refreshTimelineIdsEvents = [CyCustomEvents.FETCH, CyCustomEvents.DELETE_SELECTION].join(' ');
      cy.on(refreshTimelineIdsEvents, refreshTimelineIds);
      return () => {
        cy.off(refreshTimelineIdsEvents, refreshTimelineIds);
      }
    }
  })

  return (
    <PanelCard open={true}>
      <PanelTitle align="center">
        Timeline
      </PanelTitle>
      {loading && (
        <Loader width="100%" height={380}/>
      )}
      {!loading && error && (
        <CardContent className={classes.content}>
          <Alert severity="error" variant="outlined">{error}</Alert>
        </CardContent>
      )}
      {!loading && !error && optionsOpen && (
        <CardContent classes={{ root: classes.content }}>
          <TimelineOptionsForm
            currentOptions={options}
            cancel={() => setOptionsOpen(false)}
            apply={newOptions => {
              setOptions(newOptions);
              setOptionsOpen(false);
            }}/>
        </CardContent>
      )}
      {!loading && !error && !optionsOpen && !isEmpty(timelineData) && (
        <Timeline timelineData={timelineData} options={options}/>
      )}
      <CardActions>
        <Button size="small" classes={{ root: classes.rightAlignItems }} onClick={() => {
          setOptionsOpen(true)
        }} disabled={optionsOpen}>Options</Button>
        <Button size="small" classes={{ root: classes.rightAlignItems }} onClick={() => {
          cy.nodes().timelineShowNodes();
          close();
        }}>Close</Button>
        {/* close and keep timelineHidden as userHidden*/}
      </CardActions>
    </PanelCard>
  );
}
