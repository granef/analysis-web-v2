import React, { useContext, useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import { CyContext } from "../../../../../contexts/cy-context";
import { TimelineBar } from "../TimelineBar";

const useStyles = makeStyles(theme => ({
  content: {
    overflow: "auto",
  },
  centerItems: {
    margin: "auto"
  }
}));

export function MultiIntervalTimeline({ timelineData, options }) {
  const classes = useStyles();

  const [cy] = useContext(CyContext);

  const [selectedIndices, setSelectedIndices] = useState([]);

  useEffect(() => {
    if (!cy) return;

    const newSelectedIndices = [...Array(timelineData.length).keys()];
    setSelectedIndices(newSelectedIndices);
    cy.timelineShowNodes(newSelectedIndices);
  }, [cy, timelineData]);

  const toggleSelectedIndex = event => {
    const { index } = event;
    if (selectedIndices.includes(index)) {
      const selectedNodeIds = selectedIndices.flatMap(selectedIndex => selectedIndex === index ? [] : timelineData[selectedIndex].ids);
      cy.timelineShowAndHideNodes(selectedNodeIds, timelineData[index].ids);
      setSelectedIndices(prev => prev.filter(selectedIndex => selectedIndex !== index));
    } else {
      cy.timelineShowNodes(timelineData[index].ids);
      setSelectedIndices(prev => prev.concat([index]));
    }
  };

  const invertSelection = () => {
    const getIdsAtIndices = (indices) => indices
      .map(index => timelineData[index])
      .map(data => data.ids).flat();

    const idsToHide = getIdsAtIndices(selectedIndices);
    cy.timelineHideNodes(idsToHide);

    const indicesToShow = [...Array(timelineData.length).keys()]
      .filter(index => !selectedIndices.includes(index))

    const idsToShow = getIdsAtIndices(indicesToShow);
    cy.timelineShowNodes(idsToShow);

    setSelectedIndices(indicesToShow);
  };

  const showAll = () => {
    const ids = timelineData.map(data => data.ids).flat();
    cy.timelineShowNodes(ids);
    setSelectedIndices([...Array(timelineData.length).keys()])
  };

  const hideAll = () => {
    const ids = timelineData.map(data => data.ids).flat();
    cy.timelineHideNodes(ids);
    setSelectedIndices([]);
  };

  return (
    <>
      <CardContent className={classes.content}>
        <TimelineBar
          data={timelineData}
          selectedIndices={selectedIndices}
          valueScale={options.scale}
          onClick={toggleSelectedIndex}
        />
      </CardContent>
      <CardActions disableSpacing>
        <Button className={classes.centerItems}
                color="secondary"
                size="small"
                onClick={hideAll}>
          Hide all
        </Button>
        <Button className={classes.centerItems}
                color="secondary"
                size="small"
                onClick={showAll}>
          Show all
        </Button>
        <Button className={classes.centerItems}
                color="secondary"
                size="small"
                onClick={invertSelection}>
          Invert selection
        </Button>
      </CardActions>
    </>
  );
}
