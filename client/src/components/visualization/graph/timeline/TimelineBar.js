import { Bar } from "@nivo/bar";
import React from "react";
import { getColorForType } from "../../../../parsers/parse-utils";
import { formatUtcDate } from "../../../../context-menu/utils";

function Detail({ color, bar }) {
  const { start, end } = bar.data;

  return (
    <div
      style={{
        padding: 12,
        color,
        background: '#222222',
      }}
    >
      start: <strong>{formatUtcDate(start)}</strong>
      <br/>
      end: <strong>{formatUtcDate(end)}</strong>
      <br/>
      connections: <strong>{bar.value}</strong>
    </div>
  )
}

export function TimelineBar({ data, selectedIndices, valueScale, ...restProps }) {
  const notSelectedColor = '#e0d8c0';
  const connectionColor = getColorForType('Connection');
  const width = 225 + ((data ? data.length : 0) * 25);

  const isSelected = (index) => selectedIndices.includes(index);

  return (
    <Bar
      data={data}
      keys={['connections']}
      indexBy="id"
      width={width}
      height={380}
      margin={{ top: 10, right: 115, bottom: 85, left: 95 }}
      valueScale={{ type: valueScale }}
      valueFormat=" >-"
      borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 30,
        legend: 'Time',
        legendPosition: 'start',
        legendOffset: 80
      }}
      axisLeft={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: 'Number of connections',
        legendPosition: 'middle',
        legendOffset: -50
      }}
      labelSkipWidth={12}
      labelSkipHeight={12}
      labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
      role="application"
      ariaLabel="Timeline"
      barAriaLabel={function (e) {
        return e.id + ": " + e.formattedValue + " at " + e.indexValue
      }}
      colors={bar => isSelected(bar.index) ? connectionColor : notSelectedColor}
      tooltip={(bar) => (
        <Detail color={isSelected(bar.index) ? connectionColor : notSelectedColor} bar={bar}/>
      )}
      {...restProps}
    />
  )
}