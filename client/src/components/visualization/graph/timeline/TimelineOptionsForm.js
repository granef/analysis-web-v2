import React, { useState } from "react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { TextField } from "@material-ui/core";
import { OptionsSelect } from "../../../common/OptionsSelect";
import { fixDate, toDatetimeLocal } from "../../../../utils/date-utils";
import moment from "moment";

const asMilliseconds = (inp, unit) => moment.duration(inp, unit).asMilliseconds();
const intervalOptions = [
  { value: asMilliseconds(1, 'minute'), label: 'One minute' },
  { value: asMilliseconds(5, 'minutes'), label: 'Five minutes' },
  { value: asMilliseconds(15, 'minutes'), label: 'Fifteen minutes' },
  { value: asMilliseconds(30, 'minutes'), label: 'Thirty minutes' },
  { value: asMilliseconds(1, 'hour'), label: 'One hour' },
  { value: asMilliseconds(3, 'hours'), label: 'Three hours' },
  { value: asMilliseconds(6, 'hours'), label: 'Six hours' },
  { value: asMilliseconds(12, 'hours'), label: 'Twelve hours' },
  { value: asMilliseconds(24, 'hours'), label: 'One day' }
];

const scaleOptions = [
  { value: 'linear', label: 'Linear' },
  { value: 'symlog', label: 'Symmetrical log' },
];

function OptionSelect({ options, onChange, id, label, value }) {
  return (
    <FormControl variant="outlined" fullWidth style={{ marginTop: 8, marginBottom: 8 }}>
      <InputLabel id={`${id}-label`}>{label}</InputLabel>
      <OptionsSelect
        options={options}
        labelId={`${id}-label`}
        id={id}
        value={value}
        onChange={onChange}
        label={label}
      />
    </FormControl>
  )
}

export function TimelineOptionsForm({ currentOptions, apply, cancel }) {
  const [options, setOptions] = useState({
    ...currentOptions,
    timeRange: {
      min: toDatetimeLocal(currentOptions.timeRange.min),
      max: toDatetimeLocal(currentOptions.timeRange.max)
    }
  });

  const MAX_NUMBER_OF_INTERVALS = 200;

  const isTimeRangeValid = new Date(options.timeRange.min) < new Date(options.timeRange.max);
  const isPerformantForInterval = ((new Date(options.timeRange.max).getTime() - new Date(options.timeRange.min).getTime()) / options.interval) < MAX_NUMBER_OF_INTERVALS;
  const isValid = isTimeRangeValid && isPerformantForInterval;

  return (
    <>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <OptionSelect
            id="scale"
            options={scaleOptions}
            label="Scale"
            value={options.scale}
            onChange={(event) => setOptions(prev => ({
              ...prev,
              scale: event.target.value
            }))}
          />
        </Grid>
        <Grid item xs={12}>
          <OptionSelect
            id="time-interval-size"
            options={intervalOptions}
            label="Interval size"
            value={options.interval}
            onChange={(event) => setOptions(prev => ({
              ...prev,
              interval: event.target.value
            }))}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            style={{ marginTop: 8, marginBottom: 8 }}
            variant="outlined"
            id="time-range-from"
            label="From"
            type="datetime-local"
            defaultValue={options.timeRange.min}
            error={new Date(options.timeRange.min) >= new Date(options.timeRange.max)}
            InputLabelProps={{
              shrink: true,
            }}
            onChange={e => setOptions(prev => ({
              ...prev,
              timeRange: {
                ...prev.timeRange,
                min: e.target.value
              }
            }))}
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            style={{ marginTop: 8, marginBottom: 8 }}
            variant="outlined"
            id="time-range-to"
            label="To"
            type="datetime-local"
            defaultValue={options.timeRange.max}
            error={new Date(options.timeRange.min) >= new Date(options.timeRange.max)}
            onChange={e => setOptions(prev => ({
              ...prev,
              timeRange: {
                ...prev.timeRange,
                max: e.target.value
              }
            }))}
            InputLabelProps={{
              shrink: true,
            }}
            fullWidth
          />
        </Grid>
        <Grid item xs={6}>
          <Button onClick={cancel} color="secondary" variant="outlined" fullWidth>
            Cancel
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button onClick={() => apply({
            ...options,
            timeRange: {
              min: fixDate(options.timeRange.min),
              max: fixDate(options.timeRange.max)
            }
          })} color="secondary" variant="contained" fullWidth disabled={!isValid} disableElevation>
            Apply
          </Button>
        </Grid>
      </Grid>
    </>
  )
}