import React, { useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import ColoredAutocomplete from "../annotations/ColoredAutocomplete";
import { SliderFormControl } from "./SliderFormControl";
import { getColors, getTags } from "../../../../../api/annotations-api";
import { useParams } from "react-router-dom";
import { MultiSelect } from "../../../../common/MultiSelect";
import { NodeTypeValues } from "../../../../../parsers/node-type";

const ViewQueryForm = ({ selectByParams }) => {
  const { id } = useParams();

  const [tags, setTags] = useState([]);
  const [colors, setColors] = useState([]);

  const [selectedTypes, setSelectedTypes] = useState([]);
  const [selectedColors, setSelectedColors] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);
  const [betweennessCentrality, setBetweennessCentrality] = useState({
    apply: false,
    inverted: false,
    value: [0, 1],
  });
  const [pageRank, setPageRank] = useState({
    apply: false,
    inverted: false,
    value: [0, 1],
  });

  useEffect(() => {
    if (id) {
      getTags(id).then(response => {
        setTags(response.data);
      });
    }
  }, [id]);

  useEffect(() => {
    if (id) {
      getColors(id).then(response => {
        setColors(response.data);
      });
    }
  }, [id]);


  const handleSelect = (clear) => () => {
    selectByParams({
      tags: selectedTags,
      colors: selectedColors,
      types: selectedTypes,
      clear,
      betweennessCentrality,
      pageRank
    })
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <FormControl variant="outlined" fullWidth>
          <ColoredAutocomplete
            label="Tags"
            options={tags}
            selectedValues={selectedTags}
            setSelectedValues={setSelectedTags}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl variant="outlined" fullWidth>
          <ColoredAutocomplete
            label="Colors"
            options={colors}
            selectedValues={selectedColors}
            setSelectedValues={setSelectedColors}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <MultiSelect
          label="Node types"
          options={NodeTypeValues}
          selected={selectedTypes}
          setSelected={event => {
            setSelectedTypes(event.target.value);
          }}/>
      </Grid>
      <Grid item xs={12}>
        <SliderFormControl
          label="Betweenness centrality (normalized)"
          state={betweennessCentrality}
          setState={setBetweennessCentrality}
        />
      </Grid>
      <Grid item xs={12}>
        <SliderFormControl
          label="Page rank"
          state={pageRank}
          setState={setPageRank}
        />
      </Grid>

      <Grid item xs={6}>
        <Button onClick={handleSelect(true)} color="secondary" variant="contained" disableElevation fullWidth>
          Select
        </Button>
      </Grid>
      <Grid item xs={6}>
        <Button onClick={handleSelect(false)} color="secondary" variant="contained" disableElevation fullWidth>
          Add to selection
        </Button>
      </Grid>
    </Grid>
  )
};

export default ViewQueryForm;