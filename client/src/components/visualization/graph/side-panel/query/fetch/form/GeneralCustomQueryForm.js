import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField/TextField";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1, 0),
  }
}));

export function GeneralCustomQueryForm({ params, handleChange }) {
  const classes = useStyles();

  return (
    <Grid item xs={12}>
      <FormControl className={classes.formControl} fullWidth>
        <TextField
          id="custom-query-field"
          label="Custom Query"
          variant="outlined"
          onChange={handleChange('customQuery')}
          defaultValue={params?.customQuery}
          multiline
        />
      </FormControl>
    </Grid>
  )
}