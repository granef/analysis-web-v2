export type GeneralQuery = 'GENERAL_CUSTOM_QUERY';
export type GraphQuery = 'GRAPH_NODE_ATTRIBUTES' | 'GRAPH_ATTRIBUTE_SEARCH' | 'GRAPH_NEIGHBOURS';
export type OverviewQuery = 'OVERVIEW_HOST_INFO' | 'OVERVIEW_CONNECTION_FROM_SUBNET';
export type AnalysisQuery = 'CONNECTIONS';
export type AttackQuery = 'LOOK_FOR_XSS' | 'HTTP_SQLI_REGEX';

export type Query = GeneralQuery | GraphQuery | OverviewQuery | AnalysisQuery | AttackQuery;