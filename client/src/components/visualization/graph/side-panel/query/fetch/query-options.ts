import { Query } from './query-types';
import { GeneralCustomQueryForm } from './form/GeneralCustomQueryForm';
import { NodeAttributesForm } from './form/NodeAttributesForm';
import { AttributeSearchForm } from './form/AttributeSearchForm';
import { NeighboursForm } from './form/NeighboursForm';
import { HostInfoForm } from './form/HostInfoForm';
import { ConnectionsFromSubnetForm } from './form/ConnectionFromSubnetForm';
import {
  getAttributeSearch, getConnections, getConnectionsFromSubnet,
  getCustomQuery, getHostInfo, getNeighbours,
  getNodeAttributes,
  httpRegexpSearch,
  lookForXss,
} from '../../../../../../api/analysis-api';
import { ComponentType } from 'react';
import { ConnectionsForm } from './form/ConnectionsForm';

interface ConnectionsParams {
  originatorAddress?: string;
  responderAddress?: string;
  from?: string;
  to?: string;
}

interface NodeAttributesParams {
  uids: string;
}

interface AttributeSearchParams {
  attribute: string;
  value: string;
}

interface NeighboursParams {
  anyType: boolean;
  uids: string;
  types?: string[];
}

interface HostInfoParams {
  address: string;
}

interface ConnectionsFromSubnetParams {
  address: string;
}

interface CustomQueryParams {
  customQuery: string;
}

interface GroupQueryOptions {
  groupLabel: string
  groupOptions: QueryOption[]
}

interface QueryOption {
  label: string,
  value: Query,
  fetch?: any,
  Component?: ComponentType<any>
}

export const customQueryOption: QueryOption = {
  label: 'Custom query',
  value: 'GENERAL_CUSTOM_QUERY',
  fetch: ({ customQuery }: CustomQueryParams) => getCustomQuery(customQuery),
  Component: GeneralCustomQueryForm,
}

export const hostsInfoQueryOption: QueryOption = {
  label: 'Hosts info in given network range (CIDR)',
  value: 'OVERVIEW_HOST_INFO',
  fetch: (params: HostInfoParams) => getHostInfo(params.address),
  Component: HostInfoForm,
}

export const queryOptions: GroupQueryOptions[] = [
  {
    groupLabel: 'General',
    groupOptions: [customQueryOption],
  },
  {
    groupLabel: 'Overview Queries',
    groupOptions: [
      hostsInfoQueryOption,
      {
        label: 'Connections originated by hosts given in network range (CIDR)',
        value: 'OVERVIEW_CONNECTION_FROM_SUBNET',
        fetch: (params: ConnectionsFromSubnetParams) => getConnectionsFromSubnet(params.address),
        Component: ConnectionsFromSubnetForm,
      },
    ],
  },
  {
    groupLabel: 'Graph Queries',
    groupOptions: [
      {
        label: 'All node attributes for UIDs',
        value: 'GRAPH_NODE_ATTRIBUTES',
        fetch: (params: NodeAttributesParams) => getNodeAttributes(params.uids),
        Component: NodeAttributesForm,
      },
      {
        label: 'Nodes by attribute and value',
        value: 'GRAPH_ATTRIBUTE_SEARCH',
        fetch: (params: AttributeSearchParams) => getAttributeSearch(params.attribute, params.value),
        Component: AttributeSearchForm,
      },
      {
        label: 'Neighbours of nodes with given UIDs and types',
        value: 'GRAPH_NEIGHBOURS',
        fetch: (params: NeighboursParams) => params.anyType ? getNeighbours(params.uids) : getNeighbours(params.uids, params.types?.join(',')),
        Component: NeighboursForm,
      },
    ],
  },
  {
    groupLabel: 'Analysis Queries',
    groupOptions: [
      {
        label: 'Connections by hosts and/or time',
        value: 'CONNECTIONS',
        fetch: (params: ConnectionsParams) => getConnections(params.originatorAddress, params.responderAddress, params.from, params.to),
        Component: ConnectionsForm,
      },
    ],
  },
  {
    groupLabel: 'Attacks',
    groupOptions: [
      {
        label: 'Look for <script> in URI',
        fetch: lookForXss,
        value: 'LOOK_FOR_XSS',
      },
      {
        label: 'Look for SQL key words in URI',
        fetch: httpRegexpSearch,
        value: 'HTTP_SQLI_REGEX',
      },
    ],
  },
];