import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField/TextField";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Checkbox, FormControlLabel } from "@material-ui/core";
import { NodeTypeValues } from "../../../../../../../parsers/node-type";
import { MultiSelect } from "../../../../../../common/MultiSelect";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1, 0),
  }
}));

export function NeighboursForm({ params, handleChange, handleChangeChecked }) {
  const classes = useStyles();

  return (
    <>
      <Grid item xs={12}>
        <FormControl className={classes.formControl} fullWidth>
          <TextField
            id="uids-field"
            label="Comma separated UIDs"
            variant="outlined"
            onChange={handleChange('uids')}
            defaultValue={params?.uids}
            multiline
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControlLabel
          control={<Checkbox
            color="secondary"
            checked={params?.anyType}
            onChange={handleChangeChecked('anyType')}
            name="any-type"
          />}
          label="Any type"
        />
      </Grid>
      {!params?.anyType && (
        <Grid item xs={12}>
          <MultiSelect
            label="Node types"
            options={NodeTypeValues}
            selected={params?.types || []}
            setSelected={handleChange('types')}/>
        </Grid>
      )}
    </>
  )
}