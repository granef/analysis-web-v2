import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField/TextField";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1, 0),
  }
}));

export function ConnectionsForm({ params, handleChange }) {
  const classes = useStyles();

  return (
    <>
      <Grid item xs={12}>
        <FormControl className={classes.formControl} fullWidth>
          <TextField
            id="originator-address-field"
            label="Originator address"
            variant="outlined"
            onChange={handleChange('originatorAddress')}
            defaultValue={params?.originatorAddress}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl className={classes.formControl} fullWidth>
          <TextField
            id="responder-address-field"
            label="Responder address"
            variant="outlined"
            onChange={handleChange('responderAddress')}
            defaultValue={params?.responderAddress}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <TextField
          style={{ marginTop: 8, marginBottom: 8 }}
          variant="outlined"
          id="time-range-from"
          label="From"
          type="datetime-local"
          defaultValue={params?.from}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleChange('from')}
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          style={{ marginTop: 8, marginBottom: 8 }}
          variant="outlined"
          id="time-range-to"
          label="To"
          type="datetime-local"
          defaultValue={params.to}
          onChange={handleChange('to')}
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
        />
      </Grid>
    </>
  )
}