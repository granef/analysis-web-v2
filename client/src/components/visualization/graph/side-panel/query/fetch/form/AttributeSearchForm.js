import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField/TextField";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1, 0),
  }
}));

export function AttributeSearchForm({ params, handleChange }) {
  const classes = useStyles();

  return (
    <>
      <Grid item xs={12}>
        <FormControl className={classes.formControl} fullWidth>
          <TextField
            id="attribute-field"
            label="Attribute"
            variant="outlined"
            onChange={handleChange('attribute')}
            defaultValue={params?.attribute}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl className={classes.formControl} fullWidth>
          <TextField
            id="value-field"
            label="Value"
            variant="outlined"
            onChange={handleChange('value')}
            defaultValue={params?.value}
          />
        </FormControl>
      </Grid>
    </>
  )
}