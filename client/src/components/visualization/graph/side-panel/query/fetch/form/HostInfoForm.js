import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField/TextField";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1, 0),
  }
}));

export function HostInfoForm({ params, handleChange }) {
  const classes = useStyles();

  return (
    <Grid item xs={12}>
      <FormControl className={classes.formControl} fullWidth>
        <TextField
          id="address-field"
          label="Address"
          variant="outlined"
          onChange={handleChange('address')}
          defaultValue={params?.address}
        />
      </FormControl>
    </Grid>
  )
}