import React, { useContext, useState } from "react";
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";
import { Button, Checkbox, FormControlLabel, FormLabel, ListSubheader, Radio, RadioGroup } from "@material-ui/core";
import {
  parseGranefMultiResponse,
  parseGranefResponse
} from "../../../../../../parsers/parsers";
import { CyContext } from "../../../../../../contexts/cy-context";
import { useParams } from "react-router-dom";
import { queryOptions } from "./query-options";
import { FetchContext } from "../../../../../../contexts/fetch-context";
import { PreferencesContext } from "../../../../../../contexts/preferences-context";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1, 0),
  },
  rightAlignItems: {
    marginLeft: "auto"
  },
}));

export function FetchForm({ processActionResult }) {
  const classes = useStyles();
  const { id } = useParams();
  const [loading, setLoading] = useState(false);
  const [cy] = useContext(CyContext);
  const [preferences] = useContext(PreferencesContext);

  const {
    query, setQuery,
    clusterizeMode, setClusterizeMode,
    clear, setClear,
    params, setParams
  } = useContext(FetchContext);

  const handleChange = (prop) => (event) => {
    setParams(prevParams => ({ ...prevParams, [prop]: event.target.value }));
  };

  const handleChangeChecked = (prop) => (event) => {
    setParams(prevParams => ({ ...prevParams, [prop]: event.target.checked }));
  };

  const handleQueryChange = (event) => {
    const newQuery = event.target.value;
    setQuery(newQuery);
    setParams({ query: newQuery });
  }

  const updateGraph = async (newGraph, clusterizeMode) => {
    cy.batch(async function () {
      const { nodes: newNodes, edges: newEdges } = newGraph;

      try {
        let message;
        if (newNodes.length > 0) {
          const clusterize = {
            mode: clusterizeMode,
            ...preferences.timelineClustering
          }
          if (clear) {
            await cy.replaceWithNewGraph(newNodes, newEdges, id, clusterize);
            message = `Fetched and added ${newNodes.length} new nodes.`;
          } else {
            const addedNodesCount = await cy.unionWithGraph(newNodes, newEdges, clusterize);
            message = `Added ${addedNodesCount.length} new nodes (${newNodes.length} were fetched).`;
          }
        } else {
          message = "No nodes were fetched.";
        }
        processActionResult({ severity: "success", message });
      } catch (e) {
        console.error(e);
        processActionResult({
          severity: "error",
          message: e.response.data.message
        });
      }
    })
  };

  const handleFetch = async () => {
    setLoading(true);

    let body;
    try {
      const fetchResponse = await query.fetch(params);
      body = fetchResponse.data
    } catch (e) {
      console.error('error', e.response);
      processActionResult({
        severity: "error",
        message: e.response.data.detail
      });
      setLoading(false);
      return;
    }
    let cyElements;
    if (['GENERAL_CUSTOM_QUERY', 'HTTP_SQLI_REGEX', 'LOOK_FOR_XSS'].includes(query?.value)) {
      cyElements = parseGranefMultiResponse(body);
    } else {
      cyElements = parseGranefResponse(body);
    }

    try {
      await updateGraph(cyElements, clusterizeMode);
    } catch (e) {
      setLoading(false);
      processActionResult({
        severity: "error",
        message: e.response.data.message
      });
    }
    setLoading(false);
  }

  return (
    <>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <FormControl variant="outlined" className={classes.formControl} fullWidth>
            <InputLabel id="query-select-label">Query</InputLabel>
            <Select
              labelId="query-select-label"
              id="query-select"
              value={query}
              onChange={handleQueryChange}
              label="Query"
            >
              {queryOptions.map(group => [
                <ListSubheader>{group.groupLabel}</ListSubheader>,
                ...group.groupOptions.map(option => (
                  <MenuItem value={option}>{option.label}</MenuItem>
                ))
              ])}
            </Select>
          </FormControl>
        </Grid>

        {query?.Component && (
          <query.Component params={params} handleChange={handleChange} handleChangeChecked={handleChangeChecked}/>
        )}

        <Grid item xs={12}>
          <FormControl component="fieldset">
            <FormLabel component="legend">Clustering</FormLabel>
            <RadioGroup aria-label="clustering" name="clustering" value={clusterizeMode} row={true}
                        onChange={e => setClusterizeMode(e.target.value)}>
              <FormControlLabel value="none" control={<Radio/>} label="None"/>
              <FormControlLabel value="singleCluster" control={<Radio/>} label="Single cluster"/>
              <FormControlLabel value="timeline" control={<Radio/>} label="Timeline clustering"/>
            </RadioGroup>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            control={<Checkbox
              color="secondary"
              checked={clear}
              onChange={event => setClear(event.target.checked)}
              name="clear"
            />}
            label="Clear current visualization"
          />
        </Grid>
        <Grid container item xs={12} direction="column" alignItems="flex-end">
          <Button onClick={handleFetch} variant="contained" color="secondary" disabled={loading} fullWidth disableElevation>
            Fetch
          </Button>
        </Grid>
      </Grid>
    </>
  )
}
