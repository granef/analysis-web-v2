import React  from "react";
import { Checkbox, FormControlLabel, FormGroup, FormLabel, Slider } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  formGroup: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  }
}))

export function SliderFormControl({ label, state, setState }) {
  const classes = useStyles();

  const handleChange = (event, value) => {
    setState(prev => ({
      ...prev,
      value: value
    }))
  };

  const isDisabled = !state.apply;

  return (
    <FormControl component="fieldset" fullWidth>
      <FormLabel component="legend">{label}</FormLabel>
      <FormGroup aria-label="position" row={false} classes={{ root: classes.formGroup }}>
        <FormControlLabel
          value={state.apply}
          control={<Checkbox color="primary" onChange={event => setState(prev => ({
            ...prev,
            apply: event.target.checked
          }))}/>}
          label="Apply"
          labelPlacement="end"
        />
        {state.apply && (
          <>
            <FormControlLabel
              disabled={isDisabled}
              value={state.inverted}
              control={<Checkbox color="primary" onChange={event => setState(prev => ({
                ...prev,
                inverted: event.target.checked
              }))}/>}
              label="Invert range"
              labelPlacement="end"
            />
            <Slider
              disabled={isDisabled}
              track={state.inverted ? "inverted" : "normal"}
              value={state.value}
              onChange={handleChange}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              min={0}
              max={1}
              step={0.01}
            />
          </>
        )}
      </FormGroup>
    </FormControl>
  )
}