import React, { useContext, useState } from "react";
import FormControl from "@material-ui/core/FormControl";
import ViewQueryForm from "./ViewQueryForm";
import { FetchForm } from "./fetch/FetchForm";
import { PanelCard } from "../../../../common/PanelCard";
import {
  CardActions,
  CardContent,
  FormControlLabel,
  FormLabel,
  makeStyles,
  Radio,
  RadioGroup
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { PanelTitle } from "../PanelTitle";
import { CyContext } from "../../../../../contexts/cy-context";
import { useIsEmpty } from "../../../../../hooks/use-is-empty";

const useStyles = makeStyles(theme => ({
  root: {
    borderColor: theme.palette.secondary.light,
    borderWidth: 2,
    height: "100%",
    overflow: 'auto',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 15,
    paddingRight: 15,
  },
  content: {
    overflow: "auto",
  },
  rightAlignItems: {
    marginLeft: "auto"
  },
}));

export function QueryPanel({ open, onClose, selectByParams, processActionResult }) {
  const classes = useStyles();
  const [type, setType] = useState('in-analysis-api');

  const [cy] = useContext(CyContext);
  const isEmpty = useIsEmpty(cy);

  const handleTypeChange = (event) => {
    setType(event.target.value);
  };

  return (
    <PanelCard open={open}>
      <CardContent classes={{
        root: classes.content
      }}>
        <PanelTitle align="center">
          Search
        </PanelTitle>

        <FormControl variant="outlined" fullWidth style={{ paddingBottom: 16 }}>
          <FormLabel component="legend">Search in</FormLabel>
          <RadioGroup aria-label="search-in" name="search-in" value={type} row={true}
                      onChange={handleTypeChange}>
            <FormControlLabel value="in-visualization" control={<Radio/>} label="visualization" disabled={isEmpty}/>
            <FormControlLabel value="in-analysis-api" control={<Radio/>} label="analysis API"/>
          </RadioGroup>
        </FormControl>

        {type === 'in-visualization' && (
          <ViewQueryForm selectByParams={selectByParams}/>
        )}

        {type === 'in-analysis-api' && (
          <FetchForm processActionResult={processActionResult}/>
        )}

      </CardContent>
      <CardActions>
        <Button size="small" onClick={onClose} className={classes.rightAlignItems} disabled={isEmpty}>Close</Button>
      </CardActions>
    </PanelCard>
  )
}