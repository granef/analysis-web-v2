export interface ChipData {
  id: string,
  hex: string,
  label: string,
}

export interface ChipsDataProps {
  chipData: ChipData[],
  handleDelete: (data: ChipData) => () => void,
}
