import React, { useEffect, useState } from "react";
import ChipsList from "./chips-list";
import NewColoredAnnotationForm from "./NewColoredAnnotationForm";
import Fab from "@material-ui/core/Fab";
import AddIcon from '@material-ui/icons/Add';
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  addButton: {
    margin: theme.spacing(0.5),
  },
}));

const ColoredAnnotationOverview = ({ listApi, deleteApi, createApi }) => {
  const classes = useStyles();
  const [openNew, setOpenNew] = useState(false);
  const [annotations, setAnnotations] = useState();

  useEffect(() => {
    listApi().then(response => {
      setAnnotations(response.data);
    });
  }, [listApi]);

  const handleDelete = (tagToDelete) => () => {
    deleteApi(tagToDelete.id)
      .then(response => {
        listApi()
          .then(response => {
            setAnnotations(response.data);
          });
      })
  };

  const handleCreate = (tag) => {
    setOpenNew(false);
    createApi(tag)
      .then(response => {
        listApi()
          .then(response => {
            setAnnotations(response.data);
          });
      })
      .catch(console.error);
  };


  return (<>
    <ChipsList
      chipData={annotations}
      handleDelete={handleDelete}
    />
    {!openNew && (
      <Fab size="small" color="secondary" aria-label="add" onClick={() => setOpenNew(true)}
           className={classes.addButton}>
        <AddIcon/>
      </Fab>
    )}
    {openNew && (<NewColoredAnnotationForm
      open={openNew}
      onCancel={() => setOpenNew(false)}
      onSubmit={handleCreate}
    />)}
  </>)
};

export default ColoredAnnotationOverview;