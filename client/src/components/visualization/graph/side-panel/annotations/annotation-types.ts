import { NodeType } from '../../../../../parsers/node-type';

export interface Colored {
  id: string,
  hex: string,
  label: string,
}

export interface CentralityParams {
  apply: boolean,
  inverted: boolean,
  value: number[],
}

export interface Params {
  tags?: Colored[],
  colors?: Colored[],
  types?: NodeType[],
  clear: boolean,
  betweennessCentrality?: CentralityParams,
  pageRank?: CentralityParams,
}