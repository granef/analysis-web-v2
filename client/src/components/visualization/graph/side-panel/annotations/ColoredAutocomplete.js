import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import Chip from "@material-ui/core/Chip";
import ChipsList from "./chips-list";
import TextField from "@material-ui/core/TextField/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import React from "react";
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const icon = <CheckBoxOutlineBlankIcon fontSize="small"/>;
const checkedIcon = <CheckBoxIcon fontSize="small"/>;

const ColoredAutocomplete = ({ selectedValues, setSelectedValues, options, label, ...otherProps }) => {

  const handleUnselect = (unselected) => () => {
    setSelectedValues(prev => prev.filter(value => unselected.id !== value.id));
  };

  return (
    <Autocomplete
      multiple
      fullWidth
      disableCloseOnSelect
      // filterSelectedOptions
      value={selectedValues}
      onChange={(event, newValue) => setSelectedValues(newValue)}
      options={options}
      getOptionLabel={(option) => option.label}
      renderOption={(option, { selected }) => (
        <>
          <Checkbox
            icon={icon}
            checkedIcon={checkedIcon}
            style={{ marginRight: 8 }}
            checked={selected}
          />
          <Chip
            style={{ backgroundColor: option.hex }}
            label={option.label}
            size="small"
          />
        </>
      )}
      renderTags={(options, state) => (
        <ChipsList
          chipData={options}
          handleDelete={handleUnselect}
        />
      )}
      renderInput={(params) => (
        <TextField {...params} variant="outlined" label={label}/>
      )}
      {...otherProps}
    />
  )
};


export default ColoredAutocomplete;