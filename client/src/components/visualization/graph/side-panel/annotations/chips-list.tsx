import React from "react";
import Chip from "@material-ui/core/Chip";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Box from "@material-ui/core/Box";
import { ChipData, ChipsDataProps } from "./chips-list-types";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'left',
    flexWrap: 'wrap',
    listStyle: 'none',
    padding: theme.spacing(0.5),
    margin: 0,
    elevation: 0,
  },
  chip: {
    margin: theme.spacing(0.5),
  },
  labelLight: {
    color: '#fff'
  },
  labelDark: {
    color: '#000'
  }
}));

/**
 * source: https://stackoverflow.com/questions/3942878/how-to-decide-font-color-in-white-or-black-depending-on-background-color
 */
function pickColorAlternative(bgColor: string, lightAlt: any, darkAlt: any) {
  const color = (bgColor.charAt(0) === '#') ? bgColor.substring(1, 7) : bgColor;
  const r = parseInt(color.substring(0, 2), 16); // hexToR
  const g = parseInt(color.substring(2, 4), 16); // hexToG
  const b = parseInt(color.substring(4, 6), 16); // hexToB
  const uicolors = [r / 255, g / 255, b / 255];
  const c = uicolors.map((col) => {
    if (col <= 0.03928) {
      return col / 12.92;
    }
    return Math.pow((col + 0.055) / 1.055, 2.4);
  });
  const L = (0.2126 * c[0]) + (0.7152 * c[1]) + (0.0722 * c[2]);
  return (L > 0.179) ? darkAlt : lightAlt;
}

const ChipsList = ({chipData, handleDelete}: ChipsDataProps) => {
  const classes = useStyles();

  return (
    <Box component="ul" className={classes.root}>
      {chipData?.map((data: ChipData) => (
        <li key={data.id}>
          <Chip
            style={{backgroundColor: data.hex}}
            label={data.label}
            className={classes.chip}
            size="small"
            onDelete={handleDelete(data)}
            classes={{
              label: pickColorAlternative(data.hex, classes.labelLight, classes.labelDark)
            }}
          />
        </li>
      ))}
    </Box>
  )
};

export default ChipsList;