import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import { ChromePicker } from "react-color"
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";

const NewColoredAnnotationForm = ({ onCancel, onSubmit }) => {
  const [color, setColor] = useState({
    hex: '#ff0000',
    label: ''
  });

  const handleColorChange = newColor => {
    setColor((prevColor) => ({
      ...prevColor,
      hex: newColor.hex,
    }));
  };

  const handleLabelChange = e => {
    setColor((prevColor) => ({
      ...prevColor,
      label: e.target.value,
    }));
  };

  return (
    <Card elevation={0}>
      <CardContent>
        <FormControl fullWidth margin="normal">
          <TextField
            id="label-field"
            label="Label"
            variant="outlined"
            type="text"
            autoComplete='off'
            value={color.label}
            onChange={handleLabelChange}
            fullWidth
            required
          />
        </FormControl>
        <ChromePicker
          width="100%"
          color={color.hex}
          onChange={handleColorChange}
          onChangeComplete={handleColorChange}
          disableAlpha
          styles={{
            default: {
              picker: {
                boxShadow: 'none',
              }
            }
          }}
        />
      </CardContent>
      <CardActions>
        <div style={{ marginLeft: "auto" }}>
          <Button size="small" onClick={onCancel}>Cancel</Button>
          <Button size="small" onClick={() => onSubmit(color)} disabled={!color.label || !color.hex}>Create</Button>
        </div>
      </CardActions>
    </Card>
  );
};

export default NewColoredAnnotationForm;