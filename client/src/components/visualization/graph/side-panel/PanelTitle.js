import Typography from "@material-ui/core/Typography";
import React from "react";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  subTitle: {
    margin: 10,
    marginBottom: 20,
  },
}));

export const PanelTitle = props => {
  const classes = useStyles();

  return (
    <Typography variant="h6" color="secondary" className={classes.subTitle} {...props}/>
  )
};