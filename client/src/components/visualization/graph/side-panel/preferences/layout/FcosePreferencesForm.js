import React from "react";
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import { TextField } from "@material-ui/core";

export function FcosePreferencesForm({ layout, updateLayoutOptions }) {
  const onChangeNodeRepulsion = (evt) => {
    if (evt.target.value < 0) {
      console.error('invalid node repulsion', evt.target.value)
      return;
    }
    updateLayoutOptions({
      nodeRepulsion: Number(evt.target.value)
    });
  };

  const onChangeIdealEdgeLength = (evt) => {
    if (evt.target.value < 0) {
      console.error('invalid ideal edge length', evt.target.value)
      return;
    }
    updateLayoutOptions({
      idealEdgeLength: Number(evt.target.value)
    });
  };

  const onChangeEdgeElasticity = (evt) => {
    // if (evt.target.value < 0 || evt.target.value > 1) {
    //   console.error('invalid edge elasticity', evt.target.value)
    //   return;
    // }
    updateLayoutOptions({
      edgeElasticity: evt.target.value
    });
  };

  const onChangeGravity = (evt) => {
    if (evt.target.value < 0 || evt.target.value > 1) {
      console.error('invalid gravity', evt.target.value)
      return;
    }
    updateLayoutOptions({
      gravity: evt.target.value
    });
  };

  const onChangeNumberOfIterations = (evt) => {
    if (evt.target.value < 0) {
      console.error('invalid numIter', evt.target.value)
      return;
    }
    updateLayoutOptions({
      numIter: Number(evt.target.value)
    });
  };

  return (
    <>
      <Grid item xs={12}>
        <FormControl variant={"outlined"} fullWidth>
          <TextField
            id="node-repulsion-field"
            label="Node repulsion"
            type="number"
            InputLabelProps={{ shrink: true }}
            variant="outlined"
            onChange={onChangeNodeRepulsion}
            value={layout.nodeRepulsion}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl variant={"outlined"} fullWidth>
          <TextField
            id="ideal-edge-length-field"
            label="Ideal edge length"
            type="number"
            InputLabelProps={{ shrink: true }}
            variant="outlined"
            onChange={onChangeIdealEdgeLength}
            value={layout.idealEdgeLength}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl variant={"outlined"} fullWidth>
          <TextField
            id="edge-elasticity-field"
            label="Edge elasticity"
            type="number"
            helperText="Divisor to compute edge forces"
            InputLabelProps={{ shrink: true }}
            variant="outlined"
            InputProps={{
              inputProps: {
                max: 1, min: 0, step: 0.01
              }
            }}
            onChange={onChangeEdgeElasticity}
            value={layout.edgeElasticity}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl variant={"outlined"} fullWidth>
          <TextField
            id="gravity-field"
            label="Gravity"
            type="number"
            helperText="Gravity force (constant)"
            InputLabelProps={{ shrink: true }}
            variant="outlined"
            InputProps={{
              inputProps: {
                max: 1, min: 0, step: 0.01
              }
            }}
            onChange={onChangeGravity}
            value={layout.gravity}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl variant={"outlined"} fullWidth>
          <TextField
            id="number-of-iterations-field"
            label="Number of iterations"
            type="number"
            InputLabelProps={{ shrink: true }}
            variant="outlined"
            helperText="Maximum number of iterations to perform"
            onChange={onChangeNumberOfIterations}
            value={layout.numIter}
          />
        </FormControl>
      </Grid>
    </>
  )
}