import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import React, { useContext } from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import { CyContext } from "../../../../../../contexts/cy-context";
import { CyCustomEvents } from "../../../../../../cy-extensions/cy-custom-events";
import { PreferencesContext } from "../../../../../../contexts/preferences-context";
import { getLayoutOptions } from "../../../../../../options/layout/layout";
import { LayoutForm } from "./LayoutForm";

const useStyles = makeStyles((theme) => createStyles({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15), flexBasis: '33.33%', flexShrink: 0,
  },
}));

export function LayoutPreferencesAccordion({ expanded, onAccordionChange }) {
  const classes = useStyles();
  const [cy] = useContext(CyContext);
  const [preferences, setPreferences] = useContext(PreferencesContext);

  const update = async (patch) => {
    if (patch.name) {
      const layoutOptions = getLayoutOptions(patch.name);
      const preferencesPatch = {
        layout: layoutOptions
      }

      await setPreferences(preferencesPatch);

      cy.emit(CyCustomEvents.LAYOUT_CHANGED, [[], true]);
    } else {
      const preferencesPatch = {
        layout: {
          ...preferences.layout,
          ...patch
        },
      };

      await setPreferences(preferencesPatch);

      cy.emit(CyCustomEvents.LAYOUT_CHANGED);
    }
  }

  return (
    <Accordion expanded={expanded} onChange={onAccordionChange}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon/>}
        aria-controls="layout-panel-content"
        id="layout-panel-header"
      >
        <Typography className={classes.heading}>Layout</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.root}>
        <LayoutForm
          layout={preferences?.layout}
          updateOption={update}/>
      </AccordionDetails>
    </Accordion>
  );
}