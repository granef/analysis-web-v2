import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import React  from "react";
import { ColaPreferencesForm } from "./ColaPreferencesForm";
import { FcosePreferencesForm } from "./FcosePreferencesForm";
import { Loader } from "../../../../../common/Loader";

const layoutOptions = ["preset", "circle", "concentric", "grid", "cola", "euler", "fcose", "cose", "cose-bilkent",]

export function LayoutForm({ layout, updateOption }) {

  return layout ? (
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormControl variant={"outlined"} fullWidth>
            <InputLabel id="layout-select-label">Layout</InputLabel>
            <Select
              variant="outlined"
              labelId="layout-select-label"
              label="Layout"
              id="layout-select"
              value={layout.name}
              onChange={(event) => {
                const newValue = event.target.value;
                updateOption({name: newValue});
              }}
            >
              {layoutOptions.map((option, index) => <MenuItem value={option} key={index}>{option}</MenuItem>)}
            </Select>
          </FormControl>
        </Grid>

        {layout.name === 'cola' && (
          <ColaPreferencesForm layout={layout} updateLayoutOptions={updateOption}/>
        )}

        {layout.name === 'fcose' && (
          <FcosePreferencesForm layout={layout} updateLayoutOptions={updateOption}/>
        )}
      </Grid>
  ) : (
    <Loader width="100%" height="250px"/>
  )
}