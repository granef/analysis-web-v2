import React from "react";
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import { TextField } from "@material-ui/core";

export function ColaPreferencesForm({ layout, updateLayoutOptions }) {
  const onChangeNodeSpacing = (evt) => {
    updateLayoutOptions({
      nodeSpacing: evt.target.value
    });
  };

  const onChangeConvergenceThreshold = (evt) => {
    if (evt.target.value < 0 || evt.target.value > 1) {
      console.error('invalid convergence threshold', evt.target.value)
      return;
    }
    updateLayoutOptions({
      convergenceThreshold: evt.target.value
    });
  };

  return (
    <>
      <Grid item xs={12}>
        <FormControl variant={"outlined"} fullWidth>
          <TextField
            id="node-spacing-field"
            label="Node spacing"
            type="number"
            helperText="Extra spacing around nodes"
            InputLabelProps={{ shrink: true }}
            variant="outlined"
            onChange={onChangeNodeSpacing}
            value={layout.nodeSpacing}
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl variant={"outlined"} fullWidth>
          <TextField
            id="convergence-threshold-field"
            label="Convergence threshold"
            type="number"
            helperText="When the alpha value (system energy) falls below this value, the layout stops"
            InputLabelProps={{ shrink: true }}
            variant="outlined"
            onChange={onChangeConvergenceThreshold}
            value={layout.convergenceThreshold}
            InputProps={{
              inputProps: {
                max: 1, min: 0, step: 0.01
              }
            }}
          />
        </FormControl>
      </Grid>
    </>
  )
}