import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import React from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import {
  createColor,
  deleteColor,
  getColors,
} from "../../../../../../api/annotations-api";
import ColoredAnnotationOverview from "../../annotations/ColoredAnnotationOverview";
import { useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => createStyles({
  root: {
    width: '100%',
    overflow: "auto",
  },
  content: {
    overflow: "auto",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15), flexBasis: '33.33%', flexShrink: 0,
  },
}));

export function ColorsAccordion({ expanded, onAccordionChange }) {
  const classes = useStyles();
  const { id } = useParams();

  return (
    <Accordion expanded={expanded} onChange={onAccordionChange}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon/>}
        aria-controls="colors-panel-content"
        id="colors-panel-header"
      >
        <Typography className={classes.heading}>Colors</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.root}>
        {expanded && <div className={classes.content}>
          <ColoredAnnotationOverview
            listApi={() => getColors(id)}
            createApi={(color) => createColor({ ...color, visualization: { id } })}
            deleteApi={deleteColor}
          />
        </div>}
      </AccordionDetails>
    </Accordion>
  );
}