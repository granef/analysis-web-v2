import React, { useState } from "react";
import { PanelCard } from "../../../../common/PanelCard";
import CardContent from "@material-ui/core/CardContent";
import { CardActions, makeStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { LayoutPreferencesAccordion } from "./layout/LayoutPreferencesAccordion";
import { StylePreferencesAccordion } from "./style/StylePreferencesAccordion";
import { PanelTitle } from "../PanelTitle";
import { TimelineClusteringPreferencesAccordion } from "./clustering/ClusteringPreferencesAccordion";
import { TagsAccordion } from "./tags/TagsAccordion";
import { ColorsAccordion } from "./colors/ColorsAccordion";

const useStyles = makeStyles(theme => ({
  content: {
    overflow: "auto",
  },
  rightAlignItems: {
    marginLeft: "auto"
  },
  accordionDiv: {
    width: '100%',
  }
}));

export function PreferencesPanel({ open, onClose }) {
  const classes = useStyles();
  const [expanded, setExpanded] = useState('');

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  return (
    <PanelCard open={open}>
      <CardContent classes={{
        root: classes.content
      }}>
        <PanelTitle align="center">
          Preferences
        </PanelTitle>
        <div className={classes.accordionDiv}>
          <LayoutPreferencesAccordion
            expanded={expanded === 'layout-panel'}
            onAccordionChange={handleChange('layout-panel')}/>
          <StylePreferencesAccordion
            expanded={expanded === 'stylesheet-panel'}
            onAccordionChange={handleChange('stylesheet-panel')}/>
          <TimelineClusteringPreferencesAccordion
            expanded={expanded === 'clustering-panel'}
            onAccordionChange={handleChange('clustering-panel')}/>
          <TagsAccordion
            expanded={expanded === 'tags-panel'}
            onAccordionChange={handleChange('tags-panel')}/>
          <ColorsAccordion
            expanded={expanded === 'colors-panel'}
            onAccordionChange={handleChange('colors-panel')}/>
        </div>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={onClose} className={classes.rightAlignItems}>Close</Button>
      </CardActions>
    </PanelCard>
  )
}