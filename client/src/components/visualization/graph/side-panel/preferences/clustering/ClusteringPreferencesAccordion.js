import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import React, { useContext } from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import { PreferencesContext } from "../../../../../../contexts/preferences-context";
import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { TextField } from "@material-ui/core";

const useStyles = makeStyles((theme) => createStyles({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15), flexBasis: '33.33%', flexShrink: 0,
  },
}));

const timeUnits = ["years", "months", "weeks", "days", "hours", "minutes", "seconds", "milliseconds"];

export function TimelineClusteringPreferencesAccordion({ expanded, onAccordionChange }) {
  const classes = useStyles();
  const [preferences, setPreferences] = useContext(PreferencesContext);

  return (
    <Accordion expanded={expanded} onChange={onAccordionChange}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon/>}
        aria-controls="clustering-panel-content"
        id="clustering-panel-header"
      >
        <Typography className={classes.heading}>Timeline Clustering</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.root}>
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <FormControl variant={"outlined"} fullWidth>
              <TextField
                id="value"
                label="Value"
                type="number"
                InputLabelProps={{ shrink: true }}
                variant="outlined"
                onChange={(event) => {
                  const newValue = event.target.value;
                  if (newValue > 0) {
                    setPreferences({
                      timelineClustering: {
                        unit: preferences?.timelineClustering?.unit,
                        value: newValue,
                      }
                    });
                  }
                }}
                error={preferences?.timelineClustering?.value <= 0}
                value={preferences?.timelineClustering?.value}
                InputProps={{
                  inputProps: {
                    min: 1, step: 1
                  }
                }}
              />
            </FormControl>
          </Grid>
          <Grid item xs={6}>
            <FormControl variant={"outlined"} fullWidth>
              <InputLabel id="time-unit-select-label">Time unit</InputLabel>
              <Select
                variant="outlined"
                labelId="time-unit-select-label"
                label="Time unit"
                id="time-unit-select"
                value={preferences?.timelineClustering?.unit}
                onChange={(event) => {
                  const newValue = event.target.value;
                  setPreferences({
                    timelineClustering: {
                      unit: newValue,
                      value: preferences?.timelineClustering?.value,
                    }
                  });
                }}
              >
                {timeUnits.map((timeUnit, index) => <MenuItem value={timeUnit} key={index}>{timeUnit}</MenuItem>)}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
}