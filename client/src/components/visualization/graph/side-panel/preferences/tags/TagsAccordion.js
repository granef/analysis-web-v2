import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import React from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import {
  createTag,
  deleteTag,
  getTags
} from "../../../../../../api/annotations-api";
import ColoredAnnotationOverview from "../../annotations/ColoredAnnotationOverview";
import { useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => createStyles({
  root: {
    width: '100%',
    overflow: "auto",
  },
  content: {
    overflow: "auto",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15), flexBasis: '33.33%', flexShrink: 0,
  },
}));

export function TagsAccordion({ expanded, onAccordionChange }) {
  const classes = useStyles();
  const { id } = useParams();

  return (
    <Accordion expanded={expanded} onChange={onAccordionChange} style={{width: '100%'}}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon/>}
        aria-controls="tags-panel-content"
        id="tags-panel-header"
      >
        <Typography className={classes.heading}>Tags</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.root}>
        {expanded && <div className={classes.content}>
          <ColoredAnnotationOverview
            listApi={() => getTags(id)}
            createApi={tag => createTag({ ...tag, visualization: { id } })}
            deleteApi={deleteTag}
          />
        </div>}
      </AccordionDetails>
    </Accordion>
  );
}