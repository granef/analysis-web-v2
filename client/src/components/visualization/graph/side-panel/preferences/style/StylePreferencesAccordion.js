import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import React, { useContext } from "react";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import { CyContext } from "../../../../../../contexts/cy-context";
import { selectedNodeStyle } from "../../../../../../options/stylesheet";
import { StyleForm } from "./StyleForm";
import { PreferencesContext } from "../../../../../../contexts/preferences-context";

const useStyles = makeStyles((theme) => createStyles({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15), flexBasis: '33.33%', flexShrink: 0,
  },
}));

export function StylePreferencesAccordion({ expanded, onAccordionChange }) {
  const classes = useStyles();
  const [cy] = useContext(CyContext);
  const [preferences, setPreferences] = useContext(PreferencesContext);

  const styleUpdateHandler = (styleUpdate) => {
    switch (styleUpdate.attribute) {
      case 'nodeBorders': {
        const borderWidth = styleUpdate.value ? 3 : 0;
        cy.style()
          .selector('node').style('border-width', borderWidth)
          .selector(':selected').style(selectedNodeStyle) // refresh styles for selected nodes (otherwise they would be overloaded when turning off node borders)
          .update();
        break;
      }
      case 'nodeLabels': {
        const nodeLabel = styleUpdate.value ? 'data(label)' : '';
        cy.style()
          .selector('node').style('label', nodeLabel)
          .update();
        break;
      }
      case 'edgeArrows': {
        const edgeArrow = styleUpdate.value ? 'triangle' : 'none';
        cy.style()
          .selector('edge').style('target-arrow-shape', edgeArrow)
          .update();
        break;
      }
      case 'edgeLabels': {
        const edgeLabel = styleUpdate.value ? 'data(label)' : '';
        cy.style()
          .selector('edge').style('label', edgeLabel)
          .update();
        break;
      }
      default: {
        console.error('Unknown attribute', styleUpdate);
        return;
      }
    }
    setPreferences({
      style: {
        [styleUpdate.attribute]: styleUpdate.value
      }
    });
  }

  return (
    <Accordion expanded={expanded} onChange={onAccordionChange}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon/>}
        aria-controls="stylesheet-panel-content"
        id="stylesheet-panel-header"
      >
        <Typography className={classes.heading}>Stylesheet</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.root}>
        <StyleForm
          style={preferences?.style}
          updateOption={styleUpdateHandler}/>
      </AccordionDetails>
    </Accordion>
  );
}