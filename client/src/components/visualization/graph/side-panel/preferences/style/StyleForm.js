import Grid from "@material-ui/core/Grid";
import FormControl from "@material-ui/core/FormControl";
import { Checkbox, FormControlLabel, FormGroup, FormLabel } from "@material-ui/core";
import React from "react";
import { Loader } from "../../../../../common/Loader";

export function StyleForm({ style, updateOption }) {

  const handleChangeChecked = (updateHandler) => (event) => {
    updateHandler(event.target.checked);
  };

  const updateNodeBorders = checked => {
    updateOption({ attribute: 'nodeBorders', value: checked });
  }

  const updateNodeLabels = checked => {
    updateOption({ attribute: 'nodeLabels', value: checked });
  }

  const updateEdgeArrows = checked => {
    updateOption({ attribute:'edgeArrows', value: checked });
  }

  const updateEdgeLabels = checked => {
    updateOption({ attribute: 'edgeLabels', value: checked });
  }

  return style ? (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Nodes</FormLabel>
          <FormGroup>
            <FormControlLabel
              control={<Checkbox
                checked={style?.nodeLabels}
                onChange={handleChangeChecked(updateNodeLabels)}
                name="nodeLabels"
              />}
              label="Show labels"
            />
            <FormControlLabel
              control={<Checkbox
                checked={style?.nodeBorders}
                onChange={handleChangeChecked(updateNodeBorders)}
                name="nodeBorders"
              />}
              label="Borders"
            />
          </FormGroup>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Edges</FormLabel>
          <FormGroup>
            <FormControlLabel
              control={<Checkbox
                checked={style?.edgeLabels}
                onChange={handleChangeChecked(updateEdgeLabels)}
                name="edgeLabels"
              />}
              label="Show labels"
            />
            <FormControlLabel
              control={<Checkbox
                checked={style?.edgeArrows}
                onChange={handleChangeChecked(updateEdgeArrows)}
                name="edgeArrows"
              />}
              label="Arrows"
            />
          </FormGroup>
        </FormControl>
      </Grid>
    </Grid>
  ) : (
    <Loader width="100%" height="250px"/>
  );
}