import React, { useEffect, useState } from "react";
import { Card, CardActions, CardContent } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import ColoredAutocomplete from "../side-panel/annotations/ColoredAutocomplete";
import { getTags } from "../../../../api/annotations-api";
import { useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  select: {
    margin: theme.spacing(0.5),
    width: 500
  },
}));

const TagsPicker = ({ addTags, removeTags, onClose }) => {
  const classes = useStyles();

  const { id } = useParams();

  const [tags, setTags] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);

  useEffect(() => {
    if (id) {
      getTags(id).then(response => {
        setTags(response.data);
      });
    }
  }, [id]);

  return (
    <Card>
      <CardContent>
        <ColoredAutocomplete
          label="Tags"
          options={tags}
          selectedValues={selectedTags}
          setSelectedValues={setSelectedTags}
          className={classes.select}
        />
      </CardContent>
      <CardActions>
        <Button size="small" color="primary" onClick={() => addTags(selectedTags)}>
          Add
        </Button>
        <Button size="small" color="primary" onClick={() => removeTags(selectedTags)}>
          Remove
        </Button>
        <Button size="small" color="primary" onClick={onClose}>
          Close
        </Button>
      </CardActions>
    </Card>
  )
};

export default TagsPicker;