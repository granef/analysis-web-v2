import { Core, NodeDefinition, NodeSingular } from 'cytoscape';
import { getBaseNodes } from '../../../../cy-extensions/clustering/utils';
import { getTags, getVisualizationNodesDetails } from '../../../../api/annotations-api';

export const downloadJSON = async (cy: Core, visualizationId: string) => {
  const getGranefData = ({ id, label, bg, width, height, layoutLocked, cluster, innerNodes, zIndex, ...granefData }: any) => granefData;

  const baseNodesDataReducer = (baseNodeDataArray: any[], node: NodeSingular) => {
    if (node.data('cluster') === true) {
      const baseNodesData = getBaseNodes(node.data('innerNodes'))
        .map((node: NodeDefinition) => getGranefData(node.data));
      return baseNodeDataArray.concat(baseNodesData);
    } else {
      baseNodeDataArray.push(getGranefData(node.data()));
      return baseNodeDataArray;
    }
  }

  const visualizationTags = await getTags(visualizationId);

  let details: any = {};
  try {
    const detailsResponse = await getVisualizationNodesDetails(visualizationId);
    details = detailsResponse.data;
  } catch (error) {
    console.log(error);
    return;
  }
  const nodeTags: {
    [uid: string]: string[]
  } = details.reduce((acc: any, node: any) => {
    acc[node.granefId] = node.tags;
    return acc;
  }, {});

  const nodes = cy.nodes()
    .reduce(baseNodesDataReducer, [])
    .map((node: any) => nodeTags[node.uid] ? ({ ...node, tags: nodeTags[node.uid] }) : (node));
  const edges = cy.edges().map(edge => ({
    id: edge.id(),
    source: edge.source().id(),
    target: edge.target().id(),
  }));

  const file = new Blob([JSON.stringify({
    tags: visualizationTags.data,
    edges,
    nodes,
  }, null, 2)], { type: 'application/json' });

  const a = document.createElement('a');
  a.href = URL.createObjectURL(file);
  a.download = 'test-visualization.json';

  a.click();
};
