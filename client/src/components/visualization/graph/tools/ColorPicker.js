import { Card, CardActions, CardContent } from "@material-ui/core";
import { CirclePicker } from "react-color";
import Button from "@material-ui/core/Button";
import React, { useEffect, useState } from "react";
import { getColors } from "../../../../api/annotations-api";
import { useParams } from "react-router-dom";

const ColorPicker = ({ apply, onClose }) => {
  const { id } = useParams();

  const [colors, setColors] = useState([]);
  const [color, setColor] = useState('#ff0000');

  useEffect(() => {
    if (id) {
      getColors(id).then(response => {
        setColors(response.data.map(color => color.hex));
      });
    }
  }, [id]);

  const handleChange = (color) => {
    setColor(color.hex);
    apply(color.hex);
    onClose(color);
  };

  return (
    <Card>
      <CardContent>
        <CirclePicker
          colors={colors}
          color={color}
          // onChange={handleChange} // if both onChange and onChangeComplete are called, nodes are colored twice
          onChangeComplete={handleChange}
        />
      </CardContent>
      <CardActions>
        <Button size="small" color="primary" onClick={() => onClose(color)}>
          Close
        </Button>
      </CardActions>
    </Card>
  )
};

export default ColorPicker;