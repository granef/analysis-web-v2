import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import PictureInPictureIcon from "@material-ui/icons/PictureInPicture";
import GestureIcon from "@material-ui/icons/Gesture";
import Tooltip from "@material-ui/core/Tooltip";

const useStyles = makeStyles((theme) => ({
  buttonGroup: {
    margin: theme.spacing(0.5),
  },
  button: {
    margin: theme.spacing(0),
    padding: theme.spacing(0.4, 1.1),
    '&:hover': {
      backgroundColor: theme.palette.primary.light,
    },
  }
}));

export default function ToggleSelectionMode({ selection, toggleLasso }) {
  const classes = useStyles();

  const handleSelection = (event, newSelection) => {
    if (newSelection !== null) {
      toggleLasso(newSelection);
    }
  };

  return (
    <ToggleButtonGroup
      classes={{ root: classes.buttonGroup }}
      //color="primary"
      size="small"
      value={selection}
      exclusive
      onChange={handleSelection}
      aria-label="Selection method"
    >
      <ToggleButton
        value="lasso"
        aria-label="lasso selection"
        size="small"
        classes={{ root: classes.button }}
        //color="primary"
      >
        <Tooltip title="Lasso selection" arrow>
          <GestureIcon fontSize="small"/>
        </Tooltip>
      </ToggleButton>
      <ToggleButton
        value="box"
        aria-label="box selection"
        size="small"
        classes={{ root: classes.button }}
        //color="primary"
      >
        <Tooltip title="Box selection" arrow>
          <PictureInPictureIcon fontSize="small"/>
        </Tooltip>
      </ToggleButton>
    </ToggleButtonGroup>
  );
}
