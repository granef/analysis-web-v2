import React, { useContext, useState } from "react";
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { Checkbox, FormControlLabel, FormGroup, FormLabel, Radio, RadioGroup } from "@material-ui/core";
import { CyContext } from "../../../../contexts/cy-context";

const TRANSPARENT = "rgba(0, 0, 0, 0)";
const WHITE = "rgba(256, 256, 256, 1)";

const PNG = "png";
const JPG = "jpg";

export function DownloadImageDialog({ open, close }) {
  const [cy] = useContext(CyContext);
  const [options, setOptions] = useState({
    format: PNG,
    bg: WHITE,
    full: true,
    scale: 3,
  });

  const downloadImage = () => {
    const { format, ...restOptions } = options;
    const img = format === 'jpg' ? cy.jpg(restOptions) : cy.png(restOptions);
    const linkSource = 'data:png;base64' + img;

    const downloadLink = document.createElement('a');
    downloadLink.href = linkSource;
    downloadLink.download = `visualization.${format}`;

    downloadLink.click();
  };

  return (
    <Dialog open={open} onClose={close} aria-labelledby="fetch-data-dialog-title">
      <DialogTitle id="fetch-data-dialog-title">Download as Image</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Choose options
        </DialogContentText>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <FormControl component="fieldset">
              <FormLabel component="legend">Format</FormLabel>
              <RadioGroup
                aria-label="format"
                name="format"
                value={options.format}
                onChange={(evt) => {
                  setOptions(prev => ({
                    ...prev,
                    format: evt.target.value,
                    bg: evt.target.value === JPG ? WHITE : prev.bg
                  }))
                }}
              >
                <FormControlLabel value={PNG} control={<Radio/>} label="PNG"/>
                <FormControlLabel value={JPG} control={<Radio/>} label="JPG"/>
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <FormControl component="fieldset">
              <FormLabel component="legend">Output options</FormLabel>
              <FormGroup>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={options.full}
                      onChange={(evt) => {
                        setOptions(prev => ({
                          ...prev,
                          full: evt.target.checked
                        }))
                      }}
                      name="Full viewport"
                    />
                  }
                  label="Full viewport"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={options.bg === TRANSPARENT}
                      onChange={(evt) => {
                        setOptions(prev => ({
                          ...prev,
                          bg: evt.target.checked ? TRANSPARENT : WHITE
                        }))
                      }}
                      name="Transparent background"
                      disabled={options.format === JPG}
                    />
                  }
                  label="Transparent background"
                  // disabled={options.format = JPG}
                />
              </FormGroup>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <FormControl fullWidth>
              <TextField
                id="scale-field"
                label="Scale"
                type="number"
                helperText="Positive number (the higher the value, the higher the quality)"
                InputLabelProps={{ shrink: true }}
                variant="outlined"
                onChange={(evt) => {
                  setOptions(prev => ({
                    ...prev,
                    scale: evt.target.value
                  }))
                }}
                value={options.scale}
              />
            </FormControl>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={close} color="secondary">
          Cancel
        </Button>
        <Button onClick={() => {
          downloadImage();
          close();
        }} color="primary">
          Download
        </Button>
      </DialogActions>
    </Dialog>
  );

}