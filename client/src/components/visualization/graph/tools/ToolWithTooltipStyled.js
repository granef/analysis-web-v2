import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { IconButtonWithTooltip } from "../../../common/IconButtonWithTooltip";


const useStyles = makeStyles((theme) => ({
  tool: {
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
      color: '#ffffff'
    },
  },
  label: {
    color: '#ffffff'
  },
}));

export function ToolWithTooltipStyled({ ...props }) {
  const classes = useStyles();

  return (
    <IconButtonWithTooltip
        classes={{
          root: classes.tool,
          label: classes.label
        }}
        {...props}
    />
  )
}