import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import { VisualizationToolsBar } from "./VisualizationToolsBar";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { NodeDetail } from "../detail/NodeDetail";
import SearchIcon from '@material-ui/icons/Search';
import SettingsIcon from '@material-ui/icons/Settings';
import EditIcon from '@material-ui/icons/Edit';
import { QueryPanel } from "../side-panel/query/QueryPanel";
import { PreferencesPanel } from "../side-panel/preferences/PreferencesPanel";
import { DownloadImageDialog } from "./DownloadImageDialog";
import { CyCustomEvents } from "../../../../cy-extensions/cy-custom-events";
import { Panel } from "../side-panel/panel-enum";
import { useHistory, useParams } from "react-router-dom";
import { CyContext } from "../../../../contexts/cy-context";
import { FetchContextProvider } from "../../../../contexts/fetch-context";
import { TimelinePanel } from "../timeline/TimelinePanel";
import { MultiIntervalTimeline } from "../timeline/multi-interval/MultiIntervalTimeline";
import { SingleIntervalTimeline } from "../timeline/single-interval/SingleIntervalTimeline";
import { IconButtonWithTooltip } from "../../../common/IconButtonWithTooltip";
import { useIsEmpty } from "../../../../hooks/use-is-empty";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    borderColor: theme.palette.secondary.dark,
    borderWidth: 2,
  },
  grid: {
    height: "100%"
  },
  detailsGrid: {
    height: "max-content",
  },
  detailsBox: {
    zIndex: theme.zIndex.panel,
  },
  panelBox: {
    [theme.breakpoints.down('xs')]: {
      maxHeight: "calc(100vh - 48px)",
    },
    [theme.breakpoints.up('sm')]: {
      maxHeight: "calc(100vh - 56px)",
    },
    [theme.breakpoints.up('md')]: {
      maxHeight: "calc(100vh - 64px)",
    },
    zIndex: theme.zIndex.panel
  },
  buttonsGroup: {
    margin: theme.spacing(0.5),
  },
  buttonsGroupButton: {
    color: "#fff",
  },
}));

export function VisualizationGrid({ processActionResult }) {
  const classes = useStyles();
  const history = useHistory();

  const { id } = useParams();

  const [cy] = useContext(CyContext);

  const [downloadImageDialog, setDownloadImageDialog] = useState(false);
  const [panelOpen, setPanelOpen] = useState();

  const [details, setDetails] = useState();

  const [timeline, setTimeline] = useState(null);

  const isEmpty = useIsEmpty(cy);


  useEffect(() => {
    if (cy) {
      const showDetailCallback = function (evt) {
        if (!evt.cy.scratch('GRANEF_DETAILS_LOCKED')) {
          let showData = true;
          evt.cy.once('mouseout', () => {
            showData = false;
          });
          setTimeout(() => {
            if (showData) {
              setDetails(evt.target.data());
            }
          }, 250);
        }
      }
      const showDetailCallbackLocking = function (evt) {
        setDetails(evt.target.data());
      }
      const resetDetailsCallback = () => setDetails(undefined);

      const resetDetailsEvents = [CyCustomEvents.FETCH, CyCustomEvents.DELETE_SELECTION, CyCustomEvents.CLUSTER_SELECTION, CyCustomEvents.OPEN_CLUSTER].join(' ');

      cy.on("taphold dblclick", "node", showDetailCallbackLocking);
      cy.on("mouseover", "node", showDetailCallback);
      cy.on(resetDetailsEvents, resetDetailsCallback);

      return () => {
        cy.off("taphold dblclick", "node", showDetailCallbackLocking);
        cy.off("mouseOver", "node", showDetailCallback);
        cy.off(resetDetailsEvents, resetDetailsCallback);
      }
    }
  }, [cy]);

  const toggleTimeline = () => {
    cy.nodes().removeClass('timelineHidden');
    setTimeline('single');
  }

  const toggleMultiSelectTimeline = () => {
    cy.nodes().removeClass('timelineHidden');
    setTimeline('multi');
  }

  const openDownloadImageDialog = () => {
    setDownloadImageDialog(true);
  }

  const switchPanelFunc = (val) => () => {
    setPanelOpen(val);
  }

  const closeNodeDetail = () => {
    setDetails(undefined);
  }

  const openSelectionDetail = () => {
    const selection = cy?.nodes(':selected');
    const uids = selection.getGranefUids();
    if (uids.length === 1) {
      setDetails(cy.getElementById(uids[0]).first().data());
    } else if (uids.length > 1) {
      setDetails({
        selection: true,
        uids: uids,
        topLevelIds: selection.map((node) => node.id())
      });
    } else {
      processActionResult({
        severity: "error",
        message: "Cannot show detail for empty selection."
      });
    }
  }

  // Set the query panel open if the graph is empty
  if (isEmpty && !panelOpen) {
    setPanelOpen(Panel.QUERY)
  }

  return (
    <FetchContextProvider>
      <div className={classes.root}>
        <Grid container className={classes.grid}>
          <Grid item xs>
            <VisualizationToolsBar
              downloadImage={openDownloadImageDialog}
              toggleTimeline={toggleTimeline}
              toggleMultiSelectTimeline={toggleMultiSelectTimeline}
              openSelectionDetail={openSelectionDetail}
            />
            <ButtonGroup disableElevation className={classes.buttonsGroup} color="primary">
              <IconButtonWithTooltip title="Search" onClick={switchPanelFunc(Panel.QUERY)} className={classes.buttonsGroupButton} color="primary" Icon={SearchIcon}/>
              <IconButtonWithTooltip title="Preferences" onClick={switchPanelFunc(Panel.PREFERENCES)} className={classes.buttonsGroupButton} color="primary" Icon={SettingsIcon}/>
              <IconButtonWithTooltip title="Details" onClick={() => history.push(`/visualizations/${id}/details`)} className={classes.buttonsGroupButton} color="primary" Icon={EditIcon}/>
            </ButtonGroup>
          </Grid>
          {details && (
            <Grid item xs={12} md={3} className={classes.detailsGrid}>
              <Box classes={classes.detailsBox}  position="relative" padding={1}>
                <NodeDetail
                  onClose={closeNodeDetail}
                  data={details}
                />
              </Box>
            </Grid>
          )}
          {(panelOpen || timeline != null) && (
            <Grid item xs={12} sm={4} md={3}>
              <Box
                position="relative"
                padding={1}
                className={classes.panelBox}
                overflow="auto"
              >
                {timeline != null && (
                  <TimelinePanel
                    title="Timeline"
                    close={() => setTimeline(null)}
                    Timeline={timeline === 'single' ? SingleIntervalTimeline : MultiIntervalTimeline}
                  />
                )}
                <QueryPanel
                  open={panelOpen === Panel.QUERY}
                  onClose={() => setPanelOpen(undefined)}
                  selectByParams={(params) => cy?.selectByParams(params)}
                  processActionResult={processActionResult}
                />
                <PreferencesPanel
                  open={panelOpen === Panel.PREFERENCES}
                  onClose={() => setPanelOpen(undefined)}
                />
              </Box>
            </Grid>
          )}
        </Grid>
      </div>

      <DownloadImageDialog
        open={downloadImageDialog}
        close={() => setDownloadImageDialog(false)}
      />
    </FetchContextProvider>
  )
}
