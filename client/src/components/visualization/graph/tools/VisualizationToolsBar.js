import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import ShareIcon from '@material-ui/icons/Share';
import LockIcon from '@material-ui/icons/Lock';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import CenterFocusStrongIcon from '@material-ui/icons/CenterFocusStrong';
import PaletteIcon from '@material-ui/icons/Palette';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import LabelIcon from '@material-ui/icons/Label';
import ColorPicker from "./ColorPicker";
import GroupWorkIcon from '@material-ui/icons/GroupWork';
import GetAppIcon from '@material-ui/icons/GetApp';
import SaveIcon from '@material-ui/icons/Save';
import ImageIcon from '@material-ui/icons/Image';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import VisibilityIcon from '@material-ui/icons/Visibility';
import LayersIcon from '@material-ui/icons/Layers';
import LayersClearIcon from '@material-ui/icons/LayersClear';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import GroupWorkOutlinedIcon from '@material-ui/icons/GroupWorkOutlined';
import FlipIcon from '@material-ui/icons/Flip';
import TimelapseIcon from '@material-ui/icons/Timelapse';
import InfoIcon from '@material-ui/icons/Info';
import Popper from "@material-ui/core/Popper";
import Fade from "@material-ui/core/Fade";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import TagsPicker from "./TagsPicker";
import { ToolWithTooltipStyled } from "./ToolWithTooltipStyled";
import ToggleSelectionMode from "./ToggleSelectionMode";
import { CyContext } from "../../../../contexts/cy-context";
import { useParams } from "react-router-dom";
import { downloadJSON } from "./download-json";
import { PreferencesContext } from "../../../../contexts/preferences-context";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  buttonGroup: {
    margin: theme.spacing(1, 0.5),
  },
  popper: {
    position: "relative",
    zIndex: theme.zIndex.toolsPopper,
  }
}));

export function VisualizationToolsBar({ downloadImage, toggleTimeline, toggleMultiSelectTimeline, openSelectionDetail }) {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState(null);
  const [open, setOpen] = useState(false);
  const [hover, setHover] = useState(true);
  const [popperTool, setPopperTool] = useState();

  const { id } = useParams();

  const [cy] = useContext(CyContext);
  const [preferences, patchPreferences] = useContext(PreferencesContext);

  useEffect(() => {
    if (preferences?.selection) {
      cy?.lassoSelectionEnabled(preferences.selection === 'lasso');
    }
  }, [cy, preferences?.selection]);

  const toggleLasso = (selectionMethod) => {
    patchPreferences({
      selection: selectionMethod
    });
  }

  const handleAnnotationsToolClick = (tool) => (event) => {
    setPopperTool(tool);
    setAnchorEl(event.currentTarget);
    setOpen(true);
  };

  const closePopper = () => {
    setPopperTool(undefined);
    setAnchorEl(null);
    setOpen(false);
  };

  function addTagsAndClose(tags) {
    setOpen(false);
    cy?.addTags(tags);
  }

  function removeTagsAndClose(tags) {
    setOpen(false);
    cy?.removeTags(tags);
  }

  function toggleOnHover() {
    setHover(!hover);
    cy?.scratch('GRANEF_DETAILS_LOCKED', hover);
  }

  const buttonGroups = [
    [
      { title: "Fit", onClick: () => cy?.fitPadded(), Icon: CenterFocusStrongIcon, Component: ToolWithTooltipStyled },
      { title: "Zoom in", onClick: () => cy?.zoomIn(), Icon: ZoomInIcon, Component: ToolWithTooltipStyled },
      { title: "Zoom out", onClick: () => cy?.zoomOut(), Icon: ZoomOutIcon, Component: ToolWithTooltipStyled }
    ],
    [
      { title: "Lock", onClick: () => cy?.lockSelected(), Icon: LockIcon, Component: ToolWithTooltipStyled },
      { title: "Unlock", onClick: () => cy?.unlockSelected(), Icon: LockOpenIcon, Component: ToolWithTooltipStyled },
      { title: "Resume physics", onClick: () => cy?.layoutUnlockSelected(), Icon: ShareIcon, Component: ToolWithTooltipStyled, disabled: preferences?.layout?.name !== 'cola' },
    ],
    [
      { title: "Hide selected", onClick: () => cy?.hideSelected(), Icon: VisibilityOffIcon, Component: ToolWithTooltipStyled },
      { title: "Show hidden", onClick: () => cy?.showSelected(), Icon: VisibilityIcon, Component: ToolWithTooltipStyled },
    ],
    [
      { title: "Delete", onClick: () => cy?.deleteSelection(id), Icon: DeleteForeverIcon, Component: ToolWithTooltipStyled },
      { title: "Invert selection", onClick: () => cy?.invertSelection(), Icon: FlipIcon, Component: ToolWithTooltipStyled },
      { title: "Assign color", onClick: handleAnnotationsToolClick("COLOR"), Icon: PaletteIcon, Component: ToolWithTooltipStyled },
      { title: "Assign tag", onClick: handleAnnotationsToolClick("TAG"), Icon: LabelIcon, Component: ToolWithTooltipStyled },
      { title: "Selection detail", onClick: openSelectionDetail, Icon: InfoIcon, Component: ToolWithTooltipStyled },
      { title: "Toogle detail on hover", onClick: toggleOnHover, Icon: hover ? LayersIcon : LayersClearIcon, Component: ToolWithTooltipStyled },
    ],
    [
      { title: "Cluster outliers", onClick: () => cy?.clusterOutliers(), Icon: GroupWorkIcon, Component: ToolWithTooltipStyled },
      { title: "Clusterize with Chinese Whispers", onClick: () => cy?.chineseWhisperClusterize(true), Icon: GroupWorkOutlinedIcon, Component: ToolWithTooltipStyled },
      {
        title: "Clusterize connections",
        onClick: () => cy?.clusterizeByTime(null, true, moment.duration(preferences?.timelineClustering?.value, preferences?.timelineClustering?.unit).asMilliseconds()),
        Icon: GroupWorkOutlinedIcon,
        Component: ToolWithTooltipStyled },
      { title: "Cluster selection", onClick: () => cy?.clusterSelection(), Icon: AddCircleOutlineIcon, Component: ToolWithTooltipStyled },
      { title: "Open cluster", onClick: () => cy?.openClusterSelection(id), Icon: HighlightOffIcon, Component: ToolWithTooltipStyled }
    ],
    [
      { title: "Export as JSON", onClick: () => downloadJSON(cy, id), Icon: GetAppIcon, Component: ToolWithTooltipStyled },
      { title: "Export as image", onClick: downloadImage, Icon: ImageIcon, Component: ToolWithTooltipStyled },
      { title: "Save", onClick: () => cy?.save(id), Icon: SaveIcon, Component: ToolWithTooltipStyled }
    ],
    [
      { title: "Timeline", onClick: toggleTimeline, Icon: AccessTimeIcon, Component: ToolWithTooltipStyled },
      { title: "MultiSelect Timeline", onClick: toggleMultiSelectTimeline, Icon: TimelapseIcon, Component: ToolWithTooltipStyled },
    ]
  ];

  return (
    <>
      {buttonGroups.map((buttonGroup, index) =>
        <ButtonGroup disableElevation className={classes.buttonGroup} key={index}>
          {buttonGroup.map(({ Component, ...restProps }, index) => <Component {...restProps} key={index}/>)}
        </ButtonGroup>
      )}

      <ToggleSelectionMode
        selection={preferences?.selection || "lasso"}
        toggleLasso={toggleLasso}
      />

      <Popper id="popper-id" className={classes.popper} open={open} anchorEl={anchorEl} transition>
        {({ TransitionProps }) => (
          <ClickAwayListener onClickAway={closePopper}>
            <Fade {...TransitionProps} timeout={150}>
              <div>
                {popperTool === "COLOR" && (
                  <ColorPicker
                    apply={(color) => cy.colorSelection(color)}
                    onClose={closePopper}
                  />
                )}
                {popperTool === "TAG" && (
                  <TagsPicker
                    addTags={addTagsAndClose}
                    removeTags={removeTagsAndClose}
                    onClose={closePopper}
                  />
                )}
              </div>
            </Fade>
          </ClickAwayListener>
        )}
      </Popper>
    </>
  )
}
