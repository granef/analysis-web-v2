import React, { useEffect, useState } from 'react';
import Typography from "@material-ui/core/Typography";
import ChipsList from "../side-panel/annotations/chips-list";
import { useParams } from "react-router-dom";
import { getNodeDetail, unassignTag } from "../../../../api/annotations-api";

export function NodeTagsList({ nodeId }) {
  const { id: visualizationId } = useParams();
  const [tags, setTags] = useState([]);

  useEffect(() => {
    if (visualizationId && nodeId) {
      getNodeDetail(visualizationId, nodeId)
        .then(response => {
          setTags(response.data?.tags);
        })
        .catch(error => {
          console.error('error fetching node tags', error);
          setTags([]);
        });
    }
  }, [visualizationId, nodeId]);

  const handleTagDelete = (tagToDelete) => () => {
    unassignTag(tagToDelete.id, [nodeId])
      .then(response => {
        getNodeDetail(visualizationId, nodeId)
          .then(response => {
            setTags(response.data.tags);
          });
      })
  };

  return (
    <>
      {tags?.length > 0 && (
        <>
          <Typography variant="h5" style={{marginTop: 16}}>
            Tags
          </Typography>
          <ChipsList
            chipData={tags}
            handleDelete={handleTagDelete}
          />
        </>
      )}
    </>
  )
}