import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import { BaseNodeDetail } from "./base-node/BaseNodeDetail";
import { OverviewDetail } from "./overview/OverviewDetail";
import { NodeTagsList } from "./NodeTagsList";
import { getBaseNodes } from "../../../../cy-extensions/clustering/utils";
import Fullscreen from '@material-ui/icons/Fullscreen';
import FullscreenExit from '@material-ui/icons/FullscreenExit';

const useStyles = makeStyles(theme => ({
  root: {
    borderColor: theme.palette.secondary.light,
    borderWidth: 2,
    overflow: "auto",
    [theme.breakpoints.down('xs')]: {
      maxHeight: `calc(100vh - 48px - ${theme.spacing(2)}px)`,
    },
    [theme.breakpoints.up('sm')]: {
      maxHeight: `calc(100vh - 56px - ${theme.spacing(2)}px)`,
    },
    [theme.breakpoints.up('md')]: {
      maxHeight: `calc(100vh - 64px - ${theme.spacing(2)}px)`,
    },
  },
  fullscreen: {
    position: "fixed",
    top: theme.spacing(3),
    left: 0,
    right: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    zIndex: 1001,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  content: {
    maxHeight: "94%",
    overflow: "auto",
  },
  fullscreenContent: {
    maxHeight: "94%",
    overflow: "auto",
    minWidth: "460px",
    maxWidth: `calc(100vw - ${theme.spacing(2)}px)`
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  cardActions: {
    display: "flex",
    justifyContent: "space-between"
  },
  leftAlignButton: {
    alignSelf: "flex-start"
  },
  rightAlignButtons: {
    alignSelf: "flex-end",
    display: "flex",
    flexDirection: "row"
  }
}));

export function NodeDetail({ onClose, data }) {
  const classes = useStyles();
  const [baseNodeIds, setBaseNodeIds] = useState([]);
  const [fullscreen, setFullscreen] = useState(false);

  const { id, label, bg, width, height, layoutLocked, cluster, innerNodes, zIndex, ...granefData } = data;

  useEffect(() => {
    if (data.selection) {
      setBaseNodeIds(data.uids);
    } else if (data.cluster) {
      setBaseNodeIds(getBaseNodes(data.innerNodes)
        .map(baseNode => baseNode.data.id));
    } else {
      setBaseNodeIds([data.id]);
    }
  }, [data.cluster, data.innerNodes, data.id, data.selection, data.uids]);

  return (
    <div className={fullscreen ? classes.fullscreen : classes.root}>
    <Card className={classes.root} variant="outlined">
      <CardContent className={fullscreen ? classes.fullscreenContent : classes.content}>
        {data.cluster || data.selection
          ? <OverviewDetail baseNodeIds={baseNodeIds} topLevelIds={data.cluster ? [data.id] :  data.topLevelIds}/>
          : <BaseNodeDetail data={granefData}/>
        }
        {!data.selection && <NodeTagsList nodeId={id}/>}
      </CardContent>
      <CardActions className={classes.cardActions}>
        <Button size="small"
                className={classes.leftAlignButton}
                onClick={() => setFullscreen(!fullscreen)}>
                  {fullscreen ? <FullscreenExit /> : <Fullscreen />}
        </Button>
        <div className={classes.rightAlignButtons}>
          <Button size="small"
                  onClick={() => navigator.clipboard.writeText(baseNodeIds.join(','))}
          >
            Copy {data.cluster ? "UIDs" : "UID"}
          </Button>
          <Button size="small" onClick={onClose}>Close</Button>
        </div>
      </CardActions>
    </Card>
    </div>
  );
}
