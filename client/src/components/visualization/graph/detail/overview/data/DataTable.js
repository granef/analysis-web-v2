import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
} from "@material-ui/core";
import TablePaginationActions from "@material-ui/core/TablePagination/TablePaginationActions";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: { maxWidth: '100%', overflow: "auto" },
}));

export function DataTable({ tableHead, tableData }) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return tableData ? (
    <>
      <div className={classes.root}>
        <Table stickyHeader style={{ minWidth: 650 }} aria-label="List of visualizations">
          <TableHead>
            <TableRow>
              {tableHead.map((cell, index) => (<TableCell key={index}>{cell}</TableCell>))}
            </TableRow>
          </TableHead>
          <TableBody>
            {(rowsPerPage > 0
                ? tableData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                : tableData
            ).map((row, index) => (
              <TableRow key={index}>
                {tableHead.map((cell, index) => (
                  <TableCell component="th" scope="row" key={index}>{row[cell]}</TableCell>))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
      <div className={classes.root}>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 50, 100, { label: 'All', value: -1 }]}
          colSpan={tableHead.length}
          count={tableData.length}
          rowsPerPage={rowsPerPage}
          page={page}
          SelectProps={{
            inputProps: { 'aria-label': 'rows per page' }
          }}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          ActionsComponent={TablePaginationActions}
        />
      </div>
    </>
  ) : (<></>)
}