import React, { useEffect } from "react";
import { useTimeline } from "../../../../../hooks/use-timeline";
import { TimelineBar } from "../../timeline/TimelineBar";
import { Alert } from "@material-ui/lab";
import { Loader } from "../../../../common/Loader";

export function TimelineDetail({ baseNodeIds }) {
  const {
    timelineData,
    setUids,
    loading,
    error,
  } = useTimeline({
      interval: 5 * 60_000, // 5 minutes
      timeRange: {
        min: null,
        max: null,
      },
      scale: 'linear',
    },
    baseNodeIds,
    false
  );


  useEffect(() => {
    setUids(baseNodeIds);
  }, [baseNodeIds, setUids]);

  return (
    <>
      {loading && <Loader width="100%" height={380}/>}
      {!loading && error && (
        <Alert severity="error" variant="outlined">{error}</Alert>
      )}
      {!loading && !error && timelineData &&
        <TimelineBar
          data={timelineData}
          selectedIndices={[...Array(timelineData.length).keys()]}
          valueScale='linear'
        />
      }
    </>
  )
}