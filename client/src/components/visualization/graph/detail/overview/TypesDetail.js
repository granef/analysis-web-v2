import React, { useEffect, useState } from "react";
import { Box } from "@material-ui/core";
import { ResponsivePie } from "@nivo/pie";
import { getColorForType } from "../../../../../parsers/parse-utils";
import { Loader } from "../../../../common/Loader";

export function TypesDetail({ stats }) {
  const [typeStatistics, setTypeStatistics] = useState(null);

  useEffect(() => {
    setTypeStatistics(stats?.node?.type);
  }, [stats?.node?.type]);

  return typeStatistics ? (
    <Box display="flex" width="100%" height={380} bgcolor={'#'}>
      <ResponsivePie
        data={Object.entries(typeStatistics).map(([key, val]) => ({
          id: key,
          label: key,
          value: val
        }))}
        width={400}
        colors={Object.keys(typeStatistics).map(getColorForType)}
        margin={{ top: 40, right: 90, bottom: 90, left: 90 }}
        innerRadius={0.4}
        padAngle={0.7}
        cornerRadius={3}
        activeOuterRadiusOffset={5}
        sortByValue={true}
        borderWidth={1}
        borderColor={{ from: 'color', modifiers: [['darker', 0.2]] }}
        arcLinkLabelsDiagonalLength={15}
        arcLinkLabelsStraightLength={10}
        arcLinkLabelsSkipAngle={10}
        arcLinkLabelsThickness={2}
        arcLinkLabelsColor={{ from: 'color' }}
        arcLabelsSkipAngle={10}
        arcLabelsTextColor={{ from: 'color', modifiers: [['darker', 2]] }}
        legends={[
          {
            anchor: 'bottom',
            direction: 'row',
            justify: false,
            translateX: 0,
            translateY: 56,
            itemsSpacing: 0,
            itemWidth: 100,
            itemHeight: 18,
            itemTextColor: '#999',
            itemDirection: 'left-to-right',
            itemOpacity: 1,
            symbolSize: 18,
            symbolShape: 'circle',
            effects: [
              {
                on: 'hover',
                style: {
                  itemTextColor: '#000'
                }
              }
            ]
          }
        ]}
      />
    </Box>
  ) :  (
    <Loader width="100%" height={380}/>
  );
}