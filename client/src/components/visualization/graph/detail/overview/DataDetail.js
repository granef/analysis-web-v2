import React, { useContext, useEffect, useState } from "react";
import { Typography } from "@material-ui/core";
import { CyContext } from "../../../../../contexts/cy-context";
import _ from "lodash";
import { NodeTypeLabels } from "../../../../../parsers/node-type";
import { DataTable } from "./data/DataTable";
import { Loader } from "../../../../common/Loader";

export function DataDetail({ topLevelIds }) {
  const [cy] = useContext(CyContext);
  const [elements, setElements] = useState(null);
  const [tableData, setTableData] = useState(null);

  useEffect(() => {
    if (cy) {
      const newElements = topLevelIds === 1 ? cy.getElementById(topLevelIds[0]) : cy.nodes().filter(node => topLevelIds.includes(node.id()));
      setElements(newElements);
    } else {
      setElements(null);
    }
  }, [cy, topLevelIds]);

  useEffect(() => {
    if (!elements) {
      return;
    }
    const newGroupedData = _.chain(elements.getGranefData())
      .groupBy("dgraph.type")
      .map((value, key) => ({ type: key, values: value })).value();

    const newTableData = {};
    Object.entries(newGroupedData)
      .forEach(([, { type, values }], index) => {
        newTableData[type] = {};
        newTableData[type].tableHead = values.reduce((acc, value) => {
          const valueKeys = Object.keys(value);
          return acc.concat(valueKeys.filter(valueKey => valueKey !== "dgraph.type" && !acc.includes(valueKey)));
        }, []);
        newTableData[type].tableData = values;
      });
    setTableData(newTableData);
  }, [elements]);

  return tableData ? (
    <>
      {Object.entries(tableData).map(([key, { tableHead, tableData }], index) => (<div key={key}>
        <Typography variant="h6" style={{ marginTop: 16 }}>{NodeTypeLabels[key]}</Typography>
        <DataTable key={key} tableHead={tableHead} tableData={tableData}/>
      </div>))}
    </>
  ) : (
    <Loader width="100%" height={380}/>
  );
}