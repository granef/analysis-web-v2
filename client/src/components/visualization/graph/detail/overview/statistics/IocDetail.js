import Typography from "@material-ui/core/Typography";
import { TypeCounts } from "./TypeCounts";

/**
 * @param data {{ type: {[key]: number} }}
 */
export function IocDetail({ data }) {
  const type = data?.type;
  return (<>
    <Typography variant="h6">Ioc</Typography>
    <TypeCounts data={type} title="Type"/>
    <br/>
  </>);
}