import Typography from "@material-ui/core/Typography";

/**
 * @param data { invalid: number }
 */
export function SslDetail({ data }) {
  const invalid = data?.invalid;
  return (<>
    <Typography variant="h6">SSL</Typography>
    Invalid: {invalid}<br/>
  </>);
}