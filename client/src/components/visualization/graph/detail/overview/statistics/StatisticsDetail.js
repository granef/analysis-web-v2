import { IocDetail } from "./IocDetail";
import { DnsDetail } from "./DnsDetail";
import { HttpDetail } from "./HttpDetail";
import { FileDetail } from "./FileDetail";
import { FilesDetail } from "./FilesDetail";
import { SslDetail } from "./SslDetail";
import { ConnectionDetail } from "./ConnectionDetail";
import { Loader } from "../../../../../common/Loader";
import React from "react";

export function StatisticsDetail({ data }) {

  const typeIsPresent = (type) => data.node.type[type] && data.node.type[type] > 0;

  return data ? (
    <>
      {typeIsPresent('Ioc') && <IocDetail data={data?.ioc}/>}
      {typeIsPresent('Dns') && <DnsDetail data={data?.dns}/>}
      {typeIsPresent('Http') && <HttpDetail data={data?.http}/>}
      {typeIsPresent('File') && <FileDetail data={data?.file}/>}
      {typeIsPresent('Files') && <FilesDetail data={data?.files}/>}
      {typeIsPresent('Ssl') && <SslDetail data={data?.ssl}/>}
      {typeIsPresent('Connection') && <ConnectionDetail data={data?.connection}/>}
    </>
  ) : (
    <Loader width="100%" height={380}/>
  );
}