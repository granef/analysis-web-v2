import React from "react";
import Typography from "@material-ui/core/Typography";
import { MinMaxAvgBar } from "./MinMaxAvgBar";
import { TypeCounts } from "./TypeCounts";

/**
 * @param data {{ bytes: { max: number, min: number, avg: number }, mime_type: {[key]: number} }}
 */
export function FileDetail({ data }) {
  const mimeType = data?.mime_type;
  const bytes = data?.bytes;

  return (<>
    <Typography variant="h6">File</Typography>
    <MinMaxAvgBar data={[bytes]} title='Bytes'/>
    <TypeCounts title="Mime Type" data={mimeType}/>
  </>);
}