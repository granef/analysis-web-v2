import Typography from "@material-ui/core/Typography";
import { TypeCounts } from "./TypeCounts";
import React from "react";

/**
 * @param data {{ qtype:  {[key]: number} }}
 */
export function DnsDetail({ data }) {
  const type = data?.dns?.qtype;

  return (<>
    <Typography variant="h6">DNS</Typography>
    <TypeCounts title="Type of the query" data={type}/>
  </>);
}