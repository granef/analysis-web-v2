import React, { useEffect, useState } from "react";
import { Bar } from "@nivo/bar";
import Typography from "@material-ui/core/Typography";
import { isNil } from "lodash";
import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from "@material-ui/core";

export function MinMaxAvgBar({ title, data }) {
  const [barData, setBarData] = useState({ keys: [], data: [] });
  const [valueScale, setValueScale] = useState('symlog');

  useEffect(() => {
    const newBarData = data.map(datum => ({
      stat: datum.title,
      Minimum: datum?.min ? Math.round(datum?.min) : null,
      Average: datum?.avg ? Math.round(datum?.avg) : null,
      Maximum: datum?.max ? Math.round(datum?.max) : null
    })).filter(datum => !isNil(datum.Minimum) || !isNil(datum.Average) || !isNil(datum.Maximum));

    if (newBarData) {
      setBarData({
        keys: ["Minimum", "Average", "Maximum"],
        data: newBarData
      })
    } else {
      setBarData({ keys: [], data: [] })
    }
  }, [title, data]);

  return barData.data.length === 0 ? <></> : (<>
    <Typography variant="subtitle1">{title}</Typography>
    <Bar
      data={barData.data}
      keys={barData.keys}
      indexBy="stat"
      width={400}
      height={350}
      groupMode="grouped"
      margin={{ top: 10, right: 115, bottom: 85, left: 95 }}
      valueFormat=" >-"
      colors={["#BEAEED", "#A894E7", "#937BE2"]}
      valueScale={{type: valueScale}}
      borderColor={{ from: 'color', modifiers: [['darker', 3]] }}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        tickSize: 5,
        tickPadding: 20,
        tickRotation: 30,
      }}
      axisLeft={{
        tickSize: 5,
        tickPadding: 5,
        tickRotation: 0,
        legend: 'Count',
        legendPosition: 'middle',
        legendOffset: -70
      }}
      labelSkipWidth={12}
      labelSkipHeight={12}
      labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
      role="application"
      ariaLabel="Min/Max/Avg Statistics"
      barAriaLabel={function (e) {
        return e.stat + ": " + e.formattedValue + " at " + e.indexValue
      }}
    />
    <FormControl component="fieldset">
      <FormLabel component="legend">Scale</FormLabel>
      <RadioGroup aria-label="scale" name="scale1" value={valueScale} onChange={e => setValueScale(e.target.value)} row>
        <FormControlLabel value="linear" control={<Radio />} label="Linear" />
        <FormControlLabel value="symlog" control={<Radio />} label="Logarithmic" />
      </RadioGroup>
    </FormControl>
  </>);
}