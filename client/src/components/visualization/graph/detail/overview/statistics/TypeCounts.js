import React from "react";
import { isNil } from "lodash";
import { Pie } from "@nivo/pie";
import { Typography } from "@material-ui/core";

/**
 * Represents counts in categories with a pie chart.
 */
export function TypeCounts({ title, data, ...props }) {
  return Object.values(data).some(value => !isNil(value)) ? (<>
    <Typography variant="subtitle1">{title}</Typography>
    <Pie
      data={Object.entries(data).map(([key, val]) => ({
        id: key,
        label: key,
        value: val
      }))}
      width={400}
      height={350}
      margin={{ top: 40, right: 90, bottom: 90, left: 90 }}
      innerRadius={0.4}
      padAngle={0.7}
      cornerRadius={3}
      activeOuterRadiusOffset={5}
      sortByValue={true}
      borderWidth={1}
      borderColor={{ from: 'color', modifiers: [['darker', 0.2]] }}
      arcLinkLabelsDiagonalLength={15}
      arcLinkLabelsStraightLength={10}
      arcLinkLabelsSkipAngle={10}
      arcLinkLabelsThickness={2}
      arcLinkLabelsColor={{ from: 'color' }}
      arcLabelsSkipAngle={10}
      arcLabelsTextColor={{ from: 'color', modifiers: [['darker', 2]] }}
      legends={[
        {
          anchor: 'bottom',
          direction: 'row',
          justify: false,
          translateX: 0,
          translateY: 56,
          itemsSpacing: 0,
          itemWidth: 100,
          itemHeight: 18,
          itemTextColor: '#999',
          itemDirection: 'left-to-right',
          itemOpacity: 1,
          symbolSize: 18,
          symbolShape: 'circle',
          effects: [
            {
              on: 'hover',
              style: {
                itemTextColor: '#000'
              }
            }
          ]
        }
      ]}
      {...props}
    />
  </>) : <></>;
}