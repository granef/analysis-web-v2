import Typography from "@material-ui/core/Typography";

export function MinMaxAvgText({ title, data }) {
  const min = data?.min;
  const max = data?.max;
  const avg = data?.avg;

  return min || max || avg ? (<>
    <Typography variant="subtitle1">{title}</Typography>
    {min && (<>
      <Typography variant="subtitle2" display="inline">Minimum: </Typography>{min}<br/>
    </>)}
    {avg && (<>
      <Typography variant="subtitle2" display="inline">Average: </Typography>{avg}<br/>
    </>)}
    {max && (<>
      <Typography variant="subtitle2" display="inline">Maximum: </Typography>{max}<br/>
    </>)}
  </>) : <></>
}