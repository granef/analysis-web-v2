import Typography from "@material-ui/core/Typography";
import { TypeCounts } from "./TypeCounts";

/**
 * Returns a color representing given HTTP response code in the color schema. The colors are inspired by the usual
 * depiction of these codes (e.g. green for success, blue for information, etc.)
 *
 * @param codeStr {string}
 */
const getStatusCodeColor = (codeStr) => {
  const code = Number(codeStr);
  if (100 <= code && code < 200) {
    // 1xx informational response
    return "#8cb9ce";
  } else if (200 <= code && code < 300 ) {
    // 2xx successful
    return "#A8C287";
  } else if (300 <= code && code < 400) {
    // 3xx redirection
    return "#996FB1"
  } else if (400 <= code && code < 500) {
    // 4xx client error
    return "#F28918";
  } else if (500 <= code && code < 600) {
    // 5xx server error
    return "#E44B4E";
  }
  return "#ff0000";
}

/**
 * Returns a color representing given HTTP method in the color schema. The colors are inspired by the ones used by Swagger.
 *
 * @param method {string}
 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
 */
const getMethodColor = (method) => {
  switch (method) {
    case "GET":
      return "#61affe";
    case "HEAD":
      return "#9012fe"
    case "POST":
      return "#49cc90";
    case "PATCH":
      return "#50e3c2";
    case "PUT":
      return "#fca130"
    case "DELETE":
      return "#f93e3e"
    case "CONNECT":
      return "#ff3fe1"
    case "OPTIONS":
      return "#0d5aa7"
    case "TRACE":
      return "#ffd23f"
    default:
      return "#696969";
  }
}
/**
 * @param data { {method: {[key]: number}, status: {[key]: number}} }
 */
export function HttpDetail({ data }) {
  const status = data?.status;
  const method = data?.method;
  return (<>
    <Typography variant="h6">HTTP</Typography>
    {method && <TypeCounts title="Method"
                           data={method}
                           colors={Object.keys(method).map(getMethodColor)}/>}
    {status && <TypeCounts title="Status"
                           data={status}
                           colors={Object.keys(status).map(getStatusCodeColor)}/>}
  </>);
}