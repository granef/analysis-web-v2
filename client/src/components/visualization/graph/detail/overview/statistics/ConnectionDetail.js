import React from "react";
import Typography from "@material-ui/core/Typography";
import { MinMaxAvgText } from "./MinMaxAvgText";
import { MinMaxAvgBar } from "./MinMaxAvgBar";
import { TypeCounts } from "./TypeCounts";
import { formatUtcDate } from "../../../../../../context-menu/utils";

/**
 * Returns color assigned the given state in the color scheme. The scheme was generated with the help of a tool at
 * {@link http://vrl.cs.brown.edu/color}. List of the possible state values was obtained from
 * {@link https://docs.zeek.org/en/current/scripts/base/protocols/conn/main.zeek.html#type-Conn::Info.}
 * 
 * @param state {string}
 */
const getStateColor = (state) => {
  const scheme = [
    "#383888", "#ea6531", "#ea92a3", "#bad8f0",
    "#dbc767", "#789d23", "#2d747a", "#828672",
    "#4ab9ce", "#683c00", "#8e0049", "#d592eb",
    "#8c46d0"
  ];
  const unknown = "#696969";
  const states = ["S0", "S1", "SF", "REJ", "S2", "S3", "RSTO", "RSTR", "RSTOS0", "RSTRH", "SH", "SHR", "OTH"];

  const index = states.indexOf(state);
  return index === -1 ? unknown : scheme[index];
}
/**
 * @param data {{
 *   ts: { max: string, min: string },
 *   orig_bytes: { max: number, min: number, avg: number },
 *   resp_bytes: { max: number, min: number, avg: number },
 *   state: {[key]: number},
 *   proto: {[key]: number}
 * }}
 */
export function ConnectionDetail({ data }) {
  const { ts, "orig_bytes": originBytes, "resp_bytes": respBytes, state, proto } = data;

  return (<>
    <Typography variant="h6">Connection</Typography>
    <MinMaxAvgText data={{
      min: formatUtcDate(ts.min),
      max: formatUtcDate(ts.max),
    }} title='Timestamp'/>
    <MinMaxAvgBar title="Payload statistics" data={[
      { title: 'Request bytes', ...originBytes },
      { title: 'Response bytes', ...respBytes },
    ]}/>
    <TypeCounts title="State"
                data={state}
                colors={Object.keys(state).map(getStateColor)}/>
    <TypeCounts title="Protocol" data={proto}/>
  </>);
}