import Typography from "@material-ui/core/Typography";
import { MinMaxAvgBar } from "./MinMaxAvgBar";

/**
 * @param data {{
 *   total_bytes: { max: number, min: number, avg: number },
 *   missing_bytes: { max: number, min: number, avg: number },
 *   overflow_bytes: { max: number, min: number, avg: number }
 * }}
 */
export function FilesDetail({ data }) {
  const totalBytes = data?.total_bytes;
  const missingBytes = data?.missing_bytes;
  const overflowBytes = data?.overflow_bytes;

  return (<>
    <Typography variant="h6">Files</Typography>
    <MinMaxAvgBar title='Bytes' data={[
      { title: 'Total bytes', ...totalBytes },
      { title: 'Missing bytes', ...missingBytes },
      { title: 'Overflow bytes', ...overflowBytes },
    ]}/>
  </>);
}