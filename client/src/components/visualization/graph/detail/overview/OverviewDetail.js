import React, { useEffect, useState } from "react";
import { Button } from "@material-ui/core";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { TimelineDetail } from "./TimelineDetail";
import { TypesDetail } from "./TypesDetail";
import { FlowDetail } from "./FlowDetail";
import { StatisticsDetail } from "./statistics/StatisticsDetail";
import { getStatistics } from "../../../../../api/analysis-api";
import { isEmpty } from "lodash";
import { DataDetail } from "./DataDetail";
import { makeStyles } from "@material-ui/core/styles";

const ViewButton = ({ active, ...props }) => (
  <Button {...props} color={active ? "primary" : "secondary"}/>
)

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: '100%',
    overflow: "auto"
  },
}));

/**
 * This detail serves as an overview of base nodes with ids in given array, whether they are in a selection or contained
 * in a possibly single cluster.
 *
 * @param baseNodeIds Granef UIDs
 * @param topLevelIds IDs of nodes that are actually in the visualization (might be base nodes with Granef UIDs or cluster nodes)
 */
export function OverviewDetail({ baseNodeIds, topLevelIds }) {
  const classes = useStyles();
  const [view, setView] = useState('types');
  const [stats, setStats] = useState();

  useEffect(() => {
    if (!isEmpty(baseNodeIds)) {
      getStatistics(baseNodeIds.join(','))
        .then(response => {
          setStats(response.data.response);
        });
    }
  }, [baseNodeIds]);

  return (
    <>
      <ButtonGroup size="small" variant="text" color="secondary" disableElevation fullWidth>
        <ViewButton active={view === 'types'} onClick={() => setView('types')}>
          Types
        </ViewButton>
        <ViewButton active={view === 'statistics'} onClick={() => setView('statistics')}>
          Statistics
        </ViewButton>
        <ViewButton active={view === 'data'} onClick={() => setView('data')}>
          Data
        </ViewButton>
        {stats?.node?.type?.Connection && stats?.node?.type?.Connection > 0 && (
          <ViewButton active={view === 'timeline'} onClick={() => setView('timeline')}>
            Timeline
          </ViewButton>
        )}
        {stats?.node?.type?.Host && stats?.node?.type?.Host > 1 && (
          <ViewButton active={view === 'flow'} onClick={() => setView('flow')}>
            Flow
          </ViewButton>
        )}
      </ButtonGroup>

      {view === 'types' && (
        <TypesDetail stats={stats}/>
      )}
      {view === 'statistics' && (
        <StatisticsDetail data={stats}/>
      )}
      {view === 'data' && (
        <div className={classes.root}>
          <DataDetail topLevelIds={topLevelIds}/>
        </div>
      )}
      {view === 'timeline' && baseNodeIds && (
        <div className={classes.root}>
          <TimelineDetail baseNodeIds={baseNodeIds}/>
        </div>
      )}
      {view === 'flow' && (
        <FlowDetail baseNodeIds={baseNodeIds}/>
      )}
    </>
  )
}