import React, { useEffect, useState } from "react";
import { Box } from "@material-ui/core";
import { ResponsiveChord } from "@nivo/chord";
import { getAdjacencyMatrix, getNodeAttributes } from "../../../../../api/analysis-api";
import { TableTooltip, BasicTooltip, Chip } from '@nivo/tooltip'
import { Loader } from "../../../../common/Loader";

const ArcTooltip = ({ arc }) => (
    <BasicTooltip
        id={`${arc.label} originated connections`}
        value={arc.formattedValue}
        color={arc.color}
        enableChip={true}
    />
)
const RibbonTooltip = ({ ribbon }) => (
  <TableTooltip
    rows={[
      ['', 'Connections:'],
      [
        <Chip key="chip" color={ribbon.source.color}/>,
        <>From <strong key="id">{ribbon.source.id}</strong></>,
        ribbon.source.value,
      ],
      [
        <Chip key="chip" color={ribbon.target.color}/>,
        <>From <strong key="id">{ribbon.target.id}</strong></>,
        ribbon.target.value,
      ],
    ]}
  />
)
export function FlowDetail({ baseNodeIds }) {
  const [chordData, setChordData] = useState({
    keys: [],
    data: []
  });

  useEffect(() => {
    if (baseNodeIds) {
      // eliminate hosts with no connections originated and responded (e.g. if the x-th row is all 0s and also x-th column is all 0s)
      getAdjacencyMatrix(baseNodeIds.join(','))
        .then(response => {
          const { uids, connections } = response.data.response;
          let emptyRows = connections.reduce((acc, row, index) => {
            if (row.every(value => value === 0)) {
              acc.push(index);
            }
            return acc;
          }, []).filter(index => connections.every(row => row[index] === 0));

          const hostUids = uids.filter((uid, index) => !emptyRows.includes(index));
          const data = connections
            .filter((row, index) => !emptyRows.includes(index))
            .map((row) => row.filter((item, index) => !emptyRows.includes(index)));

          getNodeAttributes(hostUids.join(','))
            .then(response => {
              const keys = response.data.response.map(node => node['host.ip']);
              setChordData({ keys, data });
            });
        }).catch(console.error);
    } else {
      setChordData({
        keys: [],
        data: []
      });
    }
  }, [baseNodeIds]);

  return chordData && chordData.keys?.length > 0 && chordData.data?.length > 0 ? (
    <Box display="flex" width="100%" height={425} flexDirection="column">
      <ResponsiveChord
        data={chordData.data}
        keys={chordData.keys}
        margin={{ top: 30, right: 100, bottom: 30, left: 100 }}
        padAngle={0.02}
        innerRadiusRatio={0.96}
        innerRadiusOffset={0.02}
        inactiveArcOpacity={0.25}
        arcBorderColor={{
          from: 'color',
          modifiers: [
            [
              'darker',
              0.6
            ]
          ]
        }}
        activeRibbonOpacity={0.75}
        inactiveRibbonOpacity={0.25}
        ribbonBorderColor={{
          from: 'color',
          modifiers: [
            [
              'darker',
              0.6
            ]
          ]
        }}
        labelRotation={-90}
        labelTextColor={{
          from: 'color',
          modifiers: [
            [
              'darker',
              1
            ]
          ]
        }}
        colors={{ scheme: 'nivo' }}
        motionConfig="stiff"
        arcTooltip={ArcTooltip}
        ribbonTooltip={RibbonTooltip}
      />
    </Box>
  ) : (
    <Loader width="100%" height={380}/>
  );
}