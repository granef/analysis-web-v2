import Typography from "@material-ui/core/Typography";
import React from "react";

const booleanToString = (bool) => bool ? "true" : "false";

const getDataEntryComponents = (dataEntry) => {
  const { key, value, index } = dataEntry;
  if (value.constructor === Array) {
    return (
      <Typography variant="body1" key={index}>
        {key}: <strong>{value.join(', ')}</strong>
      </Typography>
    );
  } else if (typeof value === "boolean") {
    return (
      <Typography variant="body1" key={index}>
        {key}: <strong>{booleanToString(value)}</strong>
      </Typography>
    )
  } else {
    return (
      <Typography variant="body1" key={index}>
        {key}: <strong>{value}</strong>
      </Typography>
    );
  }
}

export function BaseNodeDetail({ data }) {
  return (
    <>
      <Typography variant="h5">
        Data
      </Typography>
      {Object.entries(data).map(([key, value], index) => getDataEntryComponents({key, value, index}))}
    </>
  )
}