import { findVisualization, updateVisualization } from "../../../api/visualizations-api";
import { VisualizationDetailsForm } from "./VisualizationDetailsForm";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Alert } from "@material-ui/lab";
import Snackbar from "@material-ui/core/Snackbar";
import Typography from "@material-ui/core/Typography";
import { BodyWrapper } from "../../common/BodyWrapper";
import { Paper } from "@material-ui/core";
import { Loader } from "../../common/Loader";

export function EditVisualization() {
  const { id } = useParams();
  const [data, setData] = useState();
  const [alert, setAlert] = useState();

  useEffect(() => {
    if (id) {
      findVisualization(id)
        .then(response => {
          setData(response.data);
        })
        .catch(error => {
          setAlert({
            severity: 'error',
            message: error
          })
        });
    }
  }, [id]);

  const update = (visualization) => {
    updateVisualization(visualization)
      .then(response => {
        setData(response.data);
        setAlert({
          severity: 'success',
          message: 'Visualization details were updated.'
        })
      })
      .catch(error => {
        setAlert({
          severity: 'error',
          message: error
        })
      });
  }

  return data ? (
    <BodyWrapper>
      <div style={{
        maxWidth: 750,
        margin: "0 auto",
      }}>
        <Paper square style={{
          padding: '16px'
        }}>
          <Typography variant="h5" align="center">Edit visualization</Typography>
          <VisualizationDetailsForm onSubmit={update} defaultData={data}/>
          <Snackbar
            anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
            autoHideDuration={5000}
            open={alert !== undefined}
            // children={}
            onClose={() => setAlert(undefined)}
            key="error-alert"
          ><Alert severity={alert?.severity} onClose={() => setAlert(undefined)}>{alert?.message}</Alert></Snackbar>
        </Paper>
      </div>
    </BodyWrapper>
  ) : (
    <Loader width="100%" height="450px"/>
  );
}