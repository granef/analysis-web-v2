import { createVisualization } from "../../../api/visualizations-api";
import { VisualizationDetailsForm } from "./VisualizationDetailsForm";
import { useHistory } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { BodyWrapper } from "../../common/BodyWrapper";
import { Paper } from "@material-ui/core";

export function NewVisualization() {
  const history = useHistory();

  const create = (visualization) => {
    createVisualization(visualization)
      .then(response => {
        history.push(`/visualizations/${response.data.id}`);
      })
      .catch(console.error);
  }

  return (
    <BodyWrapper>
      <div style={{
        maxWidth: 750,
        margin: "0 auto",
      }}>
        <Paper square style={{
          padding: '16px'
        }}>
          <Typography variant="h5" align="center">New visualization</Typography>
          <VisualizationDetailsForm onSubmit={create}/>
        </Paper>
      </div>
    </BodyWrapper>
  );
}