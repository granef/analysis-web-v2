import React, { useState } from "react";
import { Button, Grid } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField/TextField";
import { makeStyles } from "@material-ui/core/styles";
import { toDatetimeLocal } from "../../../utils/date-utils";
import { useHistory } from "react-router-dom";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 650,
    margin: "0 auto",
  },
  formControl: {
    margin: theme.spacing(1, 0),
  },
  rightAlignItems: {
    marginLeft: "auto"
  },
}));

export function VisualizationDetailsForm({ onSubmit, defaultData }) {
  const classes = useStyles();
  const [data, setData] = useState(defaultData);
  const history = useHistory();

  const handleChange = (prop) => (event) => {
    setData(prevData => ({ ...prevData, [prop]: event.target.value }));
  };

  return (
    <Grid container classes={{
      root: classes.root
    }}>
      <Grid item xs={12}>
        <FormControl className={classes.formControl} fullWidth>
          <TextField
            id="name-field"
            label="Name"
            variant="outlined"
            onChange={handleChange('name')}
            defaultValue={data?.name}
            required
          />
        </FormControl>
      </Grid>
      {data?.created && (
        <Grid item xs={12}>
          <TextField
            style={{ marginTop: 8, marginBottom: 8 }}
            variant="outlined"
            id="created"
            label="Created"
            type="datetime-local"
            value={toDatetimeLocal(moment(data.created).toISOString())}
            InputLabelProps={{
              shrink: true,
            }}
            disabled
            fullWidth
          />
        </Grid>
      )}
      {data?.updated && (
        <Grid item xs={12}>
          <TextField
            style={{ marginTop: 8, marginBottom: 8 }}
            variant="outlined"
            id="created"
            label="Updated"
            type="datetime-local"
            value={toDatetimeLocal(moment(data.updated).toISOString())}
            InputLabelProps={{
              shrink: true,
            }}
            disabled
            fullWidth
          />
        </Grid>
      )}
      <Grid item xs={12}>
        <FormControl className={classes.formControl} fullWidth>
          <TextField
            id="description-field"
            label="Description"
            variant="outlined"
            onChange={handleChange('description')}
            defaultValue={data?.description}
            multiline
          />
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormControl className={classes.formControl} fullWidth>
          <TextField
            id="note-field"
            label="Note"
            variant="outlined"
            onChange={handleChange('note')}
            defaultValue={data?.note}
            multiline
          />
        </FormControl>
      </Grid>
      <Grid container item xs={12} alignItems="flex-end">
        <Button onClick={() => history.goBack()}>
          Cancel
        </Button>
        <Button onClick={() => onSubmit(data)}>
          Save
        </Button>
      </Grid>
    </Grid>
  );
}