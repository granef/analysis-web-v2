import React, { useEffect, useState } from "react";
import { createVisualization, deleteVisualization, findAllVisualizations, getVisualizationState, getVisualizationPreferences, updateVisualizationPreferences, updateVisualizationState } from "../../../api/visualizations-api";
import { IconButton, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LaunchIcon from '@material-ui/icons/Launch';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import { Link } from "react-router-dom";
import { DeleteOutline } from "@material-ui/icons";
import AddIcon from "@material-ui/icons/Add";
import Fab from "@material-ui/core/Fab";
import EditIcon from '@material-ui/icons/Edit';
import { BodyWrapper } from "../../common/BodyWrapper";
import moment from "moment";
import { toDatetimeLocal } from "../../../utils/date-utils";

const useStyles = makeStyles(theme => ({
  table: {
    minWidth: 650,
  },
  root: {
    margin: theme.spacing(4, 2, 4, 2),
  },
  newVisualizationFab: {
    margin: theme.spacing(1),
  }
}));

export function VisualizationsManager() {
  const classes = useStyles();
  const [visualizations, setVisualizations] = useState();

  useEffect(() => {
    findAllVisualizations()
      .then(response => {
        setVisualizations(response.data)
      })
      .catch(console.error);
  }, []);

  const deleteAndRefreshList = (id) => {
    deleteVisualization(id)
      .then(() => {
        findAllVisualizations()
          .then(response => {
            setVisualizations(response.data);
          })
          .catch(console.error);
      })
      .catch(console.error);
  }

  const copyAndRefreshList = (visualization) => {
    const visualizationId = visualization.id;
    delete visualization.id; 
    visualization.name = visualization.name + ' copy';
    visualization.created = toDatetimeLocal(moment().toISOString());
    visualization.updated = toDatetimeLocal(moment().toISOString());
    createVisualization(visualization)
      .then(response => {
        const copyId = response.data.id;
        getVisualizationState(visualizationId)
          .then(response => {
            updateVisualizationState(copyId, response.data);
          })
          .catch(console.error);
        getVisualizationPreferences(visualizationId)
          .then(response => {
            updateVisualizationPreferences(copyId, response.data);
          })
          .catch(console.error);
      })
      .then(() => {
        findAllVisualizations()
          .then(response => {
            setVisualizations(response.data);
          })
          .catch(console.error);
      })
      .catch(console.error);
  }

  return (
    <BodyWrapper>
      <TableContainer component={Paper} elevation={0}>
        <Table stickyHeader className={classes.table} aria-label="List of visualizations">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Created</TableCell>
              <TableCell>Updated</TableCell>
              <TableCell align="center">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {visualizations && visualizations.map((visualization) => (
              <TableRow key={visualization.id}>
                <TableCell component="th" scope="row">
                  {visualization.name}
                </TableCell>
                <TableCell>{visualization.description}</TableCell>
                <TableCell>{moment(visualization.created).format('MM/DD/YYYY HH:mm')}</TableCell>
                <TableCell>{moment(visualization.updated).format('MM/DD/YYYY HH:mm')}</TableCell>
                <TableCell align="center">
                  <Link to={`/visualizations/${visualization.id}`}>
                    <IconButton color="primary" size="small">
                      <LaunchIcon/>
                    </IconButton>
                  </Link>
                  <Link to={`/visualizations/${visualization.id}/details`}>
                    <IconButton color="primary" size="small">
                      <EditIcon/>
                    </IconButton>
                  </Link>
                  <IconButton color="primary" size="small" onClick={() => copyAndRefreshList(visualization)}>
                    <FileCopyIcon/>
                  </IconButton>
                  <IconButton color="primary" size="small" onClick={() => deleteAndRefreshList(visualization.id)}>
                    <DeleteOutline/>
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <Link to={`/visualizations/new`}>
        <Fab size="small"
             color="secondary"
             aria-label="add"
             classes={{
               root: classes.newVisualizationFab
             }}>
          <AddIcon/>
        </Fab>
      </Link>

    </BodyWrapper>
  )
}