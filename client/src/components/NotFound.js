import React from "react";
import { Typography } from "@material-ui/core";

export function Notfound() {
  return (
    <Typography variant="h6" gutterBottom>
      Not found.
    </Typography>
  );
}