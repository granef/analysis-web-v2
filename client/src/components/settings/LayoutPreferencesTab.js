import React, { useEffect, useState } from "react";
import { LayoutForm } from "../visualization/graph/side-panel/preferences/layout/LayoutForm";
import {
  getDefaultVisualizationPreferences,
  updateDefaultVisualizationPreferences
} from "../../api/visualizations-api";

export function LayoutPreferencesTab() {
  const [layout, setLayout] = useState();

  useEffect(() => {
    getDefaultVisualizationPreferences()
      .then(response => response.data.layout)
      .then(setLayout)
      .catch(console.error);
  }, []);

  const updateLayoutOptions = (patch) => {
    const newLayoutOptions = {
      ...layout,
      ...patch
    }
    const preferencesPatch = {
      layout: newLayoutOptions,
    };
    updateDefaultVisualizationPreferences(preferencesPatch)
      .then(response => {
        setLayout(response.data.layout);
      });
  }

  return <LayoutForm layout={layout} updateOption={updateLayoutOptions}/>
}