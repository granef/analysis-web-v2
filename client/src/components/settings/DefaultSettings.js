import React, { useState } from "react";
import { Paper, Tabs, Tab } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { BodyWrapper } from "../common/BodyWrapper";
import { LayoutPreferencesTab } from "./LayoutPreferencesTab";
import {
  getDefaultColors, createColor, deleteColor,
  getDefaultTags, createTag, deleteTag,
} from "../../api/annotations-api";
import ColoredAnnotationOverview from "../visualization/graph/side-panel/annotations/ColoredAnnotationOverview";
import { StylePreferencesTab } from "./StylePreferencesTab";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`wrapped-tabpanel-${index}`}
      aria-labelledby={`wrapped-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}


export function DefaultSettings() {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <BodyWrapper>
      <div style={{
        maxWidth: 750,
        margin: "0 auto",
      }}>
        <Paper square>
          <Tabs
            value={value}
            indicatorColor="primary"
            textColor="primary"
            onChange={handleChange}
            aria-label="disabled tabs example"
            centered
          >
            <Tab label="Tags"/>
            <Tab label="Colors"/>
            <Tab label="Layout"/>
            <Tab label="Style"/>
          </Tabs>
          <TabPanel value={value} index={0}>
            <ColoredAnnotationOverview
              title="Tags"
              newTitle="New tag"
              listApi={getDefaultTags}
              createApi={createTag}
              deleteApi={deleteTag}
            />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <ColoredAnnotationOverview
              title="Colors"
              newTitle="New color"
              listApi={getDefaultColors}
              createApi={createColor}
              deleteApi={deleteColor}
            />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <LayoutPreferencesTab/>
          </TabPanel>
          <TabPanel value={value} index={3}>
            <StylePreferencesTab/>
          </TabPanel>
        </Paper>
      </div>
    </BodyWrapper>
  )
}