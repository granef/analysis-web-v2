import React, { useEffect, useState } from "react";
import { StyleForm } from "../visualization/graph/side-panel/preferences/style/StyleForm";
import {
  getDefaultVisualizationPreferences,
  updateDefaultVisualizationPreferences
} from "../../api/visualizations-api";

export function StylePreferencesTab()  {
  const [style, setStyle] = useState(null);

  useEffect(() => {
    getDefaultVisualizationPreferences()
      .then(response => response.data.style)
      .then(setStyle)
      .catch(console.error);
  }, []);

  const updateOption = (patch) => {
    const newStyleOptions = {
      ...style,
      [patch.attribute]: patch.value
    }
    const preferencesPatch = {
      style: newStyleOptions,
    };
    updateDefaultVisualizationPreferences(preferencesPatch)
      .then(response => {
        setStyle(response.data.style);
      });
  }

  return (
    <StyleForm style={style} updateOption={updateOption}/>
  );
}