import FormControl from "@material-ui/core/FormControl";
import React, { useEffect } from "react";
import {
  Checkbox,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select
} from "@material-ui/core";

export function MultiSelect({ label, options, selected, setSelected }) {
  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);

  useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, [inputLabel]);

  return (
    <FormControl fullWidth variant="outlined">
      <InputLabel id="multi-select-label" ref={inputLabel}>{label}</InputLabel>
      <Select
        variant="outlined"
        labelId="multi-select-label"
        id="multi-select"
        multiple
        value={selected || []}
        onChange={setSelected}
        input={<OutlinedInput labelWidth={labelWidth}/>}
        renderValue={(selected) => selected.join(', ')}
      >
        {options.map((name) => (
          <MenuItem key={name} value={name}>
            <Checkbox checked={selected?.indexOf(name) > -1}/>
            <ListItemText primary={name}/>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}