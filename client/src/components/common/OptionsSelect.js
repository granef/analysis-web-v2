import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import React from "react";

export function OptionsSelect({ options, ...restProps }) {
  return (
    <Select {...restProps}>
      {options.map((option, index) => (
        <MenuItem value={option.value} key={index}>{option.label}</MenuItem>
      ))}
    </Select>
  )
}