import Tooltip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";
import React from "react";

export function IconButtonWithTooltip({ title, Icon, ...restProps }) {
  return (
    <Tooltip title={title} arrow>
      <Button
        {...restProps}
        size="small"
        variant="contained">
        <Icon fontSize="small"/>
      </Button>
    </Tooltip>
  )
}