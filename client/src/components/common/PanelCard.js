import React  from 'react';
import { makeStyles } from "@material-ui/core/styles";
import { Card } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    borderWidth: 2,
    borderColor: theme.palette.secondary.light,
    overflow: "auto",
    [theme.breakpoints.down('xs')]: {
      maxHeight: "calc(100vh - 72px)",
    },
    [theme.breakpoints.up('sm')]: {
      maxHeight: "calc(100vh - 80px)",
    },
    [theme.breakpoints.up('md')]: {
      maxHeight: "calc(100vh - 88px)",
    },
  },
}));

export function PanelCard({ open, children }) {
  const classes = useStyles();

  return open ? (
    <Card className={classes.root} variant="outlined">
      {children}
    </Card>
  ) : null;
}