import { Box } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import React from "react";

export function Loader(props) {
  return (
    <Box {...props} display="flex">
      <Box m="auto">
        <CircularProgress color="inherit"/>
      </Box>
    </Box>
  )
}