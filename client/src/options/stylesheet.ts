import { Css, Stylesheet } from 'cytoscape';

const nodeStyle: Css.Node = {
  'width': 'data(width)',
  'height': 'data(height)',
  'background-color': 'data(bg)',
  'border-color': 'white',
  'label': 'data(label)',
  'min-zoomed-font-size': 10,
  // @ts-ignore
  'z-index': 'data(zIndex)',
};

const getNodeStyle = (borders: boolean, labels: boolean): Css.Node => ({
  'width': 'data(width)',
  'height': 'data(height)',
  'background-color': 'data(bg)',
  'background-fill': function( ele: { data: (arg0: string) => any; } ) {return ele.data('bgFill') ? ele.data('bgFill') : 'solid'},
  'background-gradient-stop-colors': function( ele: { data: (arg0: string) => any; } ) {return ele.data('gradientColors') ? ele.data('gradientColors') : ele.data('bg')},
  'background-gradient-stop-positions': function( ele: { data: (arg0: string) => any; } ) {return ele.data('gradientStoppingPoints') ? ele.data('gradientStoppingPoints') : '0%'},
  'border-color': 'white',
  'border-width': borders ? 3 : 0,
  'min-zoomed-font-size': 10,
  'label': labels ? 'data(label)' : '',
  // @ts-ignore
  'z-index': 'data(zIndex)',
});

const edgeStyle: Css.Edge = {
  'width': 'data(width)',
  'line-color': 'data(bg)',
  'curve-style': 'bezier',
  'target-arrow-shape': 'triangle',
  'target-arrow-color': 'data(bg)',
  'min-zoomed-font-size': 10,
};

const getEdgeStyle = (arrows: boolean, labels: boolean): Css.Edge => ({
  'width': 'data(width)',
  'line-color': 'data(bg)',
  'curve-style': 'bezier',
  'target-arrow-shape': arrows ? 'triangle' : 'none',
  'target-arrow-color': 'data(bg)',
  'min-zoomed-font-size': 10,
  'label': labels ? 'data(label)' : '',
});

export const selectedNodeStyle: Css.Node = {
  'border-width': 6,
  'border-color': '#ef8157',
}

// const selectedEdgeStyle : Css.Edge = {
//   "line-style": "dashed",
//   "line-dash-pattern": [10, 3]
// }

const coreStylesheet: Css.Core = {
  'active-bg-color': '#ffffff',
  'active-bg-opacity': 1,
  'active-bg-size': 1,
  'outside-texture-bg-color': '#ffffff',
  'outside-texture-bg-opacity': 1,
  'selection-box-border-width': 1,
  'selection-box-opacity': 0.2,
  'selection-box-color': '#ef8157',
  'selection-box-border-color': '#a479fa',
  // "selection-box-border-width": 1,
  // "selection-box-opacity": 1
};

/**
 * Part of the stylesheet that does not depend on user preferences.
 */
const baseStylesheet: Stylesheet[] = [
  {
    selector: ':selected',
    css: selectedNodeStyle,
  },
  {
    selector: 'core',
    css: coreStylesheet,
  },
  {
    selector: '.timelineHidden',
    css: {
      display: 'none',
    },
  },
  {
    selector: '.userHidden',
    css: {
      display: 'none',
    },
  },
];

export const stylesheet: Stylesheet[] = [
  {
    selector: 'node',
    style: nodeStyle,
  },
  {
    selector: 'edge',
    style: edgeStyle,
  },
  ...baseStylesheet,
];

interface IStylesheet {
  nodeBorders: boolean;
  nodeLabels: boolean;
  edgeArrows: boolean;
  edgeLabels: boolean;
}

/**
 * Returns graph's stylesheet for the given style preferences.
 * @param stylePreferences
 */
export const getStylesheet = (stylePreferences: IStylesheet): Stylesheet[]  => {
  return [
    {
      selector: 'node',
      style: getNodeStyle(stylePreferences.nodeBorders, stylePreferences.nodeLabels),
    },
    {
      selector: 'edge',
      style: getEdgeStyle(stylePreferences.edgeArrows, stylePreferences.edgeLabels),
    },
    ...baseStylesheet,
  ];
}