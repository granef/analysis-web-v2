import { cola } from './cola';
import { fcose } from './fcose';

/**
 * For given layout name returns default layout options.
 * @param name
 */
export const getLayoutOptions = (name: string) => {
  switch (name) {
    case 'cola':
      return cola;
    case 'fcose':
      return fcose;
    default:
      return {
        name: name,
      }
  }
};