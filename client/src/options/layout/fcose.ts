/**
 * Default fcose options.
 * See API at {@link https://github.com/iVis-at-Bilkent/cytoscape.js-fcose#api}.
 */
export const fcose = {
  name: 'fcose',
  animate: false,
  // fit: false,
  packComponents: false,
  // quality: "proof",
  nodeDimensionsIncludeLabels: true,

  /* spectral layout options */

  // False for random, true for greedy sampling
  samplingType: true,
  // Sample size to construct distance matrix
  sampleSize: 10,
  // Separation amount between nodes
  nodeSeparation: 75,
  // Power iteration tolerance
  piTol: 0.0000001,

  /* incremental layout options */

  // Node repulsion (non overlapping) multiplier
  nodeRepulsion: 45000,
  // Ideal edge (non nested) length
  idealEdgeLength: /*(edge: any) => */300,
  // Divisor to compute edge forces
  edgeElasticity: 0.45,
  // Nesting factor (multiplier) to compute ideal edge length for nested edges
  // Maximum number of i  nestingFactor: 0.1,terations to perform - this is a suggested value and might be adjusted by the algorithm as required
  numIter: 2500,
  // For enabling tiling
  tile: true,
  // Represents the amount of the vertical space to put between the zero degree members during the tiling operation(can also be a function)
  tilingPaddingVertical: 10,
  // Represents the amount of the horizontal space to put between the zero degree members during the tiling operation(can also be a function)
  tilingPaddingHorizontal: 10,
  // Gravity force (constant)
  gravity: 0.25,
  // // Gravity range (constant) for compounds
  // gravityRangeCompound: 1.5,
  // // Gravity force (constant) for compounds
  // gravityCompound: 1.0,
  // // Gravity range (constant)
  // gravityRange: 3.8,
  // Initial cooling factor for incremental layout
  initialEnergyOnIncremental: 0.3,
}