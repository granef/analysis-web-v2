import { NodeSingular } from 'cytoscape';

export const colaIsLocked = (node: NodeSingular) => {
    return node.locked() || node.data('layoutLocked');
};

/**
 * Default cola options.
 *
 * See API at {@link https://github.com/cytoscape/cytoscape.js-cola#api}.
 */
export const cola = {
  name: 'cola',
  animate: true,
  infinite: true,
  fit: false,
  padding: 0,
  nodeDimensionsIncludeLabels: true,
  nodeSpacing: 10,
  // edgeLength: 10,
  isLocked: colaIsLocked,
  lockEvents: 'lock unlock layoutLock layoutUnlock',
  // nodeSpacing: function( node ){
  //   return node.data().id*10;
  // },
  convergenceThreshold: 0.09,
};