import { getColorForType, getNodeType, getNodeLabel, edgeFactory } from "./parse-utils";
import { NodeData } from './parse-types';
import { EdgeDefinition } from 'cytoscape';

export const BASE_NODE_Z_INDEX = 1000;
export function parseToGraph(
  data: NodeData[],
  parentNode: NodeData,
  key: string,
  nodes: any,
  edges: EdgeDefinition[]
) {
  data.forEach((nodeData: NodeData) => {
    nodes[nodeData.uid] = nodes[nodeData.uid] || {};

    const node = nodes[nodeData.uid];

    if (!!parentNode) {
      const edge = key[0] === '~' ? edgeFactory(nodeData, parentNode, key.substring(1)) : edgeFactory(parentNode, nodeData, key);
      edges.push(edge);
    }

    // assign general data
    Object.entries(nodeData)
      .filter(([, val]) => isGeneralProperty(val))
      .forEach(([key,]) => node[key] = nodeData[key]);

    node["dgraph.type"] = getNodeType(nodeData);
    node.label = getNodeLabel(nodeData);
    node.bg = getColorForType(getNodeType(nodeData));
    node.id = node.uid;
    node.width = 25;
    node.height = 25;
    node.zIndex = BASE_NODE_Z_INDEX;

    // process nested nodes
    Object.entries(nodeData)
      .filter(([, val]) => shouldRecur(val))
      .forEach(([key, val]) => parseToGraph(val, node, key, nodes,  edges));
  })
}

function isGeneralProperty(obj: any) {
  return !shouldRecur(obj);
}

/**
 * Returns `true` if the object should be further processed to look for other nodes, `false` otherwise.
 */
function shouldRecur(obj: any) {
  return (obj instanceof Array && obj.length > 0 && !!obj[0].uid) || (obj === Object(obj) && !!obj.uid);
}
