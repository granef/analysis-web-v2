import { NodeType } from './node-type';
import { NodeData } from './parse-types';
import { EdgeDefinition } from 'cytoscape';
import moment from 'moment';

export function getColorForType(nodeType: NodeType) {
  switch (nodeType) {
    case 'Connection':
      return '#d79b00';
    case 'Host':
      return '#82b366';
    case 'File':
    case 'Hostname':
    case 'Software':
    case 'User_Agent':
    case 'X509':
      return "#666666";
    case 'Dns':
    case 'Files':
    case 'Ftp':
    case 'Http':
    case 'Kerberos':
    case 'Ntp':
    case 'Notice':
    case 'Rdp':
    case 'Sip':
    case 'Smtp':
    case 'Ssl':
    case 'Ssh':
    case 'Smb_Files':
    case 'Smb_Mapping':
      return '#b85450';
    case 'Misp':
    case 'Ioc':
      return '#6c8ebf';
    default: {
      console.warn(`Unknown node color for type ${nodeType}`);
      return '#282c34';
    }
  }
}

export function getNodeType(nodeData: NodeData): NodeType {
  return (typeof nodeData['dgraph.type'] === 'string' ? nodeData['dgraph.type'] : nodeData['dgraph.type'][0]) as NodeType;
}

export function getNodeLabel(nodeData: NodeData) {
  const nodeType = getNodeType(nodeData);

  switch (nodeType) {
    case 'Connection':
      return moment.utc(nodeData['connection.ts']).format('MM/DD/YYYY HH:mm:ss:SSS');
    case 'Host':
      return nodeData['host.ip'];
    default: {
      return nodeType;
    }
  }
}

export function nodeFactory(data: NodeData) {
  return {
    data: data,
    group: 'nodes',
  }
}

export function edgeFactory(source: NodeData, target: NodeData, label: string): EdgeDefinition {
  const targetType = getNodeType(target);
  const bg = getColorForType(targetType);

  return {
    data: {
      id: `${label}:${source.uid}->${target.uid}`,
      source: source.uid,
      target: target.uid,
      bg: bg,
      width: 1,
      label: label,
    },
    group: 'edges',
  }
}