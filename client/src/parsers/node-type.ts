export const NodeTypeValues = [
  'Host',
  'Connection',
  'Misp', 'Ioc',
  // Host data
  'File', 'Hostname', 'Software', 'User_Agent', 'X509',
  // Application
  'Dns', 'Files', 'Ftp', 'Http', 'Kerberos', 'Ntp', 'Notice', 'Rdp', 'Sip', 'Smtp', 'Ssl', 'Ssh', 'Smb_Mapping', 'Smb_Files'
] as const;

export const NodeTypeLabels = {
  'Host': 'Host',
  'Connection': 'Connection',
  'Misp': 'MISP',
  'Ioc': 'IOC',
  // Host data
  'File': 'File',
  'Hostname': 'Hostname',
  'Software': 'Software',
  'User_Agent': 'User Agent',
  'X509': 'X509',
  // Application
  'Dns': 'DNS',
  'Files': 'Files',
  'Ftp': 'FTP',
  'Http': 'HTTP',
  'Kerberos': 'Kerberos',
  'Ntp': 'NTP',
  'Notice': 'Notice',
  'Rdp': 'RDP',
  'Sip': 'SIP',
  'Smtp': 'SMTP',
  'Ssl': 'SSL',
  'Ssh': 'SSH',
  'Smb_Mapping': 'SMB Mapping',
  'Smb_Files': 'SMB Files'
} as const;

export type NodeType = typeof NodeTypeValues[number];