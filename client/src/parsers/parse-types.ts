import { NodeDataDefinition } from 'cytoscape';

export interface NodeData extends NodeDataDefinition {
  uid: string,
  "dgraph.type": string[],
  [key: string]: any,
}
