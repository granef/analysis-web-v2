import { parseToGraph } from './parse-to-graph';

/**
 * Parses a response from Granef analysis API and assumes the response directly contains the data to be parsed.
 * It should be used for parsing specific granef API responses.
 */
export function parseGranefResponse(body) {
  const parser = (data, nodes, edges) => parseToGraph(data, null, undefined, nodes, edges);
  return parse(body, parser);
}

/**
 * Parses a response from Granef analysis API and assumes the response contains the data to be parsed nested in an object
 * keyed by name of the corresponding query. It should be used for parsing general custom GRANEF API response.
 */
export function parseGranefMultiResponse(body) {
  const parser = (data, nodes, edges) => data.forEach(d => parseToGraph(d, null, undefined, nodes, edges));
  return parse(body, parser);
}

/**
 * Maps GRANEF API response to Cytoscape elements.
 *
 * @param body response body
 * @param parser function to map response body mapper to Cytoscape elements
 * @returns {{nodes: {data: *, group: string}[], edges: {data: *, group: string}[]}}
 */
function parse(body, parser) {
  const data = Object.values(body.response);

  const nodes = {};
  const edges = [];

  parser(data, nodes, edges);

  const cyNodes = Object.values(nodes).map(node => ({ data: node, group: "nodes" }));
  return {
    nodes: cyNodes,
    edges: edges
  }
}