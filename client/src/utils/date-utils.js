export const fixDate = (dateString) => {
  const date = new Date(dateString)
  const userTimezoneOffset = date.getTimezoneOffset() * 60000;
  const fixedDate = new Date(date.getTime() - userTimezoneOffset);
  return fixedDate.toISOString();
}

export const toDatetimeLocal = (dateString) => dateString.slice(0, 16);