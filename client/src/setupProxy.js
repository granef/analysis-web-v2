const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    '/server',
    createProxyMiddleware({
      target: 'http://server:5000',
      pathRewrite: {
        '^/server/': '/'
      },
      changeOrigin: true
    })
  )
  app.use(
    '/granef-analysis-api',
    createProxyMiddleware({
      target: 'http://granef-analysis-api:7000',
      pathRewrite: {
        '^/granef-analysis-api/': '/'
      },
      changeOrigin: true
    })
  )
}
