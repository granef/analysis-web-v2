import React from "react";
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { createTheme, ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import { Notfound } from "./components/NotFound";
import CssBaseline from "@material-ui/core/CssBaseline";
import MyAppBar from "./components/MyAppBar";

import { VisualizationsManager } from "./components/visualization/list/VisualizationsManager";
import { NewVisualization } from "./components/visualization/details/NewVisualization";
import { Visualization } from "./components/visualization/graph/Visualization";
import { EditVisualization } from "./components/visualization/details/EditVisualization";
import { DefaultSettings } from "./components/settings/DefaultSettings";

/**
 * A HOC component to enable using params in the app bar.
 */
function withAppBar(WrappedComponent) {
  return (
    <>
      <MyAppBar/>
      <WrappedComponent/>
    </>
  )
}

export default function App(props) {
  const appTheme = createTheme({
    palette: {
      primary: {
        main: "#ef8157",
        light: "#f4a88a",
        dark: "#d6744e"
      },
      secondary: {
        main: "#ef8157",
        light: "#ff965f",
        dark: "#b23401"
      },
    },
    zIndex: {
      cyBox: 0,
      panel: 1000,
      toolsPopper: 1100,
      appBar: 1200,
      loader: 2000,
    }
  });
  appTheme.overrides.MuiToggleButton = {
    root: {
      '&$selected': {
        backgroundColor: appTheme.palette.primary.main,
        color: '#ffffff',
        '&:hover': {
          backgroundColor: appTheme.palette.primary.dark,
        },
      },
      '&:hover': {
        backgroundColor: '#b3abef',
        color: '#ffffff'
      },
    },
  };

  return (
    <MuiThemeProvider theme={appTheme}>
      <CssBaseline/>
      <Router>
        <div className="App"
             style={{
               height: "100vh",
               display: "flex",
               flexFlow: "column",
             }}
        >
          <CssBaseline/>
          <Switch>
            <Route exact path="/">
              <Redirect to="/visualizations/"/>
            </Route>

            <Route exact path="/visualizations"
                   render={() => withAppBar(VisualizationsManager)}/>
            <Route exact path="/visualizations/new"
                   render={() => withAppBar(NewVisualization)}/>
            <Route exact path="/visualizations/:id"
                   render={() => withAppBar(Visualization)}/>
            <Route exact path="/visualizations/:id/details"
                   render={() => withAppBar(EditVisualization)}/>
            <Route exact path="/preferences"
                   render={() => withAppBar(DefaultSettings)}/>

            <Route exact path="/not-found" render={() => withAppBar(Notfound)}/>
            <Route render={() => withAppBar(Notfound)}/>
          </Switch>
        </div>
      </Router>
    </MuiThemeProvider>
  );
};
