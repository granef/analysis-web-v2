import { createContext, useState } from "react";
import { hostsInfoQueryOption } from "../components/visualization/graph/side-panel/query/fetch/query-options";

/**
 * Context for storing {@link FetchForm} state between mounts.
 *
 * @type {React.Context<{query: string, clusterizeMode: string, params: null}>}
 */
export const FetchContext = createContext({
  query: 'OVERVIEW_HOST_INFO',
  clusterizeMode: 'timeline',
  params: null
});

export const FetchContextProvider = (props) => {
  const [query, setQuery] = useState(hostsInfoQueryOption);
  const [clear, setClear] = useState(false);
  const [clusterizeMode, setClusterizeMode] = useState('none');
  const [params, setParams] = useState(null);

  return (
    <FetchContext.Provider value={{
      query, setQuery,
      clusterizeMode, setClusterizeMode,
      clear, setClear,
      params, setParams
    }}>
      {props.children}
    </FetchContext.Provider>
  );

}