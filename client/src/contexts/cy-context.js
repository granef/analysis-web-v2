import React, { createContext, useContext, useEffect, useRef, useState } from "react";
import { CyCustomEvents } from "../cy-extensions/cy-custom-events";
import { join } from "lodash";
import { contextMenuOptions } from "../context-menu/options";
import { contextMenuHandler } from "../context-menu/handler";
import { getVisualizationPreferences } from "../api/visualizations-api";
import { colaIsLocked } from "../options/layout/cola";
import { PreferencesContext } from "./preferences-context";

/**
 * Context for storing cytoscape graph instance.
 *
 * @type {React.Context<[GranefCore | null, (cy: GranefCore | null) => void]>}
 */
export const CyContext = createContext();

/**
 * Sets locks the target of the given event for cola layout.
 * @param event emitted event
 */
const layoutLock = (event) => {
  event.target.data('layoutLocked', true);
  event.target.emit('layoutLock');
}

export const CyContextProvider = (props) => {
  const { id } = props;
  const [cy, setCy] = useState(/** @type {GranefCore} */ null);
  const layoutInstance = useRef();
  const [preferences] = useContext(PreferencesContext);

  useEffect(() => {
    setCy(props.cy);
  }, [props.cy]);

  /**
   * Add listeners to events which change the data in the graph, so that it can be saved. Such events are fetch, delete,
   * create and open cluster, and assigning colors.
   */
  useEffect(() => {
    if (cy && id) {
      const saveState = (evt) => {
        evt.cy.save(id);
      }

      const autoSaveEvents = join([
        CyCustomEvents.FETCH,
        CyCustomEvents.DELETE_SELECTION,
        CyCustomEvents.CLUSTER_SELECTION,
        CyCustomEvents.OPEN_CLUSTER,
        CyCustomEvents.COLOR
        /* consider 'dragfree' and 'lock' */
      ], ' ');

      cy.on(autoSaveEvents, saveState);

      return () => {
        cy.off(autoSaveEvents, saveState);
      }
    }
  }, [cy, id]);

  /**
   * Register context menus.
   */
  useEffect(() => {
    if (!cy || !id || !preferences?.timelineClustering) {
      return;
    }

    const contextMenu = cy.contextMenus(contextMenuOptions(id, props.setLoading, props.setStatus, preferences.timelineClustering));
    const cxttap = contextMenuHandler(contextMenu);

    cy.on('cxttap', cxttap);
    return () => {
      cy.off('cxttap', cxttap);
    }
  }, [cy, id, props.setStatus, props.setLoading, preferences?.timelineClustering]);

  /**
   * Add listeners for refreshing layout.
   */
  useEffect(() => {
    if (!cy || !id) {
      return;
    }

    const setLoading = props.setLoading;

    const refreshLayoutCallback = (lock = false) => async (event, ids, fit = false) => {
      setLoading(true);

      const response = await getVisualizationPreferences(id);

      const preferences = response.data;
      const layout = preferences.layout;

      if (layout.name === 'cola') {
        layout.isLocked = colaIsLocked; // functions are not serialized by JSON.stringify. this is to avoid using eval
        layout.lockEvents = 'lock unlock layoutLock layoutUnlock';
      }

      const cy = event.cy;

      let lockedNodes;
      if (layout.name === 'cola' && lock && ids !== undefined && ids.length > 0) {
        lockedNodes = cy.nodes(':unlocked');
        lockedNodes = lockedNodes.filter(node => !ids.includes(node.data('id')))
        lockedNodes.lock();
      }

      layoutInstance.current?.stop();

      if (layoutInstance.current?.options.name === 'cola' && layout.name !== 'cola') {
        // If the layout is being switched from cola, it is desirable to unlock all the nodes for the layout. Otherwise,
        // the user positions assigned to nodes locked for cola with the new layout would be kept when switching back
        // to cola.
        cy.nodes().forEach((node) => {
          node.data('layoutLocked', false);
        });
        cy.off('dragfree', layoutLock);
      } else if (layout.name === 'cola') {
        cy.on('dragfree', layoutLock);
      }

      const newLayoutInstance = cy.layout({
        ...layout,
        ready: () => setLoading(false),
        stop: () => setLoading(false),
      });

      const handleLayoutReady = (evt) => {
        // unlock nodes
        if (lock && lockedNodes) {
          lockedNodes.unlock();
        }

        // fit
        if (fit) {
          setTimeout(() => {
            evt.cy.fitPadded();
            setLoading(false);
          }, 500);
        } else {
          setLoading(false);
        }
      }
      cy.once('layoutready', handleLayoutReady);

      newLayoutInstance.run();
      layoutInstance.current = newLayoutInstance;
    };

    const refreshLayoutEvents = CyCustomEvents.LAYOUT_CHANGED;
    const refreshLayoutWithLockEvents = join([CyCustomEvents.DELETE_SELECTION, CyCustomEvents.CLUSTER_SELECTION, CyCustomEvents.OPEN_CLUSTER, CyCustomEvents.FETCH], ' ');

    const refreshLayout = refreshLayoutCallback(false);
    const refreshLayoutWithLock = refreshLayoutCallback(true);

    cy.on(refreshLayoutEvents, refreshLayout);
    cy.on(refreshLayoutWithLockEvents, refreshLayoutWithLock);

    return () => {
      cy.off(refreshLayoutEvents, refreshLayout);
      cy.off(refreshLayoutWithLockEvents, refreshLayoutWithLock);
    }
  }, [cy, id, props.setStatus, props.setLoading]);

  return (
    <CyContext.Provider value={[cy, setCy]}>
      {props.children}
    </CyContext.Provider>
  );
}