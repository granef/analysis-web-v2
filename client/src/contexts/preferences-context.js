import { createContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getVisualizationPreferences, updateVisualizationPreferences } from "../api/visualizations-api";

/**
 * Context for storing visualization preferences making them accessible across the application.
 */
export const PreferencesContext = createContext(null);

export const PreferencesContextProvider = (props) => {
  const { id } = useParams();
  const [preferences, setPreferences] = useState(null);

  useEffect(() => {
    if (id) {
      getVisualizationPreferences(id)
        .then(response => response.data)
        .then(setPreferences)
        .catch(console.error);
    }
  }, [id]);

  const updatePreferences = async (patch) => {
    console.log('updatePreferences', patch);
    const updateResponse = await updateVisualizationPreferences(id, patch);
    setPreferences(updateResponse.data);
  }

  return (
    <PreferencesContext.Provider value={[preferences, updatePreferences]}>
      {props.children}
    </PreferencesContext.Provider>
  );

}