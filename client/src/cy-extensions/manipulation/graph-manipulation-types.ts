import { Core } from 'cytoscape';

export interface CoreGranefGraphManipulation extends Core {
  /**
   * Returns state snapshot, mainly intended for saving current state of the visualization. It returns an object with
   * elements, clustering scratches, and viewport.
   */
  getState(): void;

  /**
   * Saves the current state to the database.
   *
   * @param visualizationId ID of the visualization which is being represented by the core
   */
  save(visualizationId: string | number): void

  /**
   * Unselects all nodes currently in selection and creates a new selection from all the other nodes.
   */
  invertSelection(): void

  /**
   * Hide all nodes in the selection using style class `userHidden` and make them not selectable.
   */
  hideSelected(): void

  /**
   * Show all nodes that have been hidden by removing their style class `userHidden` and making them selectable again.
   */
  showSelected(): void

  /**
   * Locks all selected nodes.
   */
  lockSelected(): void

  /**
   * Unlocks all selected nodes.
   */
  unlockSelected(): void

  /**
   * Locks all selected nodes in the context of Cola layout.
   */
  layoutLockSelected(): void

  /**
   * Unlocks all selected nodes in the context of Cola layout.
   */
  layoutUnlockSelected(): void

  /**
   * Zooms in by one zoom step.
   */
  zoomIn(): void

  /**
   * Zooms out by one zoom step.
   */
  zoomOut(): void

  /**
   * Fits the camera's view to the whole graph.
   */
  fitPadded(): void
}

export type ClusterizeMode = 'none' | 'timeline' | 'chineseWhispers' | 'singleCluster';