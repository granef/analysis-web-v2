import { BASE_NODES_SCRATCH, SCRATCH_CLUSTERING } from '../clustering/clustering';
import { updateVisualizationState } from '../../api/visualizations-api';
import { Core, NodeSingular } from 'cytoscape';
import { CyCustomEvents } from '../cy-custom-events';

/**
 * @see CoreGranefGraphManipulation#getState
 */
function getState(this: Core) {
  const cy = this;

  const elements = cy.elements().map((element: NodeSingular) => ({
    data: element.data(),
    group: element.group(),
    position: element.position(),
    selected: element.selected(),
    locked: element.locked(),
    //@ts-ignore - error in types, this actually returns `string[]`
    classes: element.classes().filter(clazz => clazz !== 'timelineHidden'),
    // due to timeline, also do not save selectable
    // rest has: grabbable, pannable, removed, selectable
  }));

  const viewport = {
    zoom: cy.zoom(),
    pan: cy.pan(),
  };

  const scratch = {
    clustering: cy.scratch(SCRATCH_CLUSTERING),
    baseNodeIds: cy.scratch(BASE_NODES_SCRATCH),
  };

  return { elements, viewport, scratch };
}

/**
 * @see CoreGranefGraphManipulation#save
 */
function save(this: Core, visualizationId: string | number) {
  // @ts-ignore
  const statePatch = this.getState();
  return updateVisualizationState(visualizationId, statePatch);
}

/**
 * @see CoreGranefGraphManipulation#invertSelection
 */
function invertSelection(this: Core) {
  const selected = this.nodes(':selected').filter(':visible');
  const unselected = this.nodes(':unselected').filter(':visible');

  selected.unselect();
  unselected.select();
}

/**
 * @see CoreGranefGraphManipulation#hideSelected
 */

function hideSelected(this: Core) {
  this.nodes(':selected').filter(':visible')
    .addClass('userHidden')
    .unselect()
    .unselectify();
}

/**
 * @see CoreGranefGraphManipulation#showSelected
 */

function showSelected(this: Core) {
  this.nodes('.userHidden')
    .removeClass('userHidden')
    .selectify();
}

/**
 * @see CoreGranefGraphManipulation#lockSelected
 */
function lockSelected(this: Core) {
  this.nodes(':selected')
    // .filter(':visible')
    .lock()
}

/**
 * @see CoreGranefGraphManipulation#unlockSelected
 */
function unlockSelected(this: Core) {
  this.nodes(':selected')
    // .filter(':visible')
    .unlock()
}

/**
 * @see CoreGranefGraphManipulation#layoutLockSelected
 */
function layoutLockSelected(this: Core) {
  const selection = this.nodes(':selected').filter(':visible');
  selection.forEach(ele => {
    ele.data('layoutLocked', true);
  });
  selection.emit(CyCustomEvents.LAYOUT_LOCK);
}

/**
 * @see CoreGranefGraphManipulation#layoutUnlockSelected
 */
function layoutUnlockSelected(this: Core) {
  const selection = this.nodes(':selected').filter(':visible');
  selection.forEach(ele => {
    ele.data('layoutLocked', false);
  });
  selection.emit('layoutUnlock');
}

const ZOOM_STEP = 0.5;

/**
 * @see CoreGranefGraphManipulation#zoomIn
 */
function zoomIn(this: Core) {
  this.zoom(this.zoom() + ZOOM_STEP);
}

/**
 * @see CoreGranefGraphManipulation#zoomOut
 */
function zoomOut(this: Core) {
  this.zoom(this.zoom() - ZOOM_STEP);
}

/**
 * Padding used for fitting graph to camera's view.
 */
const PADDING = 80;

/**
 * @see CoreGranefGraphManipulation#fitPadded
 */
function fitPadded(this: Core) {
  const eles = this.elements();
  this.fit(eles, PADDING);
}

/**
 * Registers functions extending standard cytoscape functionality with some more specific functions.
 * @see CoreGranefGraphManipulation
 */
export function graphManipulation(cytoscape: (type: string, name: string, registrant: any) => void) {
  cytoscape('core', 'getState', getState);
  cytoscape('core', 'save', save);

  cytoscape('core', 'invertSelection', invertSelection);

  cytoscape('core', 'hideSelected', hideSelected);
  cytoscape('core', 'showSelected', showSelected);

  cytoscape('core', 'lockSelected', lockSelected);
  cytoscape('core', 'unlockSelected', unlockSelected);

  cytoscape('core', 'layoutLockSelected', layoutLockSelected);
  cytoscape('core', 'layoutUnlockSelected', layoutUnlockSelected);

  cytoscape('core', 'zoomIn', zoomIn);
  cytoscape('core', 'zoomOut', zoomOut);

  cytoscape('core', 'fitPadded', fitPadded);

}