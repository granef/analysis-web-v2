import { getTimeRangeFiltered } from "../../api/timeline-api";
import { min } from 'lodash';

export interface TimelineDatum {
  id: string,
  time: string,
  start: Date,
  end: Date,
  connections: number,
  ids: string[]
}

/**
 * Returns IDs for intervals of given length within a time range bounded by `minDate` and `maxDate`.
 *
 * @param uids list of node IDs
 * @param minDate lower time range bound
 * @param maxDate upper time range bound
 * @param interval length of an interval in milliseconds (last might be shorter)
 */
export async function computeTimelineData(uids: string[], minDate: Date, maxDate: Date, interval: number): Promise<TimelineDatum[]> {
  const newTimelineData = [];
  let start = minDate;
  let end;
  do {
    const endTime = min([start.getTime() + interval, maxDate.getTime()]) as number;
    end = new Date(endTime);

    const filtered = await getTimeRangeFiltered(uids, start, end);
    newTimelineData.push({
      id: start.toISOString(),
      time: `${start.toISOString()} - ${end.toISOString()}`,
      start: start,
      end: end,
      connections: filtered.data.response.uids.length,
      ids: filtered.data.response.uids
    });

    start = new Date(end.getTime() + 1);
  } while (end < maxDate);

  return newTimelineData;
}
