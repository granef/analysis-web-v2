import { CoreClustering } from '../clustering/clustering-types';
import { Collection, NodeSingular } from 'cytoscape';

/**
 * @see CoreTimeline#hideNodes
 */
function hideNodes(this: CoreClustering, ids: string[]) {
  const nodes = this.getTopLevelNodes(ids);
  nodes
    .filter(node => node.data('dgraph.type') === 'Connection' || node.data('connectionsOnly'))
    .forEach(node => node
      .unselect()
      .unselectify()
      .addClass('timelineHidden'));
}

/**
 * @see CoreTimeline#showNodes
 */
function showNodes(this: CoreClustering, ids: string[]) {
  const nodes = this.getTopLevelNodes(ids);
  nodes.forEach(node => node
    .selectify()
    .removeClass('timelineHidden'));
}

/**
 * Hides the nodes upon which the function was called in the context of timeline by adding `timelineHidden` style class.
 * @see CoreTimeline#hideNodes
 */
function hideNodesCollection(this: Collection) {
  this.nodes()
      .unselect()
      .unselectify()
      .addClass('timelineHidden');
}

/**
 * Shows the nodes upon which the function was called in the context of timeline by removing `timelineHidden` style class.
 * @see CoreTimeline#showNodes
 */
function showNodesCollection(this: CoreClustering) {
  this.nodes()
    .selectify()
    .removeClass('timelineHidden');
}

/**
 * @see CoreTimeline#showAndHideNodes
 */
function showAndHideNodes(this: CoreClustering, showIds: string[], hideIds: string[]) {
  const canBeHidden = (node: NodeSingular) => {
    return node.data('dgraph.type') === 'Connection' || node.data('connectionsOnly');
  }

  const showNodes = this.getTopLevelNodes(showIds)
    .filter(canBeHidden);

  const hideNodes = this.getTopLevelNodes(hideIds)
    .filter(canBeHidden)
    .filter(id => !showNodes.includes(id));

  this.batch(() => {
    hideNodes.forEach(node => node
      .unselect()
      .unselectify()
      .addClass('timelineHidden')
    );
    showNodes.forEach(node => node
      .selectify()
      .removeClass('timelineHidden'));
  })
}

export function timeline(cytoscape: (type: string, name: string, registrant: any) => void) {
  cytoscape('core', 'timelineHideNodes', hideNodes);
  cytoscape('core', 'timelineShowNodes', showNodes);
  cytoscape('core', 'timelineShowAndHideNodes', showAndHideNodes);

  cytoscape('collection', 'timelineHideNodes', hideNodesCollection);
  cytoscape('collection', 'timelineShowNodes', showNodesCollection);
}