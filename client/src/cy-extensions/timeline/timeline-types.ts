import { Core } from 'cytoscape';
import { CoreClustering } from '../clustering/clustering-types';

export interface CoreTimeline extends Core {
  /**
   * Maps given list of Granef UIDs to their top-level node IDs in the visualization and hides these in the context of
   * timeline by adding `timelineHidden` style class.
   *
   * @param ids list of Granef UIDs
   */
  hideNodes(this: CoreClustering, ids: string[]): void;

  /**
   * Maps given list of Granef UIDs to their top-level node IDs in the visualization and shows these in the context of
   * timeline by removing `timelineHidden` style class.
   *
   * @param ids list of Granef UIDs
   */
  showNodes(this: CoreClustering, ids: string[]): void;

  /**
   * Maps given lists of Granef UIDs to top-level nodes in the visualization and hides/shows these in the context of timeline.
   *
   * @param showIds list of Granef UIDs to be shown in the context of timeline
   * @param hideIds list of Granef UIDs to be hidden in the context of timeline
   */
  showAndHideNodes(this: CoreClustering, showIds: string[], hideIds: string[]): void;
}
