import { CollectionReturnValue, Core, EdgeDefinition, NodeCollection, NodeDefinition, NodeSingular } from 'cytoscape';
import { BaseNodeClusterIndex, ClusterNodeDefinition, EdgeWeights } from './clustering-types';
import { v4 as uuid } from 'uuid';
import { getBaseNodes, getClusterEdge, getClusterNode, isCluster } from './utils';
import { SCRATCH_CLUSTERING } from './clustering';
import { isEmpty } from 'lodash';
import { CyCustomEvents } from '../cy-custom-events';

/**
 * Reduces to `true` if all base nodes are connections, otherwise reduces to true.
 */
const connectionsOnlyReducer = (connectionsOnlyCurrent: boolean, node: NodeSingular) => {
  const connectionsOnly = node.data('cluster') === true && node.data('connectionsOnly');
  const isConnection = node.data('dgraph.type') === 'Connection';
  return connectionsOnlyCurrent && (connectionsOnly || isConnection);
}

/**
 * Returns the inner edges of the cluster to be created. These are the edges between nodes being turned into a cluster.
 *
 * @param nodes nodes to be turned into a cluster
 */
const getBaseEdges = (nodes: NodeCollection): EdgeDefinition[] => {
  const innerEdges = nodes.edgesTo(nodes);

  let baseEdges: EdgeDefinition[] = [];
  nodes
    .filter(isCluster)
    .map(innerCluster => innerCluster.data('baseEdges'))
    .forEach(innerClusterBaseEdges => baseEdges = baseEdges.concat(innerClusterBaseEdges));
  innerEdges
    .map(clusterInnerEdge => {
      const isBaseEdge = isEmpty(clusterInnerEdge.data('baseEdges'));
      return isBaseEdge ? [clusterInnerEdge.json()] : clusterInnerEdge.data('baseEdges');
    })
    .forEach(innerClusterEdgeBaseEdges => baseEdges = baseEdges.concat(innerClusterEdgeBaseEdges))
  return baseEdges;
}

/**
 * Returns an object containing edge weights information keyed by the node at that end, which will be
 * in the neighbourhood of the cluster. It provides necessary information needed to construct edges which will be
 * incident to the cluster.
 *
 * @param nodes nodes to be turned into a cluster
 */
const getEdgeWeights = (nodes: NodeCollection): EdgeWeights => {
  const outerEdges = nodes.edgesWith(nodes.complement().nodes());

  return outerEdges.reduce((acc: EdgeWeights, edge) => {
    const neighbourNode = nodes.contains(edge.source()) ? edge.target() : edge.source();
    const neighbourNodeId = neighbourNode.data('id');

    acc[neighbourNodeId] = acc[neighbourNodeId] || {};
    const neighbourNodeAcc = acc[neighbourNodeId];

    const weight = edge.data('weight') ? edge.data('weight') : 1; // i.e. number of base edges

    neighbourNodeAcc.color = neighbourNodeAcc.color || neighbourNode.data('bg');

    const endNotInClusterKey = nodes.contains(edge.source()) ? 'target' : 'source'
    neighbourNodeAcc[endNotInClusterKey] = (neighbourNodeAcc[endNotInClusterKey] || 0) + weight;
    neighbourNodeAcc.underlyingEdges = neighbourNodeAcc.underlyingEdges || {};
    neighbourNodeAcc.underlyingEdges[endNotInClusterKey] = neighbourNodeAcc.underlyingEdges[endNotInClusterKey] || [];
    neighbourNodeAcc.underlyingEdges[endNotInClusterKey].push(edge);

    return acc;
  }, {});
}

/**
 * Returns definition of edges which will be incident to the created cluster.
 *
 * @param clusterNode cluster node definition
 * @param edgeWeights edge weights object (see {@link getEdgeWeights})
 */
const getClusterIncidentEdges = (clusterNode: ClusterNodeDefinition, edgeWeights: EdgeWeights): EdgeDefinition[] => {
  const clusterId = clusterNode.data.id;

  const clusterIncidentEdges: EdgeDefinition[] = [];
  Object.entries(edgeWeights)
    .forEach(([nodeId, obj]) => {
      if (obj.source) {
        const clusterEdge = getClusterEdge(nodeId, clusterId, clusterNode.data.bg, obj.source, obj.underlyingEdges.source);
        clusterIncidentEdges.push(clusterEdge);
      }
      if (obj.target) {
        const clusterEdge = getClusterEdge(clusterId, nodeId, obj.color, obj.target, obj.underlyingEdges.target);
        clusterIncidentEdges.push(clusterEdge);
      }
    });
  return clusterIncidentEdges;
}

/**
 * Updates `SCRATCH_CLUSTERING` - appends id of the new cluster node to the arrays corresponding to cluster's base nodes.
 *
 * @param cy cytoscape instance
 * @param clusterNode created cluster node
 */
const updateClusteringScratch = (cy: Core, clusterNode: ClusterNodeDefinition) => {
  const baseNodes = getBaseNodes(clusterNode.data.innerNodes);
  const currentScratchClustering: BaseNodeClusterIndex = cy.scratch(SCRATCH_CLUSTERING) || {};
  const updatedScratchClustering = baseNodes.reduce((acc: BaseNodeClusterIndex, node: NodeDefinition) => {
    const nodeId = '' + node.data.id;
    acc[nodeId] = acc[nodeId] || [];
    acc[nodeId].push(clusterNode.data.id);
    return acc;
  }, currentScratchClustering);
  cy.scratch(SCRATCH_CLUSTERING, updatedScratchClustering);
}

/**
 * Computes and returns the definition of cluster nodes and edges incident to it.
 *
 * @param cy cytoscape instance
 * @param nodes nodes to be clustered
 * @return object containing definition of the cluster node and edges incident to it
 */
export function makeCluster(cy: Core, nodes: NodeCollection): {
  clusterNode: ClusterNodeDefinition,
  clusterIncidentEdges: EdgeDefinition[],
} {
  nodes.forEach((node) => {
    node.data('layoutLocked', false);
  });

  const baseEdges: EdgeDefinition[] = getBaseEdges(nodes);

  const edgeWeights = getEdgeWeights(nodes);

  const clusterId = uuid();
  const clusterNode = getClusterNode(clusterId, nodes, baseEdges);
  clusterNode.data.connectionsOnly = nodes.reduce(connectionsOnlyReducer, true);

  const clusterIncidentEdges: EdgeDefinition[] = getClusterIncidentEdges(clusterNode, edgeWeights);

  updateClusteringScratch(cy, clusterNode)

  return { clusterNode, clusterIncidentEdges };
}

/**
 * Creates a cluster node and replaces its inner nodes in the cytoscape instance with it. Returns ID of the created
 * cluster node.
 *
 * @param cy cytoscape instance
 * @param nodes nodes to be clustered
 * @param emitEvent `true` if {@link CyCustomEvents.CLUSTER_SELECTION} event should be emitted, `false` otherwise
 * @return ID of the created cluster node
 */
export function createCluster(cy: Core, nodes: CollectionReturnValue, emitEvent: boolean = true): string {
  const { clusterNode, clusterIncidentEdges } = makeCluster(cy, nodes);

  cy.batch(() => {
    nodes.remove();
    cy.add([clusterNode, ...clusterIncidentEdges]);
  });

  if (emitEvent) {
    cy.emit(CyCustomEvents.CLUSTER_SELECTION, [[clusterNode.data.id]]);
  }

  return clusterNode.data.id;
}