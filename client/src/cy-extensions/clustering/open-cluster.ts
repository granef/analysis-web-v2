import { Core, EdgeDefinition, NodeDefinition, NodeSingular } from 'cytoscape';
import { getBaseNodes, groupBaseEdgesByTopLevelId, isCluster } from './utils';
import { BaseNodeClusterIndex, GroupedBaseEdgesObj } from './clustering-types';
import { keyBy, last } from 'lodash';
import { SCRATCH_CLUSTERING } from './clustering';

/**
 * Collect all base edges either of cluster edges incident to it.
 *
 * @param cluster a cluster node
 */
function getNeighbourhoodBaseEdges(cluster: NodeSingular) {
  const neighborhood = cluster.neighborhood().edges();
  let baseEdges: EdgeDefinition[] = cluster.data('baseEdges');
  neighborhood.forEach((edge: any) => {
    if (edge.data('baseEdges') !== undefined) {
      baseEdges = baseEdges.concat(edge.data('baseEdges'))
    } else {
      baseEdges.push(edge);
    }
  })
  return baseEdges;
}

/**
 * Updates clustering scratch for all given nodes.
 *
 * @param cy cytoscape instance
 * @param innerNodes inner nodes' definitions
 */
function updateClusteringScratch(cy: Core, innerNodes: NodeDefinition[]) { // TODO refactor (see also make-cluster.ts::updateClusteringScratch
  innerNodes.forEach((innerNode: NodeDefinition) => {
    if (innerNode.data.cluster) {
      const scratchClustering: BaseNodeClusterIndex = cy.scratch(SCRATCH_CLUSTERING) || {};

      const baseNodes = getBaseNodes(innerNode.data.innerNodes);
      baseNodes.forEach(baseNode => scratchClustering['' + baseNode.data.id].pop())

      cy.scratch(SCRATCH_CLUSTERING, scratchClustering);
    } else {
      const clusteringScratch = cy.scratch(SCRATCH_CLUSTERING);
      delete clusteringScratch['' + innerNode.data.id]; // base node is no longer contained in any cluster node
      cy.scratch(SCRATCH_CLUSTERING, clusteringScratch);
    }
  })
}

/**
 * Coverts given base edges grouped by its source and target nodes into a list of edges which should be added to the
 * visualization after opening the cluster.
 *
 * @param cy cytoscape instance
 * @param groupedBaseEdges base edges grouped by source and target nodes
 */
function getClusterIncidentEdges(cy: Core, groupedBaseEdges: GroupedBaseEdgesObj): EdgeDefinition[] {
  return Object.values(groupedBaseEdges)
    .filter((edge: any) => edge.source !== edge.target)
    .reduce((acc: any, edgeData: any) => {
      const isSourceCluster = !isCluster(cy.getElementById(edgeData.source));
      const isTargetCluster = !isCluster(cy.getElementById(edgeData.target));

      const openAsBaseEdge = isSourceCluster && isTargetCluster;
      if (openAsBaseEdge) {
        return acc.concat(edgeData.baseEdges);
      } else {
        const newClusterEdge = {
          data: {
            ...edgeData,
            id: `${edgeData.source}->${edgeData.target}`
          },
          group: 'edges'
        }
        acc.push(newClusterEdge);
        return acc;
      }
    }, []);
}

/**
 * Removes the given cluster and replaces it with nodes from which it was first created.
 */
export function openCluster(cy: Core, cluster: NodeSingular): string[] {
  const innerNodes: NodeDefinition[] = Object.values(cluster.data('innerNodes'));

  let baseEdges = getNeighbourhoodBaseEdges(cluster);

  cluster.remove();
  cy.add(innerNodes);

  updateClusteringScratch(cy, innerNodes);

  const clusteringIndex: BaseNodeClusterIndex = cy.scratch(SCRATCH_CLUSTERING);

  const groupedBaseEdges: GroupedBaseEdgesObj = groupBaseEdgesByTopLevelId(cy, baseEdges);

  // filter base edges of clusters
  const innerClusterEdges = Object.values(groupedBaseEdges)
    .filter((edge: any) => edge.source === edge.target);
  const innerClusterEdgesKeyed = keyBy(innerClusterEdges, (edge: any) => last(clusteringIndex[edge.source]) || edge.source);

  // update baseEdges of nodes from the opened cluster
  Object.entries(innerClusterEdgesKeyed)
    .forEach(([nodeId, data]: any) => cy.getElementById(nodeId).data('baseEdges', data.baseEdges));

  // filter base edges which will be represented by edges and map them to visualization edges
  const clusterIncidentEdges = getClusterIncidentEdges(cy, groupedBaseEdges);
  cy.add(clusterIncidentEdges);

  return innerNodes.map(node => node.data.id as string);
}