import { computeTimelineData } from '../../timeline/compute-timeline-data';
import { BASE_NODES_SCRATCH, SCRATCH_CLUSTERING } from '../clustering';
import { isEmpty } from 'lodash';
import { getTimeRange } from '../../../api/timeline-api';
import { CyCustomEvents } from '../../cy-custom-events';
import { CoreClustering } from '../clustering-types';
import moment from 'moment';

const FIVE_MINUTES = 5 * 60_000;

/**
 * Returns minimum and maximum time stamp among connections with UIDs in given list. It adds 1 ms and subtracts 1 ms
 * from min and max respectively, to correct the values for later usage with API finding UIDs in specific intervals.
 * This is necessary as the API is working with open intervals. If it ever changes to better support the visualization,
 * this step will not be necessary.
 *
 * @param uids comma-separated list of UIDs
 */
async function getCorrectedTimeRangeBounds(uids: string) {

  const minMaxResponse = await getTimeRange(uids);

  // subtract/add 1 ms to min/max for correction (this is due to the analysis API operating on open intervals)
  const min = moment.utc(minMaxResponse.data.response['connection.ts.min']).subtract(1, 'ms').toDate();
  const max = moment.utc(minMaxResponse.data.response['connection.ts.max']).add(1, 'ms').toDate();

  return [min, max];
}

/**
 * @see TimelineClustering#clusterizeByTime
 */
async function clusterizeByTime(this: CoreClustering, nodeIds?: string[], emitEvent: boolean = true, interval = FIVE_MINUTES) {
  const cy = this;

  const clusteringIndex = cy.scratch(SCRATCH_CLUSTERING);
  const baseNodeIds = isEmpty(nodeIds)
    ? cy.scratch(BASE_NODES_SCRATCH).filter((id: string) => !clusteringIndex.hasOwnProperty(id)) // filter only those which are not clustered already
    : nodeIds; // these can only be base node ids (alternatively rework so that they're filtered or take intersection with baseNodeIds
  if (baseNodeIds.length <= 1) {
    return;
  }

  const uids = baseNodeIds.join(',');
  let [min, max] = await getCorrectedTimeRangeBounds(uids);
  const timelineData = await computeTimelineData(uids, min, max, interval);

  const clusterNodeIds = timelineData
    .map(data => data.ids as string[])
    .filter(data => data.length > 1)
    .map(data => cy.clusterIds(data, false));

  if (emitEvent && clusterNodeIds.length > 0) {
    cy.emit(CyCustomEvents.CLUSTER_SELECTION, [clusterNodeIds]);
  }
}

/**
 * @see TimelineClustering
 */
export function timelineClustering(cytoscape: (type: string, name: string, registrant: any) => void) {
  cytoscape('core', 'clusterizeByTime', clusterizeByTime);
}