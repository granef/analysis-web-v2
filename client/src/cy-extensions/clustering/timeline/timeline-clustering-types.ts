import { CoreClustering } from '../clustering-types';

/**
 * Extends core clustering functionality with the ability to create clusters based on timeline data.
 */
export interface TimelineClustering extends CoreClustering {
  /**
   * Creates clusters of connections using timestamps. If given `nodeIds` is not empty, only nodes with ID in this
   * list are considered. These IDs should also belong to base nodes. If no list of node IDs is given, any connection node
   * that is not part of a cluster yet is considered.
   *
   * @param nodeIds list of Granef UIDs of nodes to consider - if empty, any connection node that is not in a cluster yet
   * will be considered
   * @param emitEvent if `true`, emits {@link CyCustomEvents.CLUSTER_SELECTION} event
   * @param interval length of an interval in milliseconds (last might be shorter) each cluster will represent
   */
  clusterizeByTime(nodeIds?: string[], emitEvent?: boolean, interval?: number): void;
}

export type TimeUnit = "years" | "months" | "weeks" | "days" | "hours" | "minutes" | "seconds" | "milliseconds";
