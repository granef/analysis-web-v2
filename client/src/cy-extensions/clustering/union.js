import {
  computeClusterEdgeWidth,
  computeClusterNodeSize,
  groupBaseEdgesByTopLevelId,
  isCluster
} from "./utils";
import { isEmpty, get, last, initial, merge } from "lodash";
import { SCRATCH_CLUSTERING } from './clustering';

/**
 * Add given nodes and edges to the existing graph. If a given node has been contained already, its data will
 * only be merged with the new node definition. Otherwise, nodes are added. Edges are examined to see if they should
 * be added to the graph as given or as cluster edges.
 *
 * @param cy graph instance
 * @param nodes nodes to be added
 * @param edges edges to be added
 * @returns {{addedEdgeIds: *[], addedNodeIds: *}} added nodes and edges IDs (not including ones that has been in the visualization already)
 */
export function union(cy, nodes, edges) {
  const clusteringIndex = cy.scratch(SCRATCH_CLUSTERING) || {};

  const inVisualization = node => clusteringIndex.hasOwnProperty(node.data.id) || cy.getElementById(node.data.id).length > 0;
  const notInVisualization = node => !inVisualization(node);

  // handle nodes
  const alreadyInVisualizationNodes = nodes.filter(inVisualization);

  alreadyInVisualizationNodes.forEach(node => {
    const id = node.data.id;

    if (clusteringIndex.hasOwnProperty(id)) {
      const nodeClusteringIndex = clusteringIndex[id];

      const pathEntries = initial(nodeClusteringIndex);
      const clusterId = last(nodeClusteringIndex);

      const clusterNode = cy.getElementById(clusterId);
      const innerNodes = clusterNode.data('innerNodes');

      if (isEmpty(pathEntries)) {
        node.data.bg = innerNodes[id].data.bg;
        innerNodes[id].data = merge(innerNodes[id].data, node.data);
      } else {
        const path = pathEntries.reverse()
          .reduce((acc, pathEntry, currentIndex) => {
            const subPath = currentIndex !== (nodeClusteringIndex.length - 1) ? [pathEntry, 'data', 'innerNodes'] : [pathEntry]
            return acc.concat(subPath);
          }, []);
        path.push(id);
        const baseNode = get(innerNodes, path);
        if (baseNode === undefined) {
          console.error('could not get base node', { innerNodes, path });
        }
        node.data.bg = baseNode.data.bg;
        baseNode.data = merge(baseNode.data, node.data);
      }
      clusterNode.data('innerNodes', innerNodes);
    } else {
      const baseNode = cy.getElementById(id);
      node.data.bg = baseNode.data.bg;
      baseNode.data(merge(baseNode.data(), node.data));
    }
  })

  const newNodes = nodes.filter(notInVisualization);
  cy.add(newNodes);

  // handle edges
  const groupedBaseEdges = groupBaseEdgesByTopLevelId(cy, edges);

  let newEdges = [];
  Object.entries(groupedBaseEdges).forEach(([topLevelEdgeId, obj]) => {

    if (obj.source === obj.target) { // IF THEY SHOULD BE PART OF A CLUSTER NODE
      const clusterNode = cy.getElementById(obj.source).first();

      const baseEdges = clusterNode.data("baseEdges") || [];
      const baseEdgesIds = baseEdges.map(baseEdge => baseEdge.data.id);

      const fetchedEdgesToAdd = baseEdges.length > 0
        ? obj.baseEdges.filter(fetchedEdge => !baseEdgesIds.includes(fetchedEdge.data.id))
        : obj.baseEdges;

      if (fetchedEdgesToAdd.length > 0) {
        const updatedBaseEdges = baseEdges.concat(fetchedEdgesToAdd);

        const clusterSize = updatedBaseEdges.length;
        const clusterNodeSize = computeClusterNodeSize(clusterSize);

        clusterNode.data({
          baseEdges: updatedBaseEdges,
          width: clusterNodeSize,
          height: clusterNodeSize,
        })

        console.debug(`updated baseEdges of cluster node ${clusterNode.data("id")} by adding`, fetchedEdgesToAdd);
      } else {
        console.debug(`cluster node ${clusterNode.data("id")} already contained all the fetched edges`);
      }

    } else { // IF THEY SHOULD BE PART OF AN EDGE, POTENTIALLY A CLUSTER EDGE
      const topLevelEdges = cy.getElementById(topLevelEdgeId).edges();

      if (topLevelEdges.length > 0) {
        console.debug("the top level edge is already in the visualization");

        const topLevelEdge = topLevelEdges.first();
        const baseEdges = topLevelEdge.data("baseEdges");
        if (baseEdges && baseEdges.length > 0) { // if it is a cluster edge, update baseEdges (it cannot be empty, otherwise it is an invalid cluster edge)
          const baseEdgesIds = baseEdges.map(baseEdge => baseEdge.data.id);
          const fetchedEdgesToAdd = obj.baseEdges
            .filter(fetchedEdge => !baseEdgesIds.includes(fetchedEdge.data.id));
          if (fetchedEdgesToAdd.length > 0) {
            console.debug("updated base edges by adding", fetchedEdgesToAdd);

            const updatedBaseEdges = baseEdges.concat(fetchedEdgesToAdd)
            const weight = updatedBaseEdges.length;

            topLevelEdge.data({
              baseEdges: updatedBaseEdges,
              weight: weight,
              width: computeClusterEdgeWidth(weight)
            })
          } else {
            console.debug("no need to add any base edges, all are already present");
          }
        } else { // if it is a base edge, nothing needs to be done since it is in the visualization already (there is nothing to merge due to the nature of database scheme)
          console.debug("it is a base edge, do nothing");
        }
      } else {
        console.debug("the top level edge is not in the visualization yet");

        if (!isCluster(cy.getElementById(obj.source)) && !isCluster(cy.getElementById(obj.target))) {
          // if none of the ends are a cluster, add as base edges
          console.debug("adding new base edge", obj.baseEdges[0]);
          newEdges = newEdges.concat(obj.baseEdges);
        } else {
          const newClusterEdge = ({ data: { ...obj, id: `${obj.source}->${obj.target}` }, group: "edges" });
          console.debug("adding new cluster edge", obj.baseEdges[0]);
          newEdges.push(newClusterEdge);
        }
      }
    }
  })

  cy.add(newEdges);

  return {
    addedNodeIds: newNodes.map(node => node.data.id),
    addedEdgeIds: newEdges.map(edge => edge.data.id)
  };
}