import { Collection, Core, EdgeDefinition, NodeDataDefinition, NodeDefinition, NodeSingular } from 'cytoscape';
import { createCluster, makeCluster } from './make-cluster';
import { CyCustomEvents } from '../cy-custom-events';
import { difference, flatten, last, uniq } from 'lodash';
import { BaseNodeClusterIndex, ClusterizeOptions } from './clustering-types';
import { getBaseNodes, isCluster } from './utils';
import { openCluster } from './open-cluster';
import { removeNodes, removeNodesForVisualization } from '../../api/annotations-api';
import moment from 'moment';
import { union } from './union';
import { GranefCore } from '../granef-core-interface';

export const SCRATCH_CLUSTERING = 'clustering';
export const BASE_NODES_SCRATCH = 'BASE_NODE_IDS';

/**
 * @see CoreClustering#clusterSelection
 */
function clusterSelection(this: Core): string | undefined {
  const selectedNodes = this.nodes(':selected').filter(':visible');

  if (selectedNodes.length <= 1) {
    console.error('Cannot make a cluster of less than two nodes'); // future work: let the user know about the error
    return undefined;
  }

  return createCluster(this, selectedNodes);
}

/**
 * @see CoreClustering#clusterIds
 */
function clusterIds(this: Core, ids: string[], emitEvent: boolean = true): string | undefined {
  if (ids.length <= 1) {
    console.error('Cannot make a cluster of less than two nodes'); // let the user know about the error
    return undefined;
  }

  const nodes = this.nodes().filter(node => ids.includes(node.data('id')));
  return createCluster(this, nodes, emitEvent);
}

/**
 * @see CoreClustering#getClusteringScratch
 */
function getClusteringScratch(this: Core): BaseNodeClusterIndex {
  if (this.scratch(SCRATCH_CLUSTERING) === undefined) {
    this.scratch(SCRATCH_CLUSTERING, {});
  }
  return this.scratch(SCRATCH_CLUSTERING);
}

/**
 * @see CoreClustering#getTopLevelIds
 */
function getTopLevelIds(this: Core, ids: string[]): string[] {
  // @ts-ignore
  const clusteringIndex: BaseNodeClusterIndex = this.getClusteringScratch();
  return ids.map(id => clusteringIndex.hasOwnProperty(id) ? last(clusteringIndex[id]) : id) as string[];
}

/**
 * @see CoreClustering#getTopLevelNodes
 */
function getTopLevelNodes(this: Core, ids: string[]): NodeSingular[] {
  //@ts-ignore
  const topLevelIds = this.getTopLevelIds(ids) as string[];
  return topLevelIds.map((id: string) => this.getElementById(id).nodes().first());
}

/**
 * @see CoreClustering#openClusterSelection
 */
async function openClusterSelection(this: Core, visualizationId: number) {
  const selectedClusters = this.nodes(':selected')
    .filter(':visible')
    .filter(isCluster);
  const clusterIds = selectedClusters.map(cluster => cluster.id());

  let addedNodeIds: string[] = []
  this.batch(() => {
    selectedClusters.forEach(node => {
      const ids = openCluster(this, node)
      addedNodeIds = addedNodeIds.concat(ids);
    })
  });

  await removeNodes(visualizationId, clusterIds);

  this.emit(CyCustomEvents.OPEN_CLUSTER, [addedNodeIds]);
}

/**
 * @see CoreClustering#openClusterById
 */
async function openClusterById(this: Core, visualizationId: number, id: string) {
  const node = this.getElementById(id);
  openCluster(this, node);

  await removeNodes(visualizationId, [id]);

  this.emit(CyCustomEvents.OPEN_CLUSTER);
}


const ORPHAN_KEY = 'ORPHAN';

/**
 * Returns the id of the "key" node for each potential cluster such that:
 * <ul>
 *   <li>if the node has degree>1, its id will be returned;</li>
 *   <li>if node has degree=1, and it is connected to a node with a higher degree, the id of the node with higher degree will be returned;</li>
 *   <li>if both have degree=1, source node id will be returned;</li>
 *   <li>if the node has degree of 0, a special key for "orphaned" nodes will be returned.</li>
 * </ul>
 * Self-loops are not considered in any case.
 *
 */
const getClusterKey = (node: NodeSingular) => {
  const degree = node.degree(false);
  if (degree > 1) {
    return node.data('id');
  } else if (degree === 1) {
    const edge = node.connectedEdges()[0];

    const sourceDegree = edge.source().degree(false);
    const targetDegree = edge.target().degree(false);

    const higherDegreeNode = sourceDegree >= targetDegree ? edge.source() : edge.target();

    return higherDegreeNode.id();
  } else { // degree === 0
    return ORPHAN_KEY;
  }
}

/**
 * @see CoreClustering#clusterOutliers
 */
function clusterOutliers(this: Core) {
  const cy = this;

  const nodes = cy.nodes(':selected').filter(':visible').empty()
    ? cy.nodes()
    : cy.nodes(':selected').filter(':visible');

  const keyedClusters: { [key: string]: string[] } = nodes.reduce((keyedClusters: {
    [key: string]: string[]
  }, node) => {
    const key = getClusterKey(node);
    keyedClusters[key] = keyedClusters[key] || [];
    keyedClusters[key].push(node.id())
    return keyedClusters;
  }, {});

  const clusterIds: string[] = [];
  Object.entries(keyedClusters)
    .filter(([key, nodeIds]) => key !== ORPHAN_KEY && nodeIds.length > 1)
    .map(([, nodeIds]) => nodeIds)
    .forEach((arr: string[]) => {
      const nodes = cy.nodes().filter(node => arr.includes(node.id()));

      const { clusterNode, clusterIncidentEdges } = makeCluster(cy, nodes);

      clusterIds.push(clusterNode.data.id);
      cy.batch(() => {
        nodes.remove();
        cy.add([clusterNode, ...clusterIncidentEdges]);
      });
    });

  cy.emit(CyCustomEvents.CLUSTER_SELECTION, [clusterIds]);
}

/**
 * @see CoreClustering#getGranefData
 */
function getGranefData(this: Collection) {
  return this.nodes().map(getBaseNodeGranefData).flat();
}

/**
 * @see CoreClustering#getGranefUids
 */
function getGranefUids(this: Collection) {
  return this.nodes().map(getNodeGranefIds).flat();
}

const getBaseNodeGranefData = (node: NodeSingular) => {
  const getGranefData = ({ id, label, bg, width, height, layoutLocked, cluster, innerNodes, zIndex, ...granefData }: NodeDataDefinition) => granefData;
  if (node.data('cluster')) {
    return getBaseNodes(node.data('innerNodes'))
      .map(node => node.data)
      .map(getGranefData);
  } else {
    return getGranefData(node.data());
  }
}

const getNodeGranefIds = (node: NodeSingular) => {
  if (node.data('cluster')) {
    return getBaseNodes(node.data('innerNodes'))
      .map(node => node.data.id);
  } else {
    return node.id();
  }
}

/**
 * @see CoreClustering#replaceWithNewGraph
 */
async function replaceWithNewGraph(this: GranefCore, nodes: NodeDefinition[], edges: EdgeDefinition[], visualizationId: number, clusterize: ClusterizeOptions) {
  const cy = this;

  cy.elements().remove();
  await removeNodesForVisualization(visualizationId);

  if (nodes.length === 0 && edges.length === 0) {
    cy.scratch(BASE_NODES_SCRATCH, []);
    cy.scratch(SCRATCH_CLUSTERING, {});
    return [];
  }

  cy.add([...nodes, ...edges]);

  const nodeIds = uniq(nodes.map(node => node.data.id)) as string[];
  cy.scratch(BASE_NODES_SCRATCH, nodeIds);
  cy.scratch(SCRATCH_CLUSTERING, {});

  const clusterizeMode = clusterize.mode;
  if (clusterizeMode === 'timeline' && nodeIds.length > 1) {
    const interval = moment.duration(clusterize.value, clusterize.unit).asMilliseconds();
    await cy.clusterizeByTime([], false, interval);
  } else if (clusterizeMode === 'singleCluster' && nodeIds.length > 1) {
    cy.clusterIds(nodeIds);
  }

  if (nodes.length > 0) {
    cy.emit(CyCustomEvents.FETCH);
  }

  return nodes.length;
}

/**
 * @see CoreClustering#unionWithGraph
 */
async function unionWithGraph(this: GranefCore, nodes: NodeDefinition[], edges: EdgeDefinition[], clusterize: ClusterizeOptions) {
  const cy = this;

  if (nodes.length === 0 && edges.length === 0) {
    return [];
  }
  const { addedNodeIds, addedEdgeIds } = union(cy, nodes, edges);

  const oldBaseNodeIds = cy.scratch(BASE_NODES_SCRATCH) || [];
  cy.scratch(BASE_NODES_SCRATCH, uniq([
    ...oldBaseNodeIds,
    ...addedNodeIds,
  ]));

  const clusterizeMode = clusterize.mode;
  if (clusterizeMode === 'timeline' && addedEdgeIds.length > 1) {
    const interval = moment.duration(clusterize.value, clusterize.unit).asMilliseconds();
    await cy.clusterizeByTime(addedNodeIds, false, interval);
  } else if (clusterizeMode === 'singleCluster' && addedEdgeIds.length > 1) {
    cy.clusterIds(addedNodeIds, false);
  }

  if (addedNodeIds.length > 0 || addedEdgeIds.length > 0) {
    cy.emit(CyCustomEvents.FETCH, [addedNodeIds]);
  }

  return addedNodeIds;
}

/**
 * @see CoreClustering#deleteSelection
 */
async function deleteSelection(this: GranefCore, visualizationId: number) {
  const cy = this;

  const selectedNodes = cy.nodes(':selected').filter(':visible');
  const selectedNodeIds = selectedNodes.map(node => node.id());

  await removeNodes(visualizationId, selectedNodeIds);
  selectedNodes.remove();

  // remove from cluster index
  const clusteringIndex: BaseNodeClusterIndex = cy.scratch(SCRATCH_CLUSTERING);
  Object.entries(clusteringIndex)
    .map(([nodeId, clusterIds]) => [nodeId, last(clusterIds) || '']) // map to [node id, current cluster id]
    .filter(([nodeId, topLevelClusterId]) => /*selectedNodeIds.includes(nodeId) || */selectedNodeIds.includes(topLevelClusterId))
    .forEach(([nodeId, topLevelClusterId]) => delete clusteringIndex[nodeId])

  const baseNodeArrays = selectedNodes
    .map(selectedNode => {
      if (selectedNode.data('cluster') === true) {
        return getBaseNodes(selectedNode.data('innerNodes')).map(baseNode => baseNode.data.id);
      } else {
        return [selectedNode.data('id')];
      }
    });
  const baseNodeIds = flatten(baseNodeArrays);

  const baseNodeIdsScratchUpdated = difference(cy.scratch(BASE_NODES_SCRATCH), baseNodeIds);
  cy.scratch(BASE_NODES_SCRATCH, baseNodeIdsScratchUpdated);

  cy.emit(CyCustomEvents.DELETE_SELECTION);
}

/**
 * Registers functions implementing {@link CoreClustering} extending cytoscape with functionality allowing clusters
 * formation.
 *
 * Keep in mind that by using these clusters you cannot add and remove nodes directly using native cytoscape method
 * as it would cause inconsistencies in internal structures maintained by these functions.
 */
export function clustering(cytoscape: (type: string, name: string, registrant: any) => void) {
  cytoscape('core', 'getClusteringScratch', getClusteringScratch);

  cytoscape('core', 'getTopLevelIds', getTopLevelIds);
  cytoscape('core', 'getTopLevelNodes', getTopLevelNodes);

  cytoscape('core', 'clusterSelection', clusterSelection);
  cytoscape('core', 'clusterIds', clusterIds);

  cytoscape('core', 'openClusterSelection', openClusterSelection);
  cytoscape('core', 'openClusterById', openClusterById);

  cytoscape('core', 'clusterOutliers', clusterOutliers);

  cytoscape('core', 'replaceWithNewGraph', replaceWithNewGraph);
  cytoscape('core', 'unionWithGraph', unionWithGraph);
  cytoscape('core', 'deleteSelection', deleteSelection);

  cytoscape('collection', 'getGranefUids', getGranefUids);
  cytoscape('collection', 'getGranefData', getGranefData);
}