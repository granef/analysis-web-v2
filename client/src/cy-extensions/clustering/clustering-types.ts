import {
  Collection,
  Core,
  EdgeDataDefinition,
  EdgeDefinition,
  EdgeSingular,
  NodeDataDefinition,
  NodeDefinition,
  NodeSingular,
} from 'cytoscape';
import { NodeData } from '../../parsers/parse-types';
import { GranefCore } from '../granef-core-interface';
import { ClusterizeMode } from '../manipulation/graph-manipulation-types';
import { TimeUnit } from './timeline/timeline-clustering-types';

export interface EdgeWeights {
  [id: string]: {
    color: string,
    source?: number,
    target?: number,
    underlyingEdges: {
      source: EdgeSingular[],
      target: EdgeSingular[],
    }
  }
}

export interface ClusterEdgeDefinition extends EdgeDefinition {
  data: ClusterEdgeDataDefinition;
}

export interface ClusterEdgeDataDefinition extends EdgeDataDefinition {
  id: string;
  label?: string;
  bg: string;
  weight: number;
  width: number;
  baseEdges: EdgeDefinition[]
}

export interface ClusterNodeDefinition extends NodeDefinition {
  data: ClusterNodeDataDefinition;
}

export interface InnerNodes {
  [id:string]: NodeDefinition
}
export interface ClusterNodeDataDefinition extends NodeDataDefinition {
  id: string;
  label?: string;
  bg: string;
  size: number;
  width: number;
  height: number;
  cluster: boolean;
  innerNodes: InnerNodes;
  baseEdges?: EdgeDefinition[];
}

export interface ClusterizeOptions {
  mode: ClusterizeMode,
  value?: number,
  unit?: TimeUnit
}

export interface BaseNodeClusterIndex {
  /**
   * Key is the ID of a base node and corresponding value is ID to the top-level cluster s.t. it contains the base node
   * (i.e. the cluster that is currently being displayed)
   */
  [nodeId: string]: string[];
}

export interface GroupedBaseEdgesObj {
  [clusterEdgeId: string]: GroupedBaseEdges;
}

export interface GroupedBaseEdges {
  source: string;
  target: string;
  bg: string;
  baseEdges: EdgeDefinition[];
  weight: number;
  width: number;
}

export interface CoreClustering extends Core {

  /**
   * Returns object containing clustering index.
   */
  getClusteringScratch(): BaseNodeClusterIndex;

  /**
   * Returns list of IDs of top level nodes of nodes with ID in given list of IDs. A base node is the one fetched
   * and parsed from Granef API response. A top level node is the one currently being visualized -- it might be a base
   * node or a cluster node.
   *
   * @param ids list of IDs of base nodes (i.e. list of Granef UIDs)
   */
  getTopLevelIds(ids: string[]): string[];

  /**
   * Returns list of top level nodes of nodes with ID in given list of IDs. A base node is the one fetched and parsed
   * from Granef API response. A top level node is the one currently being visualized -- it might be a base node or
   * a cluster node.
   *
   * @param ids list of IDs of base nodes (i.e. list of Granef UIDs)
   */
  getTopLevelNodes(ids: string[]): NodeSingular[];

  /**
   * Creates a cluster containing currently selected nodes. If the selection does not contain at least 2 nodes,
   * no cluster is created and returns `undefined`. Otherwise, returns ID of the created cluster node.
   *
   * @return ID of the created cluster or `undefined` if no cluster was created
   */
  clusterSelection(): string;

  /**
   * Creates a cluster containing nodes with given IDs. If `ids` does not contain at least 2 nodes,
   * returns `undefined`. Otherwise, returns ID of the created cluster node.
   *
   * @param ids list of node IDs
   * @param emitEvent if `true`, emits {@link CyCustomEvents.CLUSTER_SELECTION} event
   * @return ID of the created cluster or `undefined` if no cluster was created
   */
  clusterIds(ids: string[], emitEvent?: boolean): string;

  /**
   * Opens all clusters in selections and removes the cluster node from the database.
   *
   * @param visualizationId ID of saved visualization
   */
  openClusterSelection(visualizationId: number): void;

  /**
   * Opens the cluster node with given ID.
   *
   * @param visualizationId ID of saved visualization
   * @param id ID of a cluster node
   */
  openClusterById(visualizationId: number, id: string): void;

  /**
   * Creates clusters such that they clusterize nodes of degree 1 with their only neighbours. If any nodes are selected,
   * only subgraph defined by the selection is considered. Note that this only has impact on what outliers will be
   * considered - nodes outside the selection might still end up in a cluster. Otherwise, the whole graph is taken
   * into account.
   */
  clusterOutliers(): void;

  /**
   * Returns data of base nodes fetched from Granef's analytics API.
   */
  getGranefData(this: Collection): NodeData[];

  /**
   * Returns Granef UIDs of base nodes fetched from Granef's analytics API.
   */
  getGranefUids(this: Collection): NodeData[];

  /**
   * Adds given nodes and edges after the old one is cleared. If a clustering mode is given, it will then clusterize
   * given nodes.
   *
   * @param nodes nodes to be added
   * @param edges edges to be added
   * @param visualizationId ID of a visualization
   * @param clusterize clustering settings to apply
   * @return number of added base nodes
   */
  replaceWithNewGraph(nodes: NodeDefinition[], edges: EdgeDefinition[], visualizationId: number, clusterize: ClusterizeOptions): number;

  /**
   * Adds given nodes and edges to the existing graph. If a clustering mode is given, it will then clusterize
   * given nodes.
   *
   * @param nodes nodes to be added
   * @param edges edges to be added
   * @param clusterize clustering settings to apply
   * @return number of added base nodes
   */
  unionWithGraph(nodes: NodeDefinition[], edges: EdgeDefinition[], clusterize: ClusterizeOptions): number;

  /**
   * Deletes selected nodes and takes care of clustering index update.
   * @param visualizationId ID of a visualization
   */
  deleteSelection(this: GranefCore, visualizationId: number): void;

}