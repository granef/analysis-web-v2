import { CoreClustering } from '../clustering-types';
import { Core } from 'cytoscape';

export interface ChineseWhisperClustering extends CoreClustering {
  /**
   * Clusterize the graph using the Chinese Whispers graph clustering algorithm.
   */
  chineseWhisperClusterize(this: Core, emitEvent: boolean): void;
}