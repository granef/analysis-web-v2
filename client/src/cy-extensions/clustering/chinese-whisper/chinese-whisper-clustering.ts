import { Core, EdgeCollection, NodeCollection } from 'cytoscape';
import createGraph, { Graph } from 'ngraph.graph';
// @ts-ignore
import createWhisper from 'ngraph.cw';
import { CoreClustering } from '../clustering-types';
import { CyCustomEvents } from '../../cy-custom-events';

/**
 * Return a ngraph representation of held graph in cytoscape core.
 * @param cy
 */
function createNGraph(cy: Core): Graph {
  let nodes: NodeCollection = cy.nodes();
  let edges: EdgeCollection = cy.edges();

  const graph: Graph = createGraph();
  nodes.forEach(node => {
    graph.addNode(node.data('id'))
  });
  edges.forEach(edge => {
    graph.addLink(edge.source().data('id'), edge.target().data('id'))
  });

  return graph;
}


/**
 * @see ChineseWhisperClustering#chineseWhisperClusterize
 */
function chineseWhisperClusterize(this: CoreClustering, emitEvent: boolean = true) {
  const cy = this;

  const graph: Graph = createNGraph(cy);
  const whisper = createWhisper(graph);

  const requiredChangeRate = 0; // 0 is complete convergence
  while (whisper.getChangeRate() > requiredChangeRate) {
    whisper.step();
  }

  let clusterNodeIds: string[] = [];
  whisper.createClusterMap()
    .forEach((cluster: string[]) => {
      if (cluster.length > 1) {
        const clusterId = cy.clusterIds(cluster, false);
        clusterNodeIds.push(clusterId);
      }
    });

  if (emitEvent && clusterNodeIds.length > 0) {
    cy.emit(CyCustomEvents.CLUSTER_SELECTION, [clusterNodeIds]);
  }
}

/**
 * @see ChineseWhisperClustering
 */
export function chineseWhisperClustering(cytoscape: (type: string, name: string, registrant: any) => void) {
  cytoscape('core', 'chineseWhisperClusterize', chineseWhisperClusterize);
}
