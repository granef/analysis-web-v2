import {
  Core,
  EdgeDefinition,
  EdgeSingular,
  NodeCollection,
  NodeDefinition,
  NodeSingular,
  SingularData,
} from 'cytoscape';
import {
  BaseNodeClusterIndex,
  ClusterEdgeDefinition,
  ClusterNodeDefinition,
  GroupedBaseEdgesObj,
  InnerNodes,
} from './clustering-types';
import { last } from 'lodash';
import { SCRATCH_CLUSTERING } from './clustering';
import { BASE_NODE_Z_INDEX } from '../../parsers/parse-to-graph';

const BASE_NODE_SIZE = 25;
const DEFAULT_CLUSTER_BG = "#acacac";
const DEFAULT_GRADIENT = 'linear-gradient'
const DEFAULT_COLOR_GRADIENT_WEIGHT = 50;

/**
 * Returns width of an edge incident to a cluster based on the number of base edges it represents.
 *
 * @param baseEdgesCount number of base edges the cluster edge represents
 * @return width of the cluster edge
 */
export function computeClusterEdgeWidth(baseEdgesCount: number): number {
  return Math.pow(baseEdgesCount - 1, 1 / 4) + 1;
}

/**
 * Returns size (width and height) of a cluster node based on the number of base nodes it represents.
 *
 * @param baseNodesCount number of base nodes the cluster node contains
 * @return size of the cluster node
 */
export function computeClusterNodeSize(baseNodesCount: number): number {
  return Math.pow(baseNodesCount, 1 / 4) * BASE_NODE_SIZE;
}

function asJson(ele: SingularData) {
  return ele.json();
}

/**
 * Returns new cluster node definition.
 */
export function getClusterNode(
  clusterId: string,
  innerNodes: NodeCollection,
  baseEdges: EdgeDefinition[]
): ClusterNodeDefinition {
  const clusterSize = innerNodes.reduce((acc, node) => acc + (node.data("size") ? node.data("size") : 1), 0); // number of base nodes
  const clusterNodeSize = computeClusterNodeSize(clusterSize);
  const zIndex = Math.round(BASE_NODE_Z_INDEX/clusterSize);
  var gradientArray = innerNodes
    .map(asJson)
    .map(obj => JSON.stringify(obj)).map(str => JSON.parse(str))
    .reduce((acc, node) => {
      acc[node.data.bg] = acc[node.data.bg] ? acc[node.data.bg] + 1 : 1;
      return acc;
    }, {});

  gradientArray = Object.keys(gradientArray).sort().reverse().reduce((r, k) => Object.assign(r, { [k]: gradientArray[k] }), {});
  const dominantColor = Object.keys(gradientArray)[0];
  
  const totalCount = Object.keys(gradientArray).reduce((acc, value) => acc + gradientArray[value], 0);
  var percent = 0;
  var stoppingPoints = '';
  const colorsCount = Object.keys(gradientArray).length;

  Object.keys(gradientArray).forEach(key => {
    percent = percent + (gradientArray[key] / totalCount) * DEFAULT_COLOR_GRADIENT_WEIGHT + (100 - DEFAULT_COLOR_GRADIENT_WEIGHT) / colorsCount;
    stoppingPoints = stoppingPoints + `${percent.toFixed(0)}% `
  })

  return {
    data: {
      id: `${clusterId}`,
      label: '',
      bg: dominantColor ? dominantColor : DEFAULT_CLUSTER_BG,
      bgFill: DEFAULT_GRADIENT,
      size: clusterSize,
      zIndex: zIndex,
      width: clusterNodeSize,
      height: clusterNodeSize,
      innerNodes: innerNodes
        .map(asJson)
        .map(obj => JSON.stringify(obj)).map(str => JSON.parse(str)) // this is only necessary because how @types/visualization are defined - .json() return value is not a string but an object
        .reduce((acc, node) => {
          acc[node.data.id] = node;
          return acc;
        }, {}),
      baseEdges: baseEdges,
      cluster: true,
      gradientColors: Object.keys(gradientArray).join(' '),
      gradientStoppingPoints: stoppingPoints.trim()
    },
    group: "nodes"
  }
}

/**
 * Returns new cluster incident edge definition.
 */
export function getClusterEdge(
  sourceId: string,
  targetId: string,
  color: string,
  weight: number,
  underlyingEdges: EdgeSingular[]
): ClusterEdgeDefinition {
  let baseEdges: EdgeDefinition[] = [];
  underlyingEdges.forEach(edge => {
    if (edge.data("baseEdges") !== undefined) {
      baseEdges = baseEdges.concat(edge.data("baseEdges"));
    } else {
      baseEdges.push(JSON.parse(JSON.stringify(edge.json()))); // this is only necessary because of a bug in @types/visualization
    }
  });

  return {
    "data": {
      "id": `${sourceId}->${targetId}`,
      "source": sourceId,
      "target": targetId,
      "label": '',
      "bg": color,
      "weight": weight,
      "width": computeClusterEdgeWidth(weight),
      "baseEdges": baseEdges,
    },
    "group": "edges",
  }
}

/**
 * Returns definitions of all base nodes contained int the cluster to which given `innerNodesObj` belong.
 */
export function getBaseNodes(innerNodesObj: InnerNodes): NodeDefinition[] {
  const innerNodes: NodeDefinition[] = Object.values(innerNodesObj);

  return innerNodes.reduce((acc: any[], innerNode: NodeDefinition) => {
    if (innerNode.data.cluster) {
      return acc.concat(getBaseNodes(innerNode.data.innerNodes));
    } else {
      acc.push(innerNode);
      return acc;
    }
  }, []);
}

/**
 * Returns `true` if given node is a cluster and `false` otherwise.
 */
export function isCluster(node: NodeSingular) {
  return node.data("cluster") === true;
}

/**
 * Groups baseEdges by the cluster/edge they belong to (respecting which is source/target) and returns an object such that:
 * <ul>
 *   <li>if `sourceId === targetId`, then the object value is an array of inner edges of a cluster;</li>
 *   <li>if `sourceId !== targetId`, and it only contains one edge and both its ends are base nodes, then it is a base edge;</li>
 *   <li>otherwise it is an array of inner edges of a cluster edge.</li>
 * </ul>
 */
export function groupBaseEdgesByTopLevelId(cy: Core, baseEdges: EdgeDefinition[]): GroupedBaseEdgesObj{
  const clusteringIndex: BaseNodeClusterIndex = cy.scratch(SCRATCH_CLUSTERING);

  return baseEdges.reduce((acc: GroupedBaseEdgesObj, edge: EdgeDefinition) => {
    const sourceId = edge.data.source;
    const targetId = edge.data.target;

    const sourceTopLevelNodeId = last(clusteringIndex[sourceId]) || sourceId;
    const targetTopLevelNodeId = last(clusteringIndex[targetId]) || targetId;

    const clusterEdgeId = `${sourceTopLevelNodeId}->${targetTopLevelNodeId}`;

    acc[clusterEdgeId] = acc[clusterEdgeId] || {
      source: sourceTopLevelNodeId,
      target: targetTopLevelNodeId,
      bg: cy.getElementById(targetTopLevelNodeId).data("bg"),
      baseEdges: [] as EdgeDefinition[],
      weight: 0,
      width: 0
    };
    acc[clusterEdgeId].baseEdges.push(edge);
    acc[clusterEdgeId].weight = acc[clusterEdgeId].weight + 1;
    acc[clusterEdgeId].width = computeClusterEdgeWidth(acc[clusterEdgeId].weight)

    return acc;
  }, {});
}