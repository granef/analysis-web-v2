import { Core } from 'cytoscape';
import { CoreGranefGraphManipulation } from './manipulation/graph-manipulation-types';
import { ChineseWhisperClustering } from './clustering/chinese-whisper/chinese-whisper-clustering-types';
import { TimelineClustering } from './clustering/timeline/timeline-clustering-types';
import { CoreClustering } from './clustering/clustering-types';
import { CoreTimeline } from './timeline/timeline-types';

/**
 * Interface gathering all interfaces representing extensions.
 */
export interface GranefCore
  extends Core,
    CoreGranefGraphManipulation,
    CoreClustering,
    CoreTimeline,
    ChineseWhisperClustering,
    TimelineClustering {}
