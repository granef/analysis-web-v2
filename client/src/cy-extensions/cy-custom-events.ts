/**
 * Events introduced to support Granef-specific operations.
 */
export enum CyCustomEvents {
  /**
   * Event to be emitted when nodes are being added to the graph from response obtained from Granef's analytical API.
   */
  FETCH = 'fetch',

  /**
   * Event to be emitted when a new cluster was created.
   */
  CLUSTER_SELECTION = 'clusterSelection',

  /**
   * Event to be emitted when a cluster was opened.
   */
  OPEN_CLUSTER = 'openCluster',

  /**
   * Event to be emitted after deletion of nodes.
   */
  DELETE_SELECTION = 'deleteSelection',

  /**
   * Event to be emitted after layout preferences were changed.
   */
  LAYOUT_CHANGED = ' LAYOUT_CHANGED',

  /**
   * Event to be emitted when nodes were assigned a new color.
   */
  COLOR = ' COLOR',

  /**
   * Event to be emitted when nodes are locked for the Cola layout.
   */
  LAYOUT_LOCK = 'layoutLock'
}