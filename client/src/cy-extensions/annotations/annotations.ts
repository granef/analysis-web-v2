import {
  CentralityParams,
  Colored, Params,
} from '../../components/visualization/graph/side-panel/annotations/annotation-types';
import { assignTag, filterNodes, unassignTag } from '../../api/annotations-api';
import { NodeSingular } from 'cytoscape';
import { CyCustomEvents } from '../cy-custom-events';
import { isEmpty } from 'lodash';
import { GranefCore } from '../granef-core-interface';

/**
 * Assigns all tags in the given list to all nodes in the current selection.
 *
 * @param tags list of tags
 */
function addTags(this: GranefCore, tags: Colored[]) {
  const tagIds = tags.map(tag => tag.id);
  const nodeIds = this.nodes(':selected').filter(':visible').map(node => node.data('id'));
  tagIds.forEach(tagId => {
    assignTag(tagId, nodeIds);
  });
}

/**
 * Unassigns all tags in the given list from all nodes in the current selection.
 *
 * @param tags list of tags
 */
function removeTags(this: GranefCore, tags: Colored[]) {
  const tagIds = tags.map(tag => tag.id);
  const nodeIds = this.nodes(':selected').filter(':visible').map(node => node.data('id'));
  tagIds.forEach(tagId => {
    unassignTag(tagId, nodeIds);
  });
}

/**
 * Changes the color of all nodes in the current selection to the given color.
 */
function colorSelection(this: GranefCore, newColor: string) {
  const cy = this;
  cy.batch(function () {
    cy.nodes().filter(':selected').filter(':visible').forEach(o => {
      o.data('bg', newColor);
      o.incomers('edge').data('bg', newColor);
    });
  });
  cy.emit(CyCustomEvents.COLOR);
}

/**
 * Returns a filter for given centrality params.
 *
 * @param params centrality params
 * @param centralityNormalized function returning normalized centrality value for a node
 */
function getCentralityFilter(params: CentralityParams, centralityNormalized: ((node: NodeSingular) => number)) {
  const [min, max] = params.value;
  if (params.inverted) {
    return (node: NodeSingular) => {
      const centrality = centralityNormalized(node);
      return min > centrality || centrality > max;
    };
  } else {
    return (node: NodeSingular) => {
      const centrality = centralityNormalized(node);
      return min <= centrality && centrality <= max;
    };
  }
}

/**
 * Based on given params, adds satisfying nodes to the selection or creates a new one for them.
 *
 * @param params filtering parameters
 */
async function selectByParams(this: GranefCore, params: Params) {
  const cy = this;
  const filters: ((node: NodeSingular) => boolean)[] = [];

  if (!isEmpty(params.tags)) {
    const filterNodesResponse = await filterNodes(params.tags?.map(tag => tag.id));
    const nodeIds = filterNodesResponse.data.map((node: any) => node.granefId);
    filters.push((node: NodeSingular) => nodeIds.includes(node.data('id')));
  }

  if (params.colors && !isEmpty(params.colors)) {
    const hexArr = params.colors.map(color => color.hex);
    filters.push((node: NodeSingular) => hexArr.includes(node.data('bg').toUpperCase()));
  }

  if (!isEmpty(params.types)) {
    filters.push((node: NodeSingular) => node.data('dgraph.type')
      && params.types?.includes(node.data('dgraph.type')))
  }

  if (params.betweennessCentrality?.apply) {
    const { betweennessNormalized } = cy.elements().betweennessCentrality({ directed: true });
    const betweennessFilter = getCentralityFilter(params.betweennessCentrality, betweennessNormalized);
    filters.push(betweennessFilter);
  }

  if (params.pageRank?.apply) {
    const { rank } = cy.elements().pageRank({});
    const betweennessFilter = getCentralityFilter(params.pageRank, rank);
    filters.push(betweennessFilter);
  }

  if (!isEmpty(filters)) {
    if (params.clear){
      cy.elements().unselect();
    }

    cy.nodes()
      .filter(node => filters
        .map(filter => filter(node))
        .reduce((acc, value) => acc && value, true))
      .select();
  }
}

/**
 * Function acting as a cytoscape extension for registering annotations-related functionality to the core.
 */
export function annotations(cytoscape: (type: string, name: string, registrant: any) => void) {
  cytoscape('core', 'addTags', addTags);
  cytoscape('core', 'removeTags', removeTags);
  cytoscape('core', 'colorSelection', colorSelection);
  cytoscape('core', 'selectByParams', selectByParams);
}