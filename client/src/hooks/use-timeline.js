import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { getTimeRange, getTimeRangeFiltered } from "../api/timeline-api";
import { isEmpty } from "lodash";
import { CyContext } from "../contexts/cy-context";
import { BASE_NODES_SCRATCH } from "../cy-extensions/clustering/clustering";
import { computeTimelineData } from "../cy-extensions/timeline/compute-timeline-data";
import moment from "moment";
import { formatUtcDate } from "../context-menu/utils";

// set to min/max dates as implemented in Granef analysis API
const MIN_DATE = new Date('0001-01-01T00:00');
const MAX_DATE = new Date('9999-12-31T23:59:59');

/**
 * This is a custom hook encapsulating logic necessary for computing timeline data. Based on given options it fetches
 * minimum and maximum dates and then fetches lists of IDs for each interval of defined size in that time range.
 * Besides the timeline data it also returns a variable indicating loading and error occurrence. Also returns functions
 * for updating defined IDs and options. When these are called, it automatically recomputes the timeline data.
 */
export const useTimeline = (defaultOptions, baseNodeIds, hideOutsideTimeRange = true) => {
  const [cy] = useContext(CyContext);

  const [timelineData, setTimelineData] = useState([]);
  const [uids, setUids] = useState(baseNodeIds ? baseNodeIds : cy.scratch(BASE_NODES_SCRATCH));
  const [options, setOptions] = useState(defaultOptions);

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const fetchMinMax = useRef(!defaultOptions.timeRange || !defaultOptions.timeRange.max || !defaultOptions.timeRange.min);

  useEffect(() => {
    if (baseNodeIds) {
      setUids(baseNodeIds);
    } else {
      setUids(cy.scratch(BASE_NODES_SCRATCH));
    }
  }, [cy, baseNodeIds]);

  useEffect(() => {
    fetchMinMax.current = true;
  }, [uids]);

  const fetchData = useCallback(async () => {
    if (fetchMinMax.current) {
      //compute min/max
      console.debug('computing min/max');
      let minMax;
      try {
        const minMaxResponse = await getTimeRange(uids.join(','));
        minMax = minMaxResponse.data.response;
      } catch (error) {
        setError(error);
        return;
      }
      // subtract/add 1 ms to min/max for correction
      const min = moment.utc(minMax['connection.ts.min']).subtract(1, 'ms').toISOString(false);
      const max = moment.utc(minMax['connection.ts.max']).add(1, 'ms').toISOString(false);

      setOptions(prev => ({
        ...prev, timeRange: { min, max }
      }));

      fetchMinMax.current = false;
      return;
    }

    // Fetch data
    console.debug('fetch timeline data');
    if (!cy || cy.elements().empty()) {
      setError('Cannot initialize timeline for an empty graph.');
      return;
    }

    if (isEmpty(uids)) {
      setError('Metadata (baseNodeIds or BASE_NODE_IDS scratch) is missing.');
      return;
    }

    setLoading(true);

    const uidsParam = uids.join(',');

    try {
      const minMaxResponse = await getTimeRange(uidsParam);
      const minInGraph = minMaxResponse.data.response['connection.ts.min'];
      const maxInGraph = minMaxResponse.data.response['connection.ts.max'];

      let min = options.timeRange.min;
      let max = options.timeRange.max;

      if (hideOutsideTimeRange) {
        if (new Date(minInGraph) < new Date(min)) {
          const hideTo = new Date(new Date(min).getTime() - 1)

          const filteredBefore = await getTimeRangeFiltered(uidsParam, MIN_DATE, hideTo);
          const beforeIds = filteredBefore.data.response.uids;

          cy.timelineHideNodes(beforeIds);
        }

        if (new Date(max) < new Date(maxInGraph)) {
          const hideFrom = new Date(new Date(max).getTime() + 1);

          const filteredAfter = await getTimeRangeFiltered(uidsParam, hideFrom, MAX_DATE);
          const afterIds = filteredAfter.data.response.uids;

          cy.timelineHideNodes(afterIds);
        }
      }

      const [minDate, maxDate] = [new Date(min), new Date(max)];
      if ((maxDate.getTime() - minDate.getTime()) / options.interval > 150) {
        // performance is bound by the performance of the Granef analysis API here
        setError('The interval is too small for the size of time range. ' +
          'Please, change the options so that a reasonable performance can be achieved. ' +
          'The maximum allowed intervals in the timeline is currently 100.');
        setLoading(false);
        return;
      }

      const newTimelineData = await computeTimelineData(uids.join(','), new Date(options.timeRange.min), new Date(options.timeRange.max), options.interval);
      setTimelineData(newTimelineData.map(datum => ({ ...datum, id: formatUtcDate(datum.id) })));
      setError(null);
    } catch (error) {
      setError(error.message || "Could not fetch all necessary data.");
      setTimelineData(null);
    }
    setLoading(false);
  }, [cy, uids, options.interval, options.timeRange, hideOutsideTimeRange]);

  useEffect(fetchData, [fetchData]);

  return {
    timelineData,
    options,
    setOptions,
    error,
    loading,
    setUids
  };
}