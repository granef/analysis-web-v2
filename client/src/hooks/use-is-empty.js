import { useEffect, useState } from "react";
import { CyCustomEvents } from "../cy-extensions/cy-custom-events";
import { join } from "lodash";


/**
 * @param cy core, a graph instance
 * @returns {boolean} `true` if the current graph instance is empty, `false` otherwise
 */
export const useIsEmpty = (cy) => {
  const [isEmpty, setEmpty] = useState(false);

  /**
   * Register callback for refreshing empty graph flag
   */
  useEffect(() => {
    if (cy) {
      const refreshEmpty = ({ cy }) => {
        setEmpty(!cy || cy.elements().length === 0);
      }

      refreshEmpty({ cy });

      const refreshEmptyEvents = join([CyCustomEvents.DELETE_SELECTION, CyCustomEvents.FETCH, 'add'], ' ');
      cy.on(refreshEmptyEvents, refreshEmpty);
      return () => {
        cy.off(refreshEmptyEvents, refreshEmpty);
      }
    } else {
      setEmpty(true);
    }
  }, [cy]);

  return isEmpty;
}