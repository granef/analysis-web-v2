import { handleTargetNodeOnly } from "./utils";

/**
 * Context menu handler ensuring the correct context menu actions are displayed.
 *
 * If a single node is selected, the context menu is generated for the node specifically.
 * If the context menu event is emitted with target being a selected node or not a node (i.e. the cytoscape plane or
 * an edge), the menu is generated for the whole selection.
 *
 * @param contextMenu
 * @returns {(function(*): void)|*}
 */
export const contextMenuHandler = (contextMenu) => function (event) {
  const { target, cy } = event;

  contextMenu.hideMenuItem('get-node-attributes');
  contextMenu.hideMenuItem('get-neighbours');
  contextMenu.hideMenuItem('select-neighbourhood');
  contextMenu.hideMenuItem('unselect-neighbourhood');
  contextMenu.hideMenuItem('open-cluster');
  contextMenu.hideMenuItem('open-clusters');

  if ((cy === target || !target.isNode()) && cy.nodes(':selected').empty()) {
    // clicked outside a node and no nodes are selected
    return;
  }

  if (handleTargetNodeOnly(cy, target)) {
    // operations on a single node (i.e. target of the cxttap event)

    const hasUnselectedNeighbourhood = !target.neighbourhood(':unselected').empty();
    if (hasUnselectedNeighbourhood) {
      contextMenu.showMenuItem('select-neighbourhood');
    }

    const anyNeighbourSelected = !target.neighbourhood(':selected').empty();
    if (anyNeighbourSelected) {
      contextMenu.showMenuItem('unselect-neighbourhood');
    }

    const isCluster = target.data('cluster') === true;
    if (isCluster) {
      contextMenu.showMenuItem('open-cluster');
    }

  } else {
    const hasUnselectedNeighbourhood = !cy.nodes(':selected').neighbourhood(':unselected').empty();
    if (hasUnselectedNeighbourhood) {
      contextMenu.showMenuItem('select-neighbourhood');
    }

    const isAnyClusterSelected = cy.nodes(':selected').some(ele => ele.data('cluster'));
    if (isAnyClusterSelected) {
      contextMenu.showMenuItem('open-clusters');
    }
  }

  contextMenu.showMenuItem('get-node-attributes');
  contextMenu.showMenuItem('get-neighbours');
}