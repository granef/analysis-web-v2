import moment from "moment";

/**
 * Returns `true` if the context menu event should be only handled for the target node, that is when the target object
 * is a node, and it is not selected.
 * @param cy
 * @param target
 * @returns {false|*|boolean}
 */
export const handleTargetNodeOnly = (cy, target) => target !== cy && target.isNode() && !target.selected();

/**
 * Formats UTC timestamp using a custom format.
 * @param ts {string} timestamp
 * @param format {string} optional date format, 'MM/DD/YYYY HH:mm:ss:SSS' will be used if not specified
 * @returns {string}
 */
export const formatUtcDate = (ts, format= 'MM/DD/YYYY HH:mm:ss:SSS') => moment.utc(ts).format(format);