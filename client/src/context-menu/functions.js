import { getNeighbours, getNodeAttributes } from "../api/analysis-api";
import { NodeTypeValues } from "../parsers/node-type";
import { fetchAndUnionWithGraph, getCommaSeparatedIds } from "./functions-utils";
import { handleTargetNodeOnly } from "./utils";

export const hostDataTypes = ['File', 'Hostname', 'User_Agent', 'X509', 'Software'];

export const applicationTypes = ['Dns', 'Files', 'Ftp', 'Http', 'Kerberos', 'Ntp', 'Notice', 'Rdp', 'Sip', 'Smtp', 'Ssl', 'Ssh', 'Smb_Mapping', 'Smb_Files'];

export const iocTypes = ['Ioc', 'Misp'];

const getNeighbourhood = (nodeTypes, clustering) => async (event) => {
  const { target, cy } = event;

  const ids = getCommaSeparatedIds(cy, target);

  const fetchFunction = () => getNeighbours(ids, nodeTypes.join(','));
  return fetchAndUnionWithGraph(event, fetchFunction, clustering);
}

export const getAllNeighbours = (clustering) => getNeighbourhood(NodeTypeValues, clustering);

export const getConnectionNeighbours = (clustering) => getNeighbourhood(['Connection'], clustering);

export const getHostNeighbours = (clustering) => getNeighbourhood(['Host'], clustering);

export const getApplicationNeighbours = (clustering) => getNeighbourhood(applicationTypes, clustering);

export const getHostDataNeighbours = (clustering) => getNeighbourhood(hostDataTypes, clustering);

export const getIocNeighbours = (clustering) => getNeighbourhood(iocTypes, clustering);

export const getNodeAttributesMenuFunction = async (event) => {
  const { target, cy } = event;

  const ids = getCommaSeparatedIds(cy, target);

  const fetchFunction = () => getNodeAttributes(ids);
  return fetchAndUnionWithGraph(event, fetchFunction, {
    mode: 'none'
  });
};

export const filterNeighboursByTypes = (node, types) => node.data('dgraph.type') && types.includes(node.data('dgraph.type'));

export const selectByTypes = (event, types) => {
  const { cy, target } = event;
  if (handleTargetNodeOnly(cy, target)) {
    target.neighborhood()
      .filter(node => filterNeighboursByTypes(node, types))
      .select();
  } else {
    cy.nodes(':selected')
      .neighborhood()
      .filter(node => filterNeighboursByTypes(node, types))
      .select();
  }
}
