import {
  applicationTypes,
  iocTypes,
  getAllNeighbours,
  getApplicationNeighbours,
  getConnectionNeighbours,
  getHostDataNeighbours,
  getHostNeighbours,
  getIocNeighbours,
  getNodeAttributesMenuFunction, hostDataTypes, selectByTypes
} from "./functions";
import { handleTargetNodeOnly } from "./utils";

/**
 * Defines all possible menu items.
 *
 * @param visualizationId ID of the visualization being viewed
 * @param loading function for indicating when the menu action is being performed
 * @param processActionResult function that processes result based on given severity and message
 * @param timelineClustering timeline clustering options
 * @returns {{evtType: string, menuItemClasses: string[], menuItems: [{onClickFunction: onClickFunction, show: boolean, coreAsWell: boolean, id: string, content: string},{submenu: [{onClickFunction: onClickFunction, coreAsWell: boolean, selector: string, id: string, content: string},{onClickFunction: onClickFunction, coreAsWell: boolean, selector: string, id: string, content: string},{onClickFunction: onClickFunction, coreAsWell: boolean, selector: string, id: string, content: string},{onClickFunction: onClickFunction, coreAsWell: boolean, selector: string, id: string, content: string},{onClickFunction: onClickFunction, coreAsWell: boolean, selector: string, id: string, content: string}], onClickFunction: onClickFunction, coreAsWell: boolean, selector: string, id: string, content: string},{onClickFunction: onClickFunction, coreAsWell: boolean, selector: string, id: string, content: string},{onClickFunction: onClickFunction, coreAsWell: boolean, selector: string, id: string, content: string},{onClickFunction: onClickFunction, coreAsWell: boolean, selector: string, id: string, content: string},null,null], contextMenuClasses: string[], submenuIndicator: {src: string, width: number, height: number}}}
 */
export const contextMenuOptions = (visualizationId, loading, processActionResult, timelineClustering) => {
  const menuItemWithFetch = (onClickFunction) => async (event) => {
    loading(true);
    try {
      const { fetchedCount, addedCount } = await onClickFunction(event);
      processActionResult({
        severity: "success",
        message: `Added ${addedCount} new nodes (${fetchedCount} were fetched).`
      })
    } catch (error) {
      console.error(error);
      processActionResult({
        severity: "error",
        message: error.response.data.message
      });
    } finally {
      loading(false);
    }
  }

  const clusterizeNone = { mode: 'nonde' };
  const clusterizeAsSingleCluster = { mode: 'singleCluster' };
  const clusterizeTimeline = {
    mode: 'timeline',
    ...timelineClustering
  }

  return ({
    // Customize event to bring up the context menu
    // Possible options https://js.cytoscape.org/#events/user-input-device-events
    evtType: 'cxttap',
    // List of initial menu items
    // A menu item must have either onClickFunction or submenu or both
    menuItems: [
      {
        id: 'select-all',
        content: 'Select all',
        coreAsWell: true,
        show: true,
        onClickFunction: function (event) {
          event.cy.nodes().select();
        }
      },
      {
        id: 'select-neighbourhood',
        content: 'Select neighbours',
        selector: 'node',
        coreAsWell: false,
        onClickFunction: function (event) {
          const { cy, target } = event;
          if (handleTargetNodeOnly(cy, target)) {
            target.neighborhood().select();
          } else {
            cy.nodes(':selected').neighborhood().select();
          }
        },
        submenu: [
          {
            id: 'select-neighbourhood-all',
            content: 'all',
            selector: 'node',
            coreAsWell: false,
            onClickFunction: function (event) {
              const { cy, target } = event;
              if (handleTargetNodeOnly(cy, target)) {
                target.neighborhood().select();
              } else {
                cy.nodes(':selected').neighborhood().select();
              }
            }
          },
          {
            id: 'select-neighbourhood-connections',
            content: 'connections',
            selector: 'node',
            coreAsWell: false,
            onClickFunction: function (event) {
              selectByTypes(event, ['Connection']);
            },
          },
          {
            id: 'select-neighbourhood-hosts',
            content: 'hosts',
            selector: 'node',
            coreAsWell: false,
            onClickFunction: function (event) {
              selectByTypes(event, ['Host'])
            },
          },
          {
            id: 'select-neighbourhood-host-data',
            content: 'host data',
            selector: 'node',
            coreAsWell: false,
            onClickFunction: function (event) {
              selectByTypes(event, hostDataTypes)
            },
          },
          {
            id: 'select-neighbourhood-application',
            content: 'application data',
            selector: 'node',
            coreAsWell: false,
            onClickFunction: function (event) {
              selectByTypes(event, applicationTypes);
            },
          },
          {
            id: 'select-neighbourhood-ioc',
            content: 'ioc data',
            selector: 'node',
            coreAsWell: false,
            onClickFunction: function (event) {
              selectByTypes(event, iocTypes);
            },
          },
        ]
      },
      {
        id: 'unselect-neighbourhood',
        content: 'Unselect neighbours',
        selector: 'node',
        coreAsWell: false,
        onClickFunction: function (event) {
          event.target.neighborhood().unselect();
        },
      },
      {
        id: 'open-cluster',
        content: 'Open cluster',
        selector: 'node',
        coreAsWell: true,
        onClickFunction: function (event) {
          const { target, cy } = event;
          cy.openClusterById(visualizationId, target.data('id'));
        },
      },
      {
        id: 'open-clusters',
        content: 'Open clusters',
        selector: 'node',
        coreAsWell: true,
        onClickFunction: function (event) {
          const { cy } = event;
          cy.nodes(':selected')
            .filter(node => node.data('cluster'))
            .forEach(node => cy.openClusterById(visualizationId, node.id()));
        },
      },
      {
        id: 'get-node-attributes',
        content: 'Fetch all attributes',
        selector: 'node',
        coreAsWell: true,
        onClickFunction: menuItemWithFetch(getNodeAttributesMenuFunction)
      },
      {
        id: 'get-neighbours',
        content: 'Fetch neighbours',
        selector: 'node',
        coreAsWell: true,
        submenu: [
          {
            id: 'get-neighbours-all-single-cluster',
            content: 'all types',
            selector: 'node',
            coreAsWell: true,
            onClickFunction: menuItemWithFetch(getAllNeighbours(clusterizeAsSingleCluster)),
            submenu: [
              {
                id: 'get-neighbours-all-timeline-clustering',
                content: 'timeline clustering',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getAllNeighbours(clusterizeTimeline)),
              },
              {
                id: 'get-neighbours-all-single-cluster',
                content: 'as single cluster',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getAllNeighbours(clusterizeAsSingleCluster)),
              },
              {
                id: 'get-neighbours-all-no-clustering',
                content: 'no clustering',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getAllNeighbours(clusterizeNone)),
              }
            ]
          },
          {
            id: 'get-neighbours-connection',
            content: 'connections',
            selector: 'node',
            coreAsWell: true,
            onClickFunction: menuItemWithFetch(getConnectionNeighbours(clusterizeAsSingleCluster)),
            submenu: [
              {
                id: 'get-neighbours-connection-timeline-clustering',
                content: 'timeline clustering',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getConnectionNeighbours(clusterizeTimeline)),
              },
              {
                id: 'get-neighbours-connection-single-cluster',
                content: 'as single cluster',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getConnectionNeighbours(clusterizeAsSingleCluster)),
              },
              {
                id: 'get-neighbours-connection-base-no-clustering',
                content: 'no clustering',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getConnectionNeighbours(clusterizeNone)),
              },
            ]
          },
          {
            id: 'get-neighbours-host',
            content: 'hosts',
            selector: 'node',
            coreAsWell: true,
            onClickFunction: menuItemWithFetch(getHostNeighbours(clusterizeAsSingleCluster)),
            submenu: [
              {
                id: 'get-neighbours-host-single-cluster',
                content: 'as single cluster',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getHostNeighbours(clusterizeAsSingleCluster)),
              },
              {
                id: 'get-neighbours-host-no-clustering',
                content: 'no clustering',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getHostNeighbours(clusterizeNone)),
              }
            ]
          },
          {
            id: 'get-neighbours-host-data',
            content: 'host data',
            selector: 'node',
            coreAsWell: true,
            onClickFunction: menuItemWithFetch(getHostDataNeighbours(clusterizeAsSingleCluster)),
            submenu: [
              {
                id: 'get-neighbours-host-data-single-cluster',
                content: 'as single cluster',
                selector: 'node',
                onClickFunction: menuItemWithFetch(getHostDataNeighbours(clusterizeAsSingleCluster)),
              },
              {
                id: 'get-neighbours-host-data-no-clustering',
                content: 'no clustering',
                selector: 'node',
                onClickFunction: menuItemWithFetch(getHostDataNeighbours(clusterizeNone)),
              }
            ]
          },
          {
            id: 'get-neighbours-application',
            content: 'application data',
            selector: 'node',
            coreAsWell: true,
            onClickFunction: menuItemWithFetch(getApplicationNeighbours(clusterizeAsSingleCluster)),
            submenu: [
              {
                id: 'get-neighbours-application-single-cluster',
                content: 'as single cluster',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getApplicationNeighbours(clusterizeAsSingleCluster)),
              },
              {
                id: 'get-neighbours-application-no-clustering',
                content: 'no clustering',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getApplicationNeighbours(clusterizeNone)),
              }
            ]
          },
          {
            id: 'get-neighbours-ioc',
            content: 'ioc data',
            selector: 'node',
            coreAsWell: true,
            onClickFunction: menuItemWithFetch(getIocNeighbours(clusterizeAsSingleCluster)),
            submenu: [
              {
                id: 'get-neighbours-ioc-single-cluster',
                content: 'as single cluster',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getIocNeighbours(clusterizeAsSingleCluster)),
              },
              {
                id: 'get-neighbours-ioc-no-clustering',
                content: 'no clustering',
                selector: 'node',
                coreAsWell: true,
                onClickFunction: menuItemWithFetch(getIocNeighbours(clusterizeNone)),
              }
            ]
          },
        ]
      },
    ],
    // css classes that menu items will have
    menuItemClasses: ['custom-menu-item'],
    // css classes that context menu will have
    contextMenuClasses: ['custom-context-menu'],
    // Indicates that the menu item has a submenu. If not provided default one will be used
    submenuIndicator: { src: `${process.env.PUBLIC_URL}/submenu-indicator-default.svg`, width: 12, height: 12 },
  });
}