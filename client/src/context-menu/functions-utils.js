import { parseGranefResponse } from "../parsers/parsers";

/**
 * Retruns comma-separated Granef UIDs.
 *
 * @param cy graph instance
 * @param target event target (core or a node)
 * @returns {string|*}
 */
export const getCommaSeparatedIds = (cy, target) => {
  const selectedNodes = cy.nodes(':selected');
  if (selectedNodes.size() > 0) {
    return selectedNodes.getGranefUids().join(',');
  } else if (target.data('cluster')) {
    return target.getGranefUids().join(',');
  } else {
    return target.id();
  }
}

/**
 * Returns list containing number of fetched nodes and number of added nodes.
 */
export const fetchAndUnionWithGraph = async (event, fetchFunction, clusterize) => {
  const { cy } = event;
  const response = await fetchFunction();
  const data = response.data;
  const cyElements = parseGranefResponse(data);
  const { nodes, edges } = cyElements;
  let addedNodes = [];
  if (nodes.length > 0) {
    addedNodes = await cy.unionWithGraph(nodes, edges, clusterize);
  }
  return { fetchedCount: nodes.length, addedCount: addedNodes.length };
}