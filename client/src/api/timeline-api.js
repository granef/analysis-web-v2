import axios from "axios";

export const getTimeRange = (uids) => axios.post('/granef-analysis-api/graph/uids_time_range', { uids });

export const getTimeRangeFiltered = (uids, minTimeStamp, maxTimestamp) => axios.post('/granef-analysis-api/graph/uids_timestamp_filter', {
  uids,
  timestamp_min: minTimeStamp.toISOString(),
  timestamp_max: maxTimestamp.toISOString(),
});