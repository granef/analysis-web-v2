import axios from "axios";

export const getDefaultColors = () => axios.get('/server/colors/default');
export const getColors = (visualizationId) => axios.get(`/server/colors/visualization/${visualizationId}`);
export const createColor = (color) => axios.post('/server/colors', color);
export const deleteColor = (id) => axios.delete(`/server/colors/${id}`);

export const getDefaultTags = () => axios.get('/server/tags/default');
export const getTags = (visualizationId) => axios.get(`/server/tags/visualization/${visualizationId}`);
export const createTag = (tag) => axios.post('/server/tags', tag);
export const deleteTag = (id) => axios.delete(`/server/tags/${id}`);

export const getNodeDetail = (visualizationId, nodeId) => axios.get(`/server/nodes/${nodeId}/visualization/${visualizationId}`);
export const assignTag = (tagId, nodeIds) => axios.put(`/server/nodes/tag/${tagId}`, nodeIds);
export const removeNodes = (visualizationId, nodeIds) => axios.delete(`/server/nodes/visualization/${visualizationId}`, {
  data: nodeIds
});
export const removeNodesForVisualization = (visualizationId) => axios.delete(`/server/nodes/visualization/${visualizationId}/all`);
export const unassignTag = (tagId, nodeIds) => axios.delete(`/server/nodes/tag/${tagId}`, {
  data: nodeIds
});
export const filterNodes = (tagIds) => axios.post(`/server/nodes/find-tagged`, tagIds);

/**
 * Retrieves all tagged nodes saved for visualization with given ID.
 *
 * @param visualizationId
 * @returns {Promise<AxiosResponse<any>>}
 */
export const getVisualizationNodesDetails = (visualizationId) => axios.get(`/server/nodes/visualization/${visualizationId}/all`)
