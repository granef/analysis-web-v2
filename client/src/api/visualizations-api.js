import axios from "axios";

const URL_BASE = '/server/visualizations'

export const findAllVisualizations = () => axios.get(URL_BASE);
export const findVisualization = (id) => axios.get(`${URL_BASE}/${id}`);

export const createVisualization = (visualization) => axios.post(URL_BASE, visualization);
export const updateVisualization = (visualization) => axios.put(URL_BASE, visualization);
export const deleteVisualization = (id) => axios.delete(`${URL_BASE}/${id}`);

export const getDefaultVisualizationPreferences = () => axios.get(`${URL_BASE}/preferences`);
export const updateDefaultVisualizationPreferences = (preferences) => axios.patch(`${URL_BASE}/preferences`, preferences);

export const getVisualizationPreferences = (id) => axios.get(`${URL_BASE}/${id}/preferences`);
export const updateVisualizationPreferences = (id, preferences) => axios.patch(`${URL_BASE}/${id}/preferences`, preferences);

export const getVisualizationState = (id) => axios.get(`${URL_BASE}/${id}/state`);
export const updateVisualizationState = (id, state) => axios.patch(`${URL_BASE}/${id}/state`, state);