import axios from "axios";

export const getCustomQuery = (query) => axios.post('/granef-analysis-api/custom_query', {
  query: query
});

export const lookForXss = () => axios.post('/granef-analysis-api/custom_query', {
  query: "{lookForXss(func: regexp(http.uri, /<script>/)) @cascade { http.uri, ..., ~connection.produced { connection.ts, ..., ~host.originated { host.ip, attacker : host.ip }, ~host.responded { host.ip, victim : host.ip }}}}"
});

export const httpRegexpSearch = () => axios.post('/granef-analysis-api/custom_query', {
  query: "{httpRegexpSearch(func: regexp(http.uri, /select+|union+|where+|from+/)) @cascade { http.uri, http.method, ~connection.produced { connection.ts, connection.resp_bytes, ..., ~host.originated { host.ip }, ~host.responded { host.ip }}}}"
});

export const getConnections = (originatorAddress, responderAddress, from, to) => axios.post('/granef-analysis-api/analysis/connections_search', {
  "address_orig": originatorAddress,
  "address_resp": responderAddress,
  "timestamp_min": from ? from+":00" : undefined,
  "timestamp_max": to ? to+":00" : undefined,
});

export const getNodeAttributes = (uids) => axios.post('/granef-analysis-api/graph/node_attributes', { uids });

export const getAttributeSearch = (attribute, value) => axios.post('/granef-analysis-api/graph/attribute_search', {
  attribute,
  value
});

export const getNeighbours = (uids, types) => axios.post('/granef-analysis-api/graph/neighbors', { uids, types });

export const getHostInfo = (address) => axios.post('/granef-analysis-api/overview/hosts_info', { address });

export const getConnectionsFromSubnet = (address) => axios.post('/granef-analysis-api/overview/connections_from_subnet', { address });

export const getStatistics = (uids) => axios.post('/granef-analysis-api/overview/cluster_statistics', { uids });

export const getAdjacencyMatrix = (uids) => axios.post('/granef-analysis-api/overview/adjacency_matrix', { uids });