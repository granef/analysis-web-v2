import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Color } from './color.entity';
import { Visualization } from '../visualizations/visualization.entity';

@Injectable()
export class ColorsService {
  constructor(
    @InjectRepository(Color)
    private colorsRepository: Repository<Color>,
  ) {}

  findAll(): Promise<Color[]> {
    return this.colorsRepository.find();
  }

  findAllDefault(): Promise<Color[]> {
    return this.colorsRepository
      .createQueryBuilder('color')
      .orWhere('color.visualization_id IS NULL')
      .orderBy('color.label')
      .getMany();
  }

  findAllForVisualization(visualizationId: string): Promise<Color[]> {
    return this.colorsRepository
      .createQueryBuilder('color')
      .where('color.visualization_id = :visualizationId', { visualizationId })
      .orderBy('color.label')
      .getMany();
  }

  findOne(id: string): Promise<Color> {
    return this.colorsRepository.findOne(id);
  }

  async create(color: Color): Promise<Color> {
    return this.colorsRepository.save(color);
  }

  async update(color: Color): Promise<Color> {
    return this.colorsRepository.save(color);
  }

  async remove(id: string): Promise<void> {
    await this.colorsRepository.delete(id);
  }

  async createDefaultColors(visualization: Visualization): Promise<Color[]> {
    const defaultColors = await this.findAllDefault();

    const visualizationColors = defaultColors.map(defaultColor => ({
      hex: defaultColor.hex,
      label: defaultColor.label,
      visualization: visualization
    }));

    return this.colorsRepository.save(visualizationColors);
  }
}
