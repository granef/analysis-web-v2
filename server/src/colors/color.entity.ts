import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Visualization } from '../visualizations/visualization.entity';
import { IColor } from './color.interface';

/**
 * Named color
 */
@Entity('color')
export class Color implements IColor {
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * Hexadecimal code
   */
  @Column({
    nullable: false,
  })
  hex: string;

  /**
   * Name
   */
  @Column({
    type: 'text',
    nullable: false,
  })
  label: string;

  /**
   * Visualisation to which the color belongs. If `null`, it will be included in default list of colors
   * assigned to newly created visualisation projects.
   */
  @ManyToOne(
    () => Visualization,
    (visualization) => visualization.tags,
    { nullable: true, onDelete: 'CASCADE' }
  )
  @JoinColumn({
    name: 'visualization_id',
  })
  visualization: Visualization;
}
