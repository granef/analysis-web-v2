import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Color } from './color.entity';
import { ColorsService } from './colors.service';

@Controller('colors')
export class ColorsController {
  constructor(private service: ColorsService) {}

  @Get('/default')
  async findAllDefault(): Promise<Color[]> {
    return this.service.findAllDefault();
  }

  @Get('/visualization/:visualizationId')
  async findAllForVisualization(
    @Param('visualizationId') visualizationId: string,
  ): Promise<Color[]> {
    return this.service.findAllForVisualization(visualizationId);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Color> {
    return this.service.findOne(id);
  }

  @Post()
  async create(@Body() color: Color): Promise<Color> {
    return this.service.create(color);
  }

  @Put()
  async update(@Body() color: Color): Promise<Color> {
    return this.service.update(color);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.service.remove(id);
  }
}
