import { Visualization } from '../visualizations/visualization.entity';

export interface IColor {
  label: string;
  hex: string;
  visualization?: Visualization;
}