import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { TagsService } from './tags.service';
import { Tag } from './tag.entity';

@Controller('tags')
export class TagsController {
  constructor(private service: TagsService) {}

  @Get('/default')
  async findAllDefault(): Promise<Tag[]> {
    return this.service.findAllDefault();
  }

  @Get('/visualization/:visualizationId')
  async findAllForVisualization(
    @Param('visualizationId') visualizationId: string,
  ): Promise<Tag[]> {
    return this.service.findAllForVisualization(visualizationId);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Tag> {
    return this.service.findOne(id);
  }

  @Post()
  async create(@Body() tag: Tag): Promise<Tag> {
    return this.service.create(tag);
  }

  @Put()
  async update(@Body() tag: Tag): Promise<Tag> {
    return this.service.update(tag);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.service.remove(id);
  }

}
