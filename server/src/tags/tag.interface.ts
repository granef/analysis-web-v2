import { Visualization } from '../visualizations/visualization.entity';

export interface ITag {
  label: string;
  hex: string;
  visualization?: Visualization;
}