import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Tag } from './tag.entity';
import { Visualization } from '../visualizations/visualization.entity';

@Injectable()
export class TagsService {
  constructor(
    @InjectRepository(Tag)
    private tagsRepository: Repository<Tag>,
    @InjectRepository(Visualization)
    private visualizationRepository: Repository<Visualization>,
  ) {
  }

  findAll(): Promise<Tag[]> {
    return this.tagsRepository.find();
  }

  findAllDefault(): Promise<Tag[]> {
    return this.tagsRepository
      .createQueryBuilder('tag')
      .orWhere('tag.visualization_id IS NULL')
      .orderBy('tag.label')
      .getMany();
  }

  findAllForVisualization(visualizationId: string): Promise<Tag[]> {
    return this.tagsRepository
      .createQueryBuilder('tag')
      .where('tag.visualization_id = :visualizationId', { visualizationId })
      .orderBy('tag.label')
      .getMany();
  }

  findOne(id: string): Promise<Tag> {
    return this.tagsRepository.findOneOrFail(id);
  }

  async create(tag: Tag): Promise<Tag> {
    return this.tagsRepository.save(tag);
  }

  async update(tag: Tag): Promise<Tag> {
    return this.tagsRepository.save(tag);
  }

  async remove(id: string): Promise<void> {
    await this.tagsRepository.delete(id);
  }

  async createDefaultTags(visualization: Visualization): Promise<Tag[]> {
    const defaultTags = await this.findAllDefault();

    const visualizationTags = defaultTags.map(defaultTag => ({
      hex: defaultTag.hex,
      label: defaultTag.label,
      visualization: visualization,
    }));

    return this.tagsRepository.save(visualizationTags);
  }
}
