import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Visualization } from '../visualizations/visualization.entity';
import { ITag } from './tag.interface';

@Entity('tag')
export class Tag implements ITag {
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * Hexadecimal code
   */
  @Column({
    nullable: false,
  })
  hex: string;

  /**
   * Name
   */
  @Column({
    type: 'text',
    nullable: false,
  })
  label: string;

  /**
   * Visualisation to which the color belongs. If `null`, it will be included in default list of tags
   * assigned to newly created visualisation projects.
   */
  @ManyToOne(
    () => Visualization,
    (visualization) => visualization.tags,
    { nullable: true, onDelete: 'CASCADE'}
  )
  @JoinColumn({
    name: 'visualization_id',
  })
  visualization: Visualization;
}
