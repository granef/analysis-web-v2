import { InjectRepository } from '@nestjs/typeorm';
import { Tag } from '../tags/tag.entity';
import { DeleteResult, Repository } from 'typeorm';
import { Node } from './node.entity';
import { Visualization } from '../visualizations/visualization.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
export class NodesService {
  constructor(
    @InjectRepository(Tag)
    private tagsRepository: Repository<Tag>,
    @InjectRepository(Node)
    private nodesRepository: Repository<Node>,
    @InjectRepository(Visualization)
    private visualizationRepository: Repository<Visualization>,
  ) {
  }

  findNode(visualizationId: number, nodeId: string): Promise<Node> {
    return this.nodesRepository.createQueryBuilder('node')
      .leftJoinAndSelect('node.tags', 'tag')
      .leftJoinAndSelect('node.visualization', 'visualization')
      .whereInIds(this.getNodeId(visualizationId, nodeId))
      .getOne();
  }

  findNodes(visualizationId: number): Promise<Node[]> {
    return this.nodesRepository.createQueryBuilder('node')
      .loadAllRelationIds({
        relations: ['tags'],
      })
      .where('node.visualization_id = :visualizationId', { visualizationId })
      .getMany();
  }

  findAllForVisualization(visualizationId: number): Promise<Node[]> {
    return this.nodesRepository
      .createQueryBuilder('node')
      .loadAllRelationIds({
        relations: ['tags'],
        disableMixedMap: true,
      })
      .where('node.visualization_id = :visualizationId', { visualizationId })
      .getMany();
  }

  findTagged(tagIds: number[]): Promise<Node[]> {
    return this.nodesRepository.createQueryBuilder('node')
      .innerJoinAndSelect('node.tags', 'tag', `tag.id IN (:...tagIds)`, { tagIds })
      .select('granef_id', 'granefId')
      .distinct(true)
      .getRawMany();
  }

  async remove(visualizationId: number, nodeIds: string[]): Promise<DeleteResult> {
    return this.nodesRepository.createQueryBuilder()
      .delete()
      .whereInIds(this.getNodeIds(visualizationId, nodeIds))
      .execute();
  }

  async removeAllForVisualization(visualizationId: number): Promise<DeleteResult> {
    return this.nodesRepository.createQueryBuilder()
      .delete()
      .where('node.visualization_id = :visualizationId', { visualizationId })
      .execute();
  }

  /**
   * Assigns tag with given ID to nodes with ID in given list of granef UIDs. If a node with given Granef UID
   * in tag's visualization exists, it is updated if it has not been tagged with the tag yet. If a node with given
   * Granef UID does not exist yet, it is created and tagged with the desired tag.
   *
   * @param tagId ID of a tag
   * @param nodeIds list of Granef UIDs
   */
  async assignToMany(tagId: number, nodeIds: string[]): Promise<string[]> {
    const tag = await this.tagsRepository.findOneOrFail(tagId, {
      loadRelationIds: {
        relations: ['visualization'],
        disableMixedMap: true
      },
    });

    if (!tag.visualization) {
      throw new Error(`Cannot assign default Tag@${tagId}.`);
    }
    const visualizationId = tag.visualization.id;

    const existingNodes = await this.nodesRepository.findByIds(this.getNodeIds(visualizationId, nodeIds), {
      loadRelationIds: {
        relations: ['visualization', 'tags'],
        disableMixedMap: true,
      },
      where: {
        visualization: {
          id: visualizationId,
        },
      },
    });

    const existingNodesNotTagged = existingNodes.filter(node => !node.tags.map(tag => tag.id).includes(tag.id));
    existingNodesNotTagged.forEach(node => node.tags.push(tag));

    const existingIds = existingNodes.map(node => node.granefId);
    const newIds = nodeIds.filter(id => !existingIds.includes(id));
    const newNodes = newIds.map(id => ({
      granefId: id,
      visualization: tag.visualization,
      tags: [tag],
    }));

    const entities = [
      ...existingNodesNotTagged,
      ...newNodes,
    ];
    const saved = await this.nodesRepository.save(entities);
    return saved.map(node => node.granefId);
  }

  async unassignFromMany(tagId: number, nodeIds: string[]) {
    const tag = await this.tagsRepository.findOneOrFail(tagId, {
      loadRelationIds: {
        relations: ['visualization'],
        disableMixedMap: true
      },
    });

    if (!tag.visualization) {
      throw new Error(`Cannot unassign default Tag@${tagId}.`);
    }
    const visualizationId = tag.visualization.id;

    const taggedNodes = await this.nodesRepository.createQueryBuilder('node')
      .loadAllRelationIds({
        relations: ['visualization', 'tags'],
        disableMixedMap: true,
      })
      .innerJoin('node.tags', 'tag', `tag.id = :tagId`, { tagId })
      .whereInIds(this.getNodeIds(visualizationId, nodeIds))
      .getMany();
    taggedNodes.forEach(taggedNode => {
      taggedNode.tags = taggedNode.tags.filter(tag => tag.id !== tagId);
    })
    const saved = await this.nodesRepository.save(taggedNodes);
    return saved.map(node => node.granefId);
  }

  private getNodeIds(visualizationId: number, granefUids: string[]) {
    const getIdForVisualization = (uid) => this.getNodeId(visualizationId, uid);
    return granefUids.map(getIdForVisualization);
  }

  private getNodeId(visualizationId: number, granefUid: string) {
    return {
      granefId: granefUid,
      visualization: {
        id: visualizationId
      }
    }
  }
}