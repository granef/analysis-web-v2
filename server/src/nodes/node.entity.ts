import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import { Visualization } from '../visualizations/visualization.entity';
import { Tag } from '../tags/tag.entity';

/**
 * Entity representing a node for storing information about assigned annotations.
 */
@Entity('node')
@Unique("UQ_VISUALIZATION_NODE", ["granefId", "visualization"])
export class Node {

  @PrimaryColumn({
    name: 'granef_id',
    nullable: false
  })
  granefId: string;

  @ManyToOne(
    () => Visualization,
    (visualization) => visualization.tags,
    { nullable: true, primary: true, onDelete: 'CASCADE' }
  )
  @JoinColumn({
    name: 'visualization_id',
  })
  visualization: Visualization;

  @ManyToMany(() => Tag)
  @JoinTable()
  tags: Tag[];

}