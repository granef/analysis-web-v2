import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Node } from './node.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Node])],
  exports: [TypeOrmModule],
})
export class NodesModule {}
