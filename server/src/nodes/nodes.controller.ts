import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { NodesService } from './nodes.service';
import { Node } from './node.entity';

@Controller('nodes')
export class NodesController {
  constructor(private service: NodesService) {
  }

  @Get('/:nodeId/visualization/:visualizationId/')
  async findNode(
    @Param('visualizationId', ParseIntPipe) visualizationId: number,
    @Param('nodeId') nodeId: string
  ): Promise<Node> {
    return this.service.findNode(visualizationId, nodeId);
  }

  @Get('/visualization/:visualizationId/all')
  async findNodes(
    @Param('visualizationId', ParseIntPipe) visualizationId: number,
  ): Promise<Node[]> {
    return this.service.findAllForVisualization(visualizationId);
  }

  @Delete('/visualization/:visualizationId')
  async remove(
    @Param('visualizationId', ParseIntPipe) visualizationId: number,
    @Body() ids: string[]
  ) {
    return this.service.remove(visualizationId, ids);
  }

  @Delete('/visualization/:visualizationId/all')
  async removeAllForVisualization(
    @Param('visualizationId', ParseIntPipe) visualizationId: number
  ) {
    return this.service.removeAllForVisualization(visualizationId);
  }

  @Put('/tag/:tagId')
  assignToMany(
    @Param('tagId', ParseIntPipe) tagId: number,
    @Body() nodeIds: string[],
  ) {
    return this.service.assignToMany(tagId, nodeIds);
  }

  @Delete('/tag/:tagId')
  unassignFromMany(
    @Param('tagId', ParseIntPipe) tagId: number,
    @Body() nodeIds: string[],
  ) {
    return this.service.unassignFromMany(tagId, nodeIds);
  }

  @Post('/find-tagged')
  async findTagged(
    @Body() tagIds: number[],
  ): Promise<Node[]> {
    return this.service.findTagged(tagIds);
  }
}
