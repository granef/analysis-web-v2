import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ColorsModule } from './colors/colors.module';
import { ColorsController } from './colors/colors.controller';
import { ColorsService } from './colors/colors.service';
import { NotesModule } from './notes/notes.module';
import { TagsModule } from './tags/tags.module';
import { VisualizationsModule } from './visualizations/visualizations.module';
import { TagsController } from './tags/tags.controller';
import { VisualizationsController } from './visualizations/visualizations.controller';
import { TagsService } from './tags/tags.service';
import { NotesService } from './notes/notes.service';
import { VisualizationsService } from './visualizations/visualizations.service';
import { SeedService } from './seed/seed.service';
import { PreferencesModule } from './preferences/preferences.module';
import { PreferencesService } from './preferences/preferences.service';
import { StateService } from './visualizations/state/state.service';
import { NodesModule } from './nodes/nodes.module';
import { NodesController } from './nodes/nodes.controller';
import { NodesService } from './nodes/nodes.service';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    ColorsModule,
    NotesModule,
    TagsModule,
    NodesModule,
    VisualizationsModule,
    PreferencesModule,
  ],
  controllers: [
    AppController,
    ColorsController,
    TagsController,
    VisualizationsController,
    NodesController
  ],
  providers: [
    AppService,
    ColorsService,
    NotesService,
    TagsService,
    VisualizationsService,
    SeedService,
    PreferencesService,
    StateService,
    NodesService
  ],
})
export class AppModule {}
