import {MigrationInterface, QueryRunner} from "typeorm";

export class TaggedNode1647461865881 implements MigrationInterface {
    name = 'TaggedNode1647461865881'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "TaggedNode" ("granef_id" character varying NOT NULL, "visualization_id" integer NOT NULL, CONSTRAINT "UQ_VISUALIZATION_NODE" UNIQUE ("granef_id", "visualization_id"), CONSTRAINT "PK_f753191bdebad525a201b371652" PRIMARY KEY ("granef_id", "visualization_id"))`);
        await queryRunner.query(`CREATE TABLE "tagged_node_tags_tag" ("taggedNodeGranefId" character varying NOT NULL, "taggedNodeVisualizationId" integer NOT NULL, "tagId" integer NOT NULL, CONSTRAINT "PK_8f8d15fdd66ac70a9b7ebf4350e" PRIMARY KEY ("taggedNodeGranefId", "taggedNodeVisualizationId", "tagId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_b98f25138506fc742f6e6ac82c" ON "tagged_node_tags_tag" ("taggedNodeGranefId", "taggedNodeVisualizationId") `);
        await queryRunner.query(`CREATE INDEX "IDX_b42db0196b4857e7b4f977ef7d" ON "tagged_node_tags_tag" ("tagId") `);
        await queryRunner.query(`ALTER TABLE "state" DROP COLUMN "elements"`);
        await queryRunner.query(`ALTER TABLE "state" ADD "elements" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "TaggedNode" ADD CONSTRAINT "FK_903e37b9c105dd48937e92c3094" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tagged_node_tags_tag" ADD CONSTRAINT "FK_b98f25138506fc742f6e6ac82c2" FOREIGN KEY ("taggedNodeGranefId", "taggedNodeVisualizationId") REFERENCES "TaggedNode"("granef_id","visualization_id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "tagged_node_tags_tag" ADD CONSTRAINT "FK_b42db0196b4857e7b4f977ef7d3" FOREIGN KEY ("tagId") REFERENCES "tag"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "tagged_node_tags_tag" DROP CONSTRAINT "FK_b42db0196b4857e7b4f977ef7d3"`);
        await queryRunner.query(`ALTER TABLE "tagged_node_tags_tag" DROP CONSTRAINT "FK_b98f25138506fc742f6e6ac82c2"`);
        await queryRunner.query(`ALTER TABLE "TaggedNode" DROP CONSTRAINT "FK_903e37b9c105dd48937e92c3094"`);
        await queryRunner.query(`ALTER TABLE "state" DROP COLUMN "elements"`);
        await queryRunner.query(`ALTER TABLE "state" ADD "elements" character varying NOT NULL`);
        await queryRunner.query(`DROP INDEX "public"."IDX_b42db0196b4857e7b4f977ef7d"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_b98f25138506fc742f6e6ac82c"`);
        await queryRunner.query(`DROP TABLE "tagged_node_tags_tag"`);
        await queryRunner.query(`DROP TABLE "TaggedNode"`);
    }

}
