import {MigrationInterface, QueryRunner} from "typeorm";

export class CascadeDeleteVisualizationTagsAndColors1647476199355 implements MigrationInterface {
    name = 'CascadeDeleteVisualizationTagsAndColors1647476199355'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "tag" DROP CONSTRAINT "FK_cde59f3dd8d4f6d653f159c7a46"`);
        await queryRunner.query(`ALTER TABLE "color" DROP CONSTRAINT "FK_1cbe8c45f9185fd047f674cac30"`);
        await queryRunner.query(`ALTER TABLE "node" ADD CONSTRAINT "UQ_VISUALIZATION_NODE" UNIQUE ("granef_id", "visualization_id")`);
        await queryRunner.query(`ALTER TABLE "tag" ADD CONSTRAINT "FK_cde59f3dd8d4f6d653f159c7a46" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "color" ADD CONSTRAINT "FK_1cbe8c45f9185fd047f674cac30" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "color" DROP CONSTRAINT "FK_1cbe8c45f9185fd047f674cac30"`);
        await queryRunner.query(`ALTER TABLE "tag" DROP CONSTRAINT "FK_cde59f3dd8d4f6d653f159c7a46"`);
        await queryRunner.query(`ALTER TABLE "node" DROP CONSTRAINT "UQ_VISUALIZATION_NODE"`);
        await queryRunner.query(`ALTER TABLE "color" ADD CONSTRAINT "FK_1cbe8c45f9185fd047f674cac30" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "tag" ADD CONSTRAINT "FK_cde59f3dd8d4f6d653f159c7a46" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
