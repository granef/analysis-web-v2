import {MigrationInterface, QueryRunner} from "typeorm";

export class CascadeDeleteVisualizationPreferencesAndState1647476693489 implements MigrationInterface {
    name = 'CascadeDeleteVisualizationPreferencesAndState1647476693489'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "preferences" DROP CONSTRAINT "FK_d455eb0c41045a56a2d3aa8f14c"`);
        await queryRunner.query(`ALTER TABLE "state" DROP CONSTRAINT "FK_0597c8261e817bcbd14376349bc"`);
        await queryRunner.query(`ALTER TABLE "preferences" ADD CONSTRAINT "FK_d455eb0c41045a56a2d3aa8f14c" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "state" ADD CONSTRAINT "FK_0597c8261e817bcbd14376349bc" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "state" DROP CONSTRAINT "FK_0597c8261e817bcbd14376349bc"`);
        await queryRunner.query(`ALTER TABLE "preferences" DROP CONSTRAINT "FK_d455eb0c41045a56a2d3aa8f14c"`);
        await queryRunner.query(`ALTER TABLE "state" ADD CONSTRAINT "FK_0597c8261e817bcbd14376349bc" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "preferences" ADD CONSTRAINT "FK_d455eb0c41045a56a2d3aa8f14c" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
