import {MigrationInterface, QueryRunner} from "typeorm";

export class VisualizationNote1647184732654 implements MigrationInterface {
    name = 'VisualizationNote1647184732654'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "visualization" ADD "note" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "visualization" DROP COLUMN "note"`);
    }

}
