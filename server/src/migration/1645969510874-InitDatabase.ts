import {MigrationInterface, QueryRunner} from "typeorm";

export class InitDatabase1645969510874 implements MigrationInterface {
    name = 'InitDatabase1645969510874'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "tag" ("id" SERIAL NOT NULL, "hex" character varying NOT NULL, "label" text NOT NULL, "visualization_id" integer, CONSTRAINT "PK_8e4052373c579afc1471f526760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "visualization" ("id" SERIAL NOT NULL, "created" TIMESTAMP NOT NULL DEFAULT now(), "updated" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "description" character varying, CONSTRAINT "PK_0cbc54139ff1d8afe1b75daf118" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "color" ("id" SERIAL NOT NULL, "hex" character varying NOT NULL, "label" text NOT NULL, "visualization_id" integer, CONSTRAINT "PK_d15e531d60a550fbf23e1832343" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "note" ("id" SERIAL NOT NULL, "content" text NOT NULL, "node_id" character varying NOT NULL, CONSTRAINT "PK_96d0c172a4fba276b1bbed43058" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "preferences" ("id" SERIAL NOT NULL, "selection" character varying NOT NULL, "selectionType" character varying NOT NULL, "layout" text NOT NULL, "visualization_id" integer, "styleNodeborders" boolean NOT NULL, "styleNodelabels" boolean NOT NULL, "styleEdgearrows" boolean NOT NULL, "styleEdgelabels" boolean NOT NULL, CONSTRAINT "PK_17f8855e4145192bbabd91a51be" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "state" ("id" SERIAL NOT NULL, "elements" character varying NOT NULL, "scratch" text NOT NULL, "visualization_id" integer, "viewportZoom" numeric NOT NULL, "viewportPanX" integer NOT NULL, "viewportPanY" integer NOT NULL, CONSTRAINT "PK_549ffd046ebab1336c3a8030a12" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "tag" ADD CONSTRAINT "FK_cde59f3dd8d4f6d653f159c7a46" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "color" ADD CONSTRAINT "FK_1cbe8c45f9185fd047f674cac30" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "preferences" ADD CONSTRAINT "FK_d455eb0c41045a56a2d3aa8f14c" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "state" ADD CONSTRAINT "FK_0597c8261e817bcbd14376349bc" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "state" DROP CONSTRAINT "FK_0597c8261e817bcbd14376349bc"`);
        await queryRunner.query(`ALTER TABLE "preferences" DROP CONSTRAINT "FK_d455eb0c41045a56a2d3aa8f14c"`);
        await queryRunner.query(`ALTER TABLE "color" DROP CONSTRAINT "FK_1cbe8c45f9185fd047f674cac30"`);
        await queryRunner.query(`ALTER TABLE "tag" DROP CONSTRAINT "FK_cde59f3dd8d4f6d653f159c7a46"`);
        await queryRunner.query(`DROP TABLE "state"`);
        await queryRunner.query(`DROP TABLE "preferences"`);
        await queryRunner.query(`DROP TABLE "note"`);
        await queryRunner.query(`DROP TABLE "color"`);
        await queryRunner.query(`DROP TABLE "visualization"`);
        await queryRunner.query(`DROP TABLE "tag"`);
    }

}
