import {MigrationInterface, QueryRunner} from "typeorm";

export class FixPanTypes1646606595104 implements MigrationInterface {
    name = 'FixPanTypes1646606595104'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "state" DROP COLUMN "viewportPanX"`);
        await queryRunner.query(`ALTER TABLE "state" ADD "viewportPanX" numeric NOT NULL`);
        await queryRunner.query(`ALTER TABLE "state" DROP COLUMN "viewportPanY"`);
        await queryRunner.query(`ALTER TABLE "state" ADD "viewportPanY" numeric NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "state" DROP COLUMN "viewportPanY"`);
        await queryRunner.query(`ALTER TABLE "state" ADD "viewportPanY" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "state" DROP COLUMN "viewportPanX"`);
        await queryRunner.query(`ALTER TABLE "state" ADD "viewportPanX" integer NOT NULL`);
    }

}
