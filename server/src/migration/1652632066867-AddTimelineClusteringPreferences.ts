import {MigrationInterface, QueryRunner} from "typeorm";

export class AddTimelineClusteringPreferences1652632066867 implements MigrationInterface {
    name = 'AddTimelineClusteringPreferences1652632066867'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "preferences" ADD "timelineClusteringUnit" character varying`);
        await queryRunner.query(`UPDATE "preferences" SET "timelineClusteringUnit" = 'minutes' WHERE "timelineClusteringUnit" IS NULL`);
        await queryRunner.query(`ALTER TABLE "preferences" ALTER COLUMN "timelineClusteringUnit" SET NOT NULL`);

        await queryRunner.query(`ALTER TABLE "preferences" ADD "timelineClusteringValue" integer`);
        await queryRunner.query(`UPDATE "preferences" SET "timelineClusteringValue" = 5 WHERE "timelineClusteringValue" IS NULL`);
        await queryRunner.query(`ALTER TABLE "preferences" ALTER COLUMN "timelineClusteringValue" SET NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "preferences" DROP COLUMN "timelineClusteringValue"`);
        await queryRunner.query(`ALTER TABLE "preferences" DROP COLUMN "timelineClusteringUnit"`);
    }

}
