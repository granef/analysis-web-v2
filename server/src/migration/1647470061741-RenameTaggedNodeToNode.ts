import {MigrationInterface, QueryRunner} from "typeorm";

export class RenameTaggedNodeToNode1647470061741 implements MigrationInterface {
    name = 'RenameTaggedNodeToNode1647470061741'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "node" ("granef_id" character varying NOT NULL, "visualization_id" integer NOT NULL, CONSTRAINT "UQ_VISUALIZATION_NODE" UNIQUE ("granef_id", "visualization_id"), CONSTRAINT "PK_2497b216f01532f1f89a299b806" PRIMARY KEY ("granef_id", "visualization_id"))`);
        await queryRunner.query(`CREATE TABLE "node_tags_tag" ("nodeGranefId" character varying NOT NULL, "nodeVisualizationId" integer NOT NULL, "tagId" integer NOT NULL, CONSTRAINT "PK_119070500d1a0ee7ba7711bf104" PRIMARY KEY ("nodeGranefId", "nodeVisualizationId", "tagId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_6dcfa2d1ce9963742e76c71fc9" ON "node_tags_tag" ("nodeGranefId", "nodeVisualizationId") `);
        await queryRunner.query(`CREATE INDEX "IDX_2050877825e4558d76d4b21b7d" ON "node_tags_tag" ("tagId") `);
        await queryRunner.query(`ALTER TABLE "node" ADD CONSTRAINT "FK_5fa6cc18b8190de4ee7428e595f" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "node_tags_tag" ADD CONSTRAINT "FK_6dcfa2d1ce9963742e76c71fc9d" FOREIGN KEY ("nodeGranefId", "nodeVisualizationId") REFERENCES "node"("granef_id","visualization_id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "node_tags_tag" ADD CONSTRAINT "FK_2050877825e4558d76d4b21b7d1" FOREIGN KEY ("tagId") REFERENCES "tag"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "node_tags_tag" DROP CONSTRAINT "FK_2050877825e4558d76d4b21b7d1"`);
        await queryRunner.query(`ALTER TABLE "node_tags_tag" DROP CONSTRAINT "FK_6dcfa2d1ce9963742e76c71fc9d"`);
        await queryRunner.query(`ALTER TABLE "node" DROP CONSTRAINT "FK_5fa6cc18b8190de4ee7428e595f"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_2050877825e4558d76d4b21b7d"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_6dcfa2d1ce9963742e76c71fc9"`);
        await queryRunner.query(`DROP TABLE "node_tags_tag"`);
        await queryRunner.query(`DROP TABLE "node"`);
    }

}
