import {MigrationInterface, QueryRunner} from "typeorm";

export class CascadeDeleteVisualizationNodes1647476929968 implements MigrationInterface {
    name = 'CascadeDeleteVisualizationNodes1647476929968'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "node" DROP CONSTRAINT "FK_5fa6cc18b8190de4ee7428e595f"`);
        await queryRunner.query(`ALTER TABLE "node" ADD CONSTRAINT "FK_5fa6cc18b8190de4ee7428e595f" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "node" DROP CONSTRAINT "FK_5fa6cc18b8190de4ee7428e595f"`);
        await queryRunner.query(`ALTER TABLE "node" ADD CONSTRAINT "FK_5fa6cc18b8190de4ee7428e595f" FOREIGN KEY ("visualization_id") REFERENCES "visualization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
