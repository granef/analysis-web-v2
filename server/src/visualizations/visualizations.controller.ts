import {
  Body,
  Controller,
  Delete,
  Get,
  Param, ParseIntPipe, Patch,
  Post,
  Put,
} from '@nestjs/common';
import { VisualizationsService } from './visualizations.service';
import { Visualization } from './visualization.entity';
import { Preferences } from '../preferences/preferences.entity';
import { State } from './state/state.entity';
import { DeleteResult } from 'typeorm';

@Controller('visualizations')
export class VisualizationsController {
  constructor(private service: VisualizationsService) {}

  @Post()
  async create(@Body() visualization: Visualization): Promise<Visualization> {
    return this.service.create(visualization);
  }

  @Put()
  async update(@Body() visualization: Visualization): Promise<Visualization> {
    return this.service.update(visualization);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<DeleteResult> {
    return this.service.remove(id);
  }

  @Get()
  async findAll(): Promise<Visualization[]> {
    return this.service.findAll();
  }

  @Get('/preferences')
  async findDefaultPreferences(): Promise<Preferences> {
    return this.service.findPreferences(null);
  }

  @Patch('/preferences')
  async updateDefaultPreferences(@Body() preferences: Preferences): Promise<Preferences> {
    return this.service.updatePreferences(null, preferences);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Visualization> {
    return this.service.findOne(id);
  }

  @Get(':id/preferences')
  async findPreferences(@Param('id', ParseIntPipe) id: number): Promise<Preferences> {
    return this.service.findPreferences(id);
  }

  @Patch(':id/preferences')
  async updatePreferences(@Param('id', ParseIntPipe) id: number, @Body() preferences: Preferences): Promise<Preferences> {
    return this.service.updatePreferences(id, preferences);
  }

  @Get(':id/state')
  async findState(@Param('id') id: string): Promise<State> {
    return this.service.findState(id);
  }

  @Patch(':id/state')
  async updateState(@Param('id') id: string, @Body() state: State): Promise<State> {
    return this.service.updateState(id, state);
  }
}
