import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Visualization } from './visualization.entity';
import { PreferencesService } from '../preferences/preferences.service';
import { StateService } from './state/state.service';
import { Preferences } from '../preferences/preferences.entity';
import { State } from './state/state.entity';
import { IPreferences } from '../preferences/preferences.interface';
import { ColorsService } from '../colors/colors.service';
import { TagsService } from '../tags/tags.service';

@Injectable()
export class VisualizationsService {
  constructor(
    @InjectRepository(Visualization)
    private visualizationsRepository: Repository<Visualization>,
    private preferencesService: PreferencesService,
    private stateService: StateService,
    private colorsService: ColorsService,
    private tagsService: TagsService
  ) {
  }

  findAll(): Promise<Visualization[]> {
    // return this.visualizationsRepository.find();
    return this.visualizationsRepository
      .createQueryBuilder('visualization')
      .orderBy({
        'updated': 'DESC',
      })
      .getMany();
  }

  findOne(id: string | number): Promise<Visualization> {
    return this.visualizationsRepository.findOneOrFail(id);
  }

  findPreferences(id: number | null): Promise<Preferences> {
    return this.preferencesService.findForVisualization(id);
  }

  async updatePreferences(id: number | null, preferences: Preferences): Promise<Preferences> {
    if (id !== null) {
      const visualization = await this.findOne(id);
      visualization.updated = new Date();
      await this.update(visualization);
    }
    return this.preferencesService.updatePreferences(id, preferences);
  }

  findState(id: string): Promise<State> {
    return this.stateService.findForVisualization(id);
  }

  async updateState(id: string, state: State): Promise<State> {
    const visualization = await this.findOne(id);
    visualization.updated = new Date();
    await this.update(visualization);
    return this.stateService.updateState(id, state);
  }

  async create(visualization: Visualization): Promise<Visualization> {
    return this.visualizationsRepository.save(visualization)
      .then((visualization) => {
        console.debug('create visualization defaults');

        this.preferencesService.createDefaultPreferences(visualization);
        this.stateService.createDefaultState(visualization);
        this.colorsService.createDefaultColors(visualization);
        this.tagsService.createDefaultTags(visualization);

        return visualization;
      });
  }

  async update(visualization: Visualization): Promise<Visualization> {
    return this.visualizationsRepository.save(visualization);
  }

  /**
   * Deletes visualization with given `ID`. Related entities of type `Preferences`, `State`, `Tag`, `Node`
   * will be deleted as a result of a cascade operation.
   *
   * @param id id of a visualization
   */
  async remove(id: string): Promise<DeleteResult> {
    return this.visualizationsRepository.delete(id);
  }
}
