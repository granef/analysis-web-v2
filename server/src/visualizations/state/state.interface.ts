import { Visualization } from '../visualization.entity';

export interface IPan {
  x: number;
  y: number;
}

export interface IViewport {
  zoom: number;
  pan: IPan;
}

export interface IState {
  visualization?: Visualization;
  elements?: any[];
  viewport?: IViewport;
  scratch?: any;
}