import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { State } from './state.entity';
import { defaultState } from '../../seed/default-state';
import { Visualization } from '../visualization.entity';
import { merge } from "lodash";

@Injectable()
export class StateService {
  constructor(
    @InjectRepository(State)
    private stateRepository: Repository<State>
  ) {}

  async findForVisualization(visualizationId: string | number): Promise<State> {
    return this.stateRepository
      .createQueryBuilder('state')
      .where('state.visualization_id = :visualizationId', { visualizationId })
      .getOneOrFail();
  }

  async updateState(visualizationId: string | number, state: State): Promise<State> {
    return this.findForVisualization(visualizationId)
      .then((oldState) => {
        return this.stateRepository.save({
          ...state,
          id: oldState.id,
        });
      });
  }

  async createDefaultState(visualization: Visualization): Promise<State> {
    const state = this.stateRepository.create(defaultState);
    state.visualization = visualization;

    return this.stateRepository.save(state);
  }

  async remove(id: string | number): Promise<void> {
    await this.stateRepository.delete(id);
  }

}