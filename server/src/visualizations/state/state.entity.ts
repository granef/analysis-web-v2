import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Visualization } from '../visualization.entity';
import { IPan, IViewport, IState } from './state.interface';


export class Pan implements IPan {
  @Column({
    type: 'decimal'
  })  x: number;

  @Column({
    type: 'decimal'
  })  y: number;
}

export class Viewport implements IViewport {
  @Column({
    type: 'decimal'
  })
  zoom: number;

  @Column(() => Pan)
  pan: IPan;
}

/**
 * A class for storing visualization state, i.e. description of the current viewport, elements and scratches.
 */
@Entity()
export class State implements IState {

  @PrimaryGeneratedColumn()
  id: number;

  /**
   * Visualization to which the state belongs.
   */
  @ManyToOne(
    () => Visualization,
    (visualization) => visualization.tags,
    { nullable: true, onDelete: 'CASCADE' }
  )
  @JoinColumn({
    name: 'visualization_id',
  })
  visualization: Visualization;

  @Column(() => Viewport)
  viewport: Viewport;

  @Column({
    type: 'simple-json'
  })
  elements: any;

  /**
   * Long text for storing cytoscape scratches. It is a stringified object with the form of an object with structure
   * corresponding to `{ clustering: {}, baseNodeIds: []}`.
   */
  @Column({
    type: 'simple-json'
  })
  scratch: any;

}