import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Visualization } from './visualization.entity';
import { State } from './state/state.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Visualization]), TypeOrmModule.forFeature([State])],
  exports: [TypeOrmModule],
})
export class VisualizationsModule {}
