import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Tag } from '../tags/tag.entity';
import { Color } from '../colors/color.entity';

@Entity()
export class Visualization {
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * Create date
   */
  @CreateDateColumn({
    nullable: false,
  })
  created: Date;

  /**
   * Last update date
   */
  @UpdateDateColumn({
    nullable: false,
  })
  updated: Date;

  /**
   * Name
   */
  @Column({
    nullable: false,
  })
  name: string;

  /**
   * Description
   */
  @Column({
    nullable: true,
  })
  description: string;

  /**
   * Tags that can be assigned to nodes in the visualization
   */
  @OneToMany(() => Tag, (tag) => tag.visualization)
  tags: Tag[];

  /**
   * Colors that can be assigned to nodes in the visualization
   */
  @OneToMany(() => Color, (color) => color.visualization)
  colors: Color[];

  /**
   * Longer text that might be used to store notes about the data being visualized.
   */
  @Column({
    nullable: true,
  })
  note: string;

}
