import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('note')
export class Note {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'text',
    nullable: false,
  })
  content: string;

  @Column({
    name: 'node_id',
    nullable: false,
  })
  nodeId: string;
}
