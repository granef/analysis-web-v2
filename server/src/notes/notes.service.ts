import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Note } from './note.entity';

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(Note)
    private notesRepository: Repository<Note>,
  ) {}

  findAll(): Promise<Note[]> {
    return this.notesRepository.find();
  }

  findOne(id: string): Promise<Note> {
    return this.notesRepository.findOne(id);
  }

  async create(note: Note): Promise<Note> {
    return this.notesRepository.save(note);
  }

  async update(note: Note): Promise<Note> {
    return this.notesRepository.save(note);
  }

  async remove(id: string): Promise<void> {
    await this.notesRepository.delete(id);
  }
}
