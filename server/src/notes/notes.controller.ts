import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { NotesService } from './notes.service';
import { Note } from './note.entity';

@Controller('notes')
export class NotesController {
  constructor(private service: NotesService) {}

  @Post()
  async create(@Body() note: Note): Promise<Note> {
    return this.service.create(note);
  }

  @Post()
  async update(@Body() note: Note): Promise<Note> {
    return this.service.update(note);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.service.remove(id);
  }

  @Get()
  async findAll(): Promise<Note[]> {
    return this.service.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Note> {
    return this.service.findOne(id);
  }
}
