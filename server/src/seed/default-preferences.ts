import { IPreferences } from '../preferences/preferences.interface';

export const defaultPreferences: IPreferences = {
  layout: {
    name: 'cola',
    animate: true,
    infinite: true,
    fit: false,
    padding: 0,
    nodeDimensionsIncludeLabels: true,
    nodeSpacing: 10,
    convergenceThreshold: 0.09,
    // edgeLength: 10,
    // isLocked: colaIsLocked,
    // lockEvents: 'lock unlock layoutLock layoutUnlock',
  },
  selection: 'lasso',
  selectionType: 'additive', // || single
  style: {
    nodeBorders: false,
    nodeLabels: true,
    edgeArrows: true,
    edgeLabels: false,
  },
  timelineClustering: {
    unit: 'minutes',
    value: 15,
  },
}