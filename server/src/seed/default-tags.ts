export const tags = [
  { hex: '#EB9694', label: 'Malicious' },
  { hex: '#FAD0C3', label: 'Suspicious' },
  { hex: '#C4DEF6', label: 'Normal' },
]