import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { Color } from '../colors/color.entity';
import { colors } from './default-colors';
import { IColor } from '../colors/color.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Tag } from '../tags/tag.entity';
import { ITag } from '../tags/tag.interface';
import { tags } from './default-tags';
import { Preferences } from '../preferences/preferences.entity';
import { defaultPreferences } from './default-preferences';

@Injectable()
export class SeedService implements OnApplicationBootstrap {

  constructor(
    @InjectRepository(Color)
    private readonly colorRepository: Repository<Color>,
    @InjectRepository(Tag)
    private readonly tagRepository: Repository<Tag>,
    @InjectRepository(Preferences)
    private readonly preferencesRepository: Repository<Preferences>,
  ) {
  }


  async onApplicationBootstrap() {
    if (process.env.INITIALIZE_DATABASE === 'true') {
      await Promise.all(this.createDefaultColors())
        .then(createdEntities => {
          const numberOfNewColors = createdEntities.filter(nullOrCreated => !!nullOrCreated).length
          console.log(`Created ${numberOfNewColors} colors.`);
          return Promise.resolve(true);
        })
        .catch(error => Promise.reject(error))
        .finally(() => console.log('Default colors initialization finished.'));

      await Promise.all(this.createDefaultTags())
        .then(createdEntities => {
          const numberOfNewColors = createdEntities.filter(nullOrCreated => !!nullOrCreated).length
          console.log(`Created ${numberOfNewColors} tags.`);
          return Promise.resolve(true);
        })
        .catch(error => Promise.reject(error))
        .finally(() => console.log('Default colors initialization finished.'));
    }

    await this.createDefaultPreferences()
      .then(value => {
        if (value) {
          console.log('Created default preferences.');
        }
      })
      .catch(error => console.error('Could not create default preferences.', error))
      .finally(() => console.log('Default preferences initialization finished.'));
  }


  /**
   * Create default colors. First it checks if a default color with given hex exists. If not, creates it.
   */
  createDefaultColors(): Array<Promise<Color>> {
    return colors.map(async (color: IColor) => {
      return await this.colorRepository
        .findOne({
          hex: color.hex,
          visualization: null,
        })
        .then(async dbColor => Promise.resolve(!!dbColor ? null : await this.colorRepository.save(color)))
        .catch(error => Promise.reject(error));
    });
  }


  /**
   * Create default tags. First it checks if a default tag with given hex exists. If not, creates it.
   */
  createDefaultTags(): Array<Promise<Tag>> {
    return tags.map(async (tag: ITag) => {
      return await this.tagRepository
        .findOne({
          hex: tag.hex,
          visualization: null,
        })
        .then(async dbTag => Promise.resolve(!!dbTag ? null : await this.tagRepository.save(tag)))
        .catch(error => Promise.reject(error));
    });
  }

  /**
   * Create default preferences if not exists.
   */
  createDefaultPreferences(): Promise<Preferences>  {
    return this.preferencesRepository.findOne({ visualization: null })
      .then(dbPreferences => dbPreferences ? null : this.preferencesRepository.save(defaultPreferences))
      .catch(error => Promise.reject(error));
  }
}