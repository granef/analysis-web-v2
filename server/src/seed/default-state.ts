import { IState } from '../visualizations/state/state.interface';

const eles = [];
const scratch = {
  clustering: {},
  baseNodeIds: []
};

export const defaultState: IState = {
  elements: eles,
  scratch: scratch,
  viewport: {
    zoom: 1,
    pan: {
      x: 0,
      y: 0
    }
  },
}