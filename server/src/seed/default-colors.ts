import { IColor } from '../colors/color.interface';

export const colors: IColor[] = [
  { hex: '#B80000', label: 'red' },
  { hex: '#DB3E00', label: 'orange' },
  { hex: '#FCCB00', label: 'yellow' },
  { hex: '#008B02', label: 'green' },
  { hex: '#006B76', label: 'turquoise' },
  { hex: '#1273DE', label: 'light blue' },
  { hex: '#004DCF', label: 'dark blue' },
  { hex: '#5300EB', label: 'violet' },
]
