import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Visualization } from '../visualizations/visualization.entity';
import {
  IPreferences,
  IStyle,
  ITimelineClustering,
  LayoutOptions,
  Selection,
  SelectionType,
  TimeUnit,
} from './preferences.interface';

export class StylePreferences implements IStyle {

  @Column()
  nodeBorders: boolean;

  @Column()
  nodeLabels: boolean;

  @Column()
  edgeArrows: boolean;

  @Column()
  edgeLabels: boolean;

}

export class TimelineClustering implements ITimelineClustering {
  @Column()
  unit: TimeUnit;

  @Column()
  value: number;
}

@Entity()
export class Preferences implements IPreferences {
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * Visualization to which the preference settings belong. If `null`, they will be used as default preferences
   * set to newly created visualization projects.
   */
  @ManyToOne(
    () => Visualization,
    (visualization) => visualization.tags,
    { nullable: true, onDelete: 'CASCADE' }
  )
  @JoinColumn({
    name: 'visualization_id',
  })
  visualization: Visualization;

  /**
   * Mode of selection
   */
  @Column()
  selection: Selection;

  /**
   * Type of selection
   */
  @Column()
  selectionType: SelectionType;

  @Column(() => StylePreferences)
  style: StylePreferences;

  @Column(() => TimelineClustering)
  timelineClustering: TimelineClustering;

  @Column({
    type: 'simple-json'
  })
  layout: LayoutOptions; // consider saving for multiple layouts

  //consider adding (possibly for default only): wheel sensitivity, hideEdgesOnViewport, motionBlur
}