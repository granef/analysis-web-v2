import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Preferences } from './preferences.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Preferences])],
  exports: [TypeOrmModule],
})
export class PreferencesModule {}
