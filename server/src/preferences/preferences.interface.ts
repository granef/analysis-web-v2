import { Visualization } from '../visualizations/visualization.entity';
import { StylePreferences, TimelineClustering } from './preferences.entity';

export interface IStyle {
  nodeBorders: boolean;
  nodeLabels: boolean;
  edgeArrows: boolean;
  edgeLabels: boolean;
}


export type TimeUnit = "years" | "months" | "weeks" | "days" | "hours" | "minutes" | "seconds" | "milliseconds";
export interface ITimelineClustering {
  unit: TimeUnit,
  value: number,
}

export type Selection = 'lasso' | 'box';
export type SelectionType = 'additive' | 'single';
export type Layout =
  'random'
  | 'preset'
  | 'grid'
  | 'circle'
  | 'concentric'
  | 'avsdf'
  | 'dagre'
  | 'breadthfirst'
  | 'klay'
  | 'fcose'
  | 'cose-bilkent'
  | 'cose'
  | 'cola'
  | 'cise'
  | 'euler'
  | 'spread'
  | 'springy';

export interface LayoutOptions {
  name: Layout;

  [option: string]: any;
}

export interface IPreferences {
  visualization?: Visualization;
  selection: Selection;
  selectionType: SelectionType;
  style: StylePreferences;
  layout: LayoutOptions;
  timelineClustering: TimelineClustering;
}