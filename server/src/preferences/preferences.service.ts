import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Preferences } from './preferences.entity';
import { Visualization } from '../visualizations/visualization.entity';

@Injectable()
export class PreferencesService {
  constructor(
    @InjectRepository(Preferences)
    private preferencesRepository: Repository<Preferences>,
  ) {
  }

  async findForVisualization(visualizationId: number | null): Promise<Preferences> {
    const queryBuilder = this.preferencesRepository.createQueryBuilder('preferences');

    if (visualizationId === null) {
      queryBuilder.where('preferences.visualization_id IS NULL');
    } else {
      queryBuilder.where('preferences.visualization_id = :visualizationId', { visualizationId });
    }

    return queryBuilder.getOneOrFail();
  }

  async updatePreferences(visualizationId: number | null, preferences: Preferences): Promise<Preferences> {
    const oldPreferences = await this.findForVisualization(visualizationId);
    await this.preferencesRepository.save({
      ...preferences,
      id: oldPreferences.id,
    });
    return this.findForVisualization(visualizationId);
  }

  async createDefaultPreferences(visualization: Visualization): Promise<Preferences> {
    const { id, visualization: nullVisualization, ...defaultPreferences } = await this.findForVisualization(null);

    const preferences = this.preferencesRepository.create(defaultPreferences);
    preferences.visualization = visualization;

    return this.preferencesRepository.save(preferences);
  }

  async remove(id: string | number): Promise<void> {
    await this.preferencesRepository.delete(id);
  }

}