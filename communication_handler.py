#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""Granef visualization handler to load and start all visualization components (PostgreSQL, backend server using node and frontend using Nginx).
"""


from math import e
import sys          # Common system functions
import os           # Common operation system functions
import argparse     # Arguments parser
import logging, coloredlogs                 # Standard logging functionality with colors functionality
import pathlib      # Directory path functions
import subprocess   # Run external processes


def create_logs_directory(directory_path: str = "/var/log/granef/") -> bool:
    """Creates defined directory in the filesystem.

    Args:
        directory_path (str, optional): Full path to the directory that should be created. Defaults to "/var/log/granef".

    Returns:
        bool: False if some error occurred, True otherwise.
    """
    try:
        os.makedirs(directory_path, exist_ok=True)
        logging.debug("Logging directory {directory_path} created".format(directory_path=directory_path))
        return True
    except Exception as exc:
        logging.error("Logging directory {directory_path} cannot be created: {error}".format(directory_path=directory_path), error=str(exc))
        return False


def start_subprocess(process_command: str, success_message: str, log_stdout: str, log_stderr: str, directory: str = None, user: str = None) -> subprocess.Popen[str]:
    """Starts specified command as a subprocess and returns object related to the created process.

    Args:
        process_command (str): Command to start.
        success_message (str): STDOUT message of the command that marks successful command start.
        log_stdout (str): Full path where the stdout of the process should be stored. 
        log_stderr (str): Full path where the stderr of the process should be stored. 
        directory (str, optional): The directory in which the command should be started. Defaults to None (use the same directory as this script).
        user (str, optional): The user by which the command should be started. Defaults to None (use the same user as this script).

    Returns:
        subprocess.Popen[str]: An initialized object of the started process, None if some error occurred.
    """
    # Create process log files and set up file handlers
    try:
        file_stdout_write = open(log_stdout, "a")
        file_stderr_write = open(log_stderr, "a")
        file_stdout_read = open(log_stdout, "r")
        file_stderr_read = open(log_stderr, "r")
    except Exception as exc:
        logging.error("Log file cannot be created: {error}".format(error=str(exc)))
        return None
    # Start nginx process and check if the subprocess is running
    process = subprocess.Popen(process_command, shell=True, stdout=file_stdout_write, stderr=file_stderr_write, cwd=directory, user=user, bufsize=1, universal_newlines=True)
    while True:
        nextline_stdout = file_stdout_read.readline()
        if nextline_stdout == '' and process.poll() is not None:
            stderr = file_stderr_read.read()
            logging.error("Process \"{command}\" cannot be started: {error_msg}".format(command=process_command, error_msg=stderr))
            return None
        if nextline_stdout.find(success_message) != -1:
            break
    logging.debug("Process \"{command}\" successfully started".format(command=process_command))
    return process


def set_nginx_configuration(configuration_path: str, listen_ip: str, listen_port: int, api_ip: str, api_port: int) -> bool:
    """Updates fields <listen_ip:port> and <api_ip:port> in the nginx.conf configuration file according to function arguments.

    Args:
        configuration_path (str): Path to the nginx.conf configuration file.
        listen_ip (str): IP address on which the Nginx server should listen for new connections.
        listen_port (int): Port on which the Nginx server should listen for new connections.
        api_ip (str): IP address or domain name of the Granef API.
        api_port (int): Port of the Granef API.

    Returns:
        bool: False if some error occurred, True otherwise.
    """
    # Check if the configuration file exists
    if not os.path.isfile(configuration_path):
        logging.error("Nginx configuration file not found")
        return False

    # Update the configuration file
    listen_ip_port = "{ip}:{port}".format(ip=listen_ip, port=listen_port)
    api_ip_port = "{ip}:{port}".format(ip=api_ip, port=api_port)
    with open(configuration_path, "rt") as configuration_file:
        configuration = configuration_file.read()

    configuration = configuration.replace("<listen_ip:port>", listen_ip_port)
    configuration = configuration.replace("<api_ip:port>", api_ip_port)

    with open(configuration_path, "wt") as configuration_file:
        configuration_file.write(configuration)
    
    logging.debug("Nginx configuration file updated")
    return True


if __name__ == "__main__":
    # Argument parser automatically creates -h argument
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", metavar="FILE_OR_DIRECTORY_PATH", help="Dummy argument to preserve compatibility.", default="/", type=str, required=False)
    parser.add_argument("-ip", "--ip", metavar="IP_ADDRESS", help="IP address to bind the web visualization application.", default="0.0.0.0", type=str, required=False)
    parser.add_argument("-p", "--port", metavar="PORT_NUMBER", help="Port to bind the web visualization application.", default=80, type=int, required=False)
    parser.add_argument("-ai", "--api_ip", metavar="IP_ADDRESS", help="IP address of the Granef analysis API.", default="granef-analysis-api", type=str, required=False)
    parser.add_argument("-ap", "--api_port", metavar="PORT_NUMBER", help="Port of the Granef analysis API.", default=7000, type=int, required=False)
    parser.add_argument("-nc", "--nginx_configuration", metavar="FILE_PATH", help="Nginx configuration file path.", default="/etc/nginx/conf.d/default.conf", type=pathlib.Path, required=False)
    parser.add_argument("-lp", "--logs_path", metavar="DIRECTORY_PATH", help="Directory to store subprocess outputs.", default="/var/log/granef/", type=str, required=False)
    parser.add_argument("-l", "--log", choices=["debug", "info", "warning", "error", "critical"], help="Log level", default="INFO", type=str, required=False)
    args = parser.parse_args()

    # Set logging
    coloredlogs.install(level=getattr(logging, args.log.upper()), fmt="%(asctime)s [%(levelname)s]: %(message)s")

    # Update Nginx configuration
    if not set_nginx_configuration(configuration_path=args.nginx_configuration, listen_ip=args.ip, listen_port=args.port, api_ip=args.api_ip, api_port=args.api_port):
        logging.critical("Nginx server cannot be started")
        sys.exit(1)

    # Create subprocess logs directory
    create_logs_directory(args.logs_path)
    
    # Start database
    database_process = start_subprocess(
        process_command="pg_ctl start -D /var/lib/postgresql/data",
        success_message="server started",
        user="postgres",
        log_stdout=args.logs_path + "database-stdout.log",
        log_stderr=args.logs_path + "database-stderr.log")
    if not database_process:
        logging.critical("Database cannot be started")
        sys.exit(2)

    # Start visualization backend server
    backend_process = start_subprocess(
        process_command="node dist/main",
        success_message="Default preferences initialization finished.",
        directory="/usr/share/nginx/html/server/",
        log_stdout=args.logs_path + "backend-stdout.log",
        log_stderr=args.logs_path + "backend-stderr.log")
    if not backend_process:
        logging.critical("Backend server cannot be started")
        sys.exit(3)
    
    # Start Nginx server by default outputs to STDERR, STDERR is therefore redirected to STDOUT
    nginx_process = start_subprocess(
        process_command="/usr/sbin/nginx 2>&1",
        success_message="start worker processes",
        log_stdout=args.logs_path + "nginx-stdout.log",
        log_stderr=args.logs_path + "nginx-stderr.log")
    if not nginx_process:
        logging.critical("Nginx server cannot be started")
        sys.exit(4)
    
    # Run all processes till Keyboard interrupt
    try:
        database_process.communicate()
        backend_process.communicate()
        nginx_process.communicate()
    except KeyboardInterrupt:
        logging.debug("System kill of all visualization services")
        subprocess.run("killall nginx postgres node", shell=True)
        nginx_process.communicate()
        backend_process.communicate()
        database_process.communicate()
