#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


# Use node image as a parent image to build server application
FROM node:lts-alpine AS server-build
WORKDIR /usr/src/app

# Copy YARN configuration packages
COPY ./server/package*.json ./
COPY ./server/yarn.lock ./

# Install all project dependencies
RUN yarn install --frozen-lockfile

# Copy and build the application
COPY ./server/ .
RUN yarn build


# Use node image as a parent image to build client application
FROM node:lts-alpine AS client-build
WORKDIR /usr/src/app

# Copy YARN configuration packages and Cytoscape module
COPY ./client/package*.json ./
COPY ./client/yarn.lock ./
COPY ./client/cytoscape-cola-with-layout-locking-2.5.0.tgz ./

# Temporar fix for incompatible OpenSSL
ENV NODE_OPTIONS="--openssl-legacy-provider"

# Install all project dependencies
RUN yarn install --frozen-lockfile

# Copy and build the application
COPY ./client/ .
RUN yarn build


# Copy created application and start with clean nginx image
FROM nginx:alpine AS visualization

# Set the working directory
WORKDIR /usr/local/bin/granef

# Copy all required files
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY ./communication_handler.py /usr/local/bin/granef/
COPY ./requirements.txt /usr/local/bin/granef/
COPY --from=server-build /usr/src/app/dist /usr/share/nginx/html/server/dist/
COPY --from=server-build /usr/src/app/node_modules /usr/share/nginx/html/server/node_modules/
COPY --from=server-build /usr/src/app/package.json /usr/share/nginx/html/server/
COPY --from=server-build /usr/src/app/ormconfig.json /usr/share/nginx/html/server/
COPY --from=client-build /usr/src/app/build /usr/share/nginx/html/client/

# Install NodeJS, PostgreSQL, and Python3 with required modules
RUN apk add --update --no-cache postgresql nodejs python3 py3-pip \
    && sed -i 's/"host": "database"/"host": "localhost"/g' /usr/share/nginx/html/server/ormconfig.json \
    && mkdir /run/postgresql \
    && chown postgres:postgres /run/postgresql/ \
    && su postgres -c "mkdir /var/lib/postgresql/data && chmod 0700 /var/lib/postgresql/data" \
    && su postgres -c "initdb -D /var/lib/postgresql/data" \
    && su postgres -c "pg_ctl start -D /var/lib/postgresql/data" \
    && su postgres -c "psql -c \"create database granef_visualization;\"" \
    && su postgres -c "psql -c \"create user granef with encrypted password 'granef';\"" \
    && su postgres -c "psql -c \"grant all privileges on database granef_visualization to granef;\"" \
    && su postgres -c "psql -d granef_visualization -c \"GRANT ALL ON SCHEMA public TO granef;\"" \
    && su postgres -c "pg_ctl stop -D /var/lib/postgresql/data" \
    && pip3 install --trusted-host pypi.python.org --no-cache-dir --break-system-packages -r requirements.txt

# Run communication-handler.py with arguments when container launches (CMD if there are no arguments) 
ENTRYPOINT ["python3", "communication_handler.py"]
